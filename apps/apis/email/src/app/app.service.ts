import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import {
  EmailService,
  OpsmatixLogger,
  CaseDocument,
  CaseService,
  CaseTypeService,
  CreateDraftEmailResponse,
  ActivityNoteService,
  BaseService,
  UserService,
  EmailServiceErrorCodes,
  MailboxService,
  AutoReplyOrUndeliverableResponse,
  EmailIndexService,
} from '@opsmatix/core';
import {
  Case,
  User,
  CaseDetails,
  ForwardEmail,
  Omnichannel,
  ReplyEmail,
  ReplyToAllEmail,
  SendEmail,
  CreateDraftEmail,
} from '@opsmatix/interfaces';
import { EmailContentProvider } from './email-content.provider';
import { ContactListProvider } from './contact-list.provider';
import EmailConversations = CaseDetails.EmailConversations;

const findMailbox = (
  emailConversations: EmailConversations[],
  emailId?: string
): string => {
  const email: CaseDetails.EmailConversations = emailConversations.find(
    (emailConversation) =>
      emailConversation.emails.find(
        (em) => emailId == null || em.emailId === emailId
      ) !== undefined
  );
  return email?.emails?.find((em) => emailId == null || em.emailId === emailId)
    ?.mailbox;
};

const findMail = (
  emailConversations: EmailConversations[],
  emailId: string
): Omnichannel.Email => {
  const email: CaseDetails.EmailConversations = emailConversations.find(
    (emailConversation) =>
      emailConversation.emails.find((em) => em.emailId === emailId) !==
      undefined
  );
  return email?.emails?.find((em) => em.emailId === emailId);
};

@Injectable()
export class AppService {
  private readonly logger: Logger = new OpsmatixLogger(AppService.name);
  constructor(
    private readonly caseService: CaseService,
    private readonly caseTypeService: CaseTypeService,
    private readonly mailboxService: MailboxService,
    private readonly emailService: EmailService,
    private readonly emailContentProvider: EmailContentProvider,
    private readonly contactListProvider: ContactListProvider,
    private readonly userService: UserService,
    private readonly activityNoteService: ActivityNoteService,
    private readonly emailIndexService: EmailIndexService
  ) {}

  async userDetails(userId: string) {
    const user = userId
      ? await this.userService.findByIdAsync(userId)
      : undefined;
    const userFullname = user ? User.getFullName(user) : 'System';
    const userDetails = user ? `${userFullname}(${user.username})` : 'System';
    return userDetails;
  }

  async getEmailContent(
    caseId: string,
    emailId: string,
    format: Omnichannel.ContentType,
    replaceCidWithImageUrl = false
  ): Promise<string> {
    try {
      const opxCase: Case = await this.caseService.findByIdDetail(caseId);

      const email = findMail(opxCase.caseDetails.emailConversations, emailId);

      if (!email) {
        throw new Error(`email ${emailId} is not found!`);
      }

      if (email.body.contentType === format) {
        return email.body.content;
      }

      const mailbox = email.mailbox;
      const response = await this.emailService
        .getEmail(mailbox, emailId, format)
        .then((res) => {
          if (res?.body?.content == null) {
            throw new Error(`Email not found!`);
          }
          return res;
        })
        .catch((err) => {
          //if error, try finding new email id assuming it is moved to a different folder
          return this.findNewEmailId(emailId, null, opxCase).then(
            (newEmailId) => {
              if (newEmailId) {
                //found new email id
                return this.emailService.getEmail(
                  email.mailbox,
                  newEmailId,
                  format
                );
              }
              throw err;
            }
          );
        });
      if (response?.body?.content) {
        const content = response?.body?.content;
        const updatedContent =
          content != null && format === 'html' && replaceCidWithImageUrl
            ? await this.enrichHtmlContent(content, mailbox, response.id)
            : content;
        return updatedContent;
      } else {
        throw new Error(`email ${emailId} is not found!`);
      }
    } catch (err) {
      const message = `Email Content could not be fetched! - ${err.message}`;
      this.logger.error(message, err.stack);
      throw err;
    }
  }

  async getEmailContentFromMailbox(
    mailbox: string,
    emailId: string,
    format: Omnichannel.ContentType,
    replaceCidWithImageUrl = false
  ): Promise<string> {
    try {
      const response = await this.emailService
        .getEmail(mailbox, emailId, format)
        .then((res) => {
          if (!res?.body?.content) {
            throw new Error(`Email not found!`);
          }
          return res;
        });
      if (response?.body?.content) {
        const content = response?.body?.content;
        const updatedContent =
          content != null && format === 'html' && replaceCidWithImageUrl
            ? await this.enrichHtmlContent(content, mailbox, response.id)
            : content;
        return updatedContent;
      } else {
        throw new Error(`email ${emailId} is not found!`);
      }
    } catch (err) {
      const message = `Email Content could not be fetched! - ${err.message}`;
      this.logger.error(message, err.stack);
      throw err;
    }
  }

  async getEmailTypeInfoFromMailbox(
    mailbox: string,
    emailIdInput: string
  ): Promise<AutoReplyOrUndeliverableResponse | null> {
    try {
      const emailId = await this.currentOrNewEmailId(mailbox, emailIdInput);
      if (emailId == null) {
        return null;
      }

      const response = await this.emailService.isAutoReplyOrUndeliverable(
        mailbox,
        emailId
      );

      return response;
    } catch (err) {
      const message = `Email Type Info could not be fetched! - ${err.message}`;
      this.logger.error(message, err.stack);
      throw err;
    }
  }

  async getEmailInternetMessageHeaders(
    mailbox: string,
    emailIdInput: string
  ): Promise<Record<string, string> | null> {
    try {
      const emailId = await this.currentOrNewEmailId(mailbox, emailIdInput);
      if (emailId == null) {
        return null;
      }

      const response = await this.emailService.getEmailInternetMessageHeaders(
        mailbox,
        emailId
      );

      return response;
    } catch (err) {
      const message = `Email InternetMessageHeaders could not be fetched! - ${err.message}`;
      this.logger.error(message, err.stack);
      throw err;
    }
  }

  /* async enrichHtmlContent(content: string, mailbox: string, emailId: string) {
    this.logger.debug(`enriching.............`)
    return content.replace(
      /cid:/g,
      `/api/email/attachment-by-cid/${mailbox}/${emailId}/`
    );
  } */

  async getEmailAttachmentsMetadata(
    mailboxUser: string,
    emailId: string,
    caseId?: string
  ) {
    try {
      const response = await this.emailService
        .getAttachmentsMetadata(mailboxUser, emailId)
        .catch((err) => {
          //if error, try finding new email id assuming it is moved to a different folder
          return this.findNewEmailId(emailId, caseId).then((newEmailId) => {
            if (newEmailId) {
              //found new email id
              return this.getEmailAttachmentsMetadata(mailboxUser, newEmailId);
            }
            throw err;
          });
        });
      if (response) {
        return response;
      } else {
        throw new Error(`email ${emailId} is not found!`);
      }
    } catch (err) {
      const message = `email attachment metadata could not be fetched! - ${err.message}`;
      this.logger.error(message, err.stack);
      throw err;
    }
  }

  //FIXME: ENG-1312 - does not need to get the entire email to check its existence
  async currentOrNewEmailId(
    mailboxUser: string,
    emailId: string,
    caseId?: string,
    opxCase?: Case
  ): Promise<string | null> {
    const email = await this.emailService
      .getEmail(mailboxUser, emailId, Omnichannel.ContentType.Text)
      .catch((_) => null);
    if (email && email?.id) {
      return email.id;
    }
    this.logger.log(
      `Email is not found, searching if there is a new id for the email. caseId[${
        caseId ?? opxCase?._id
      }], emailId[${emailId}]`
    );
    return await this.findNewEmailId(emailId, caseId, opxCase);
  }

  async findNewEmailId(
    emailId: string,
    caseId?: string,
    opxCase?: Case
  ): Promise<string | null> {
    try {
      let email: Omnichannel.Email | undefined = undefined;
      if (!caseId && !opxCase) {
        // if conversationId is found, can be used to find the new case.
        let conversationId: string | undefined = undefined;

        // can use EmailIndex to find the new email id, will not work for older emails.
        const emailIndex = await this.emailIndexService.findByEmailId(emailId);
        if (emailIndex) {
          conversationId = emailIndex.conversationId;
        }
        if (conversationId) {
          const opxCases = await this.caseService.findAllAsync({
            conversationId: conversationId,
          });

          for (const opxCaseI of opxCases) {
            await opxCaseI.populate('caseDetails');
            email = findMail(opxCaseI.caseDetails.emailConversations, emailId);
            if (email) {
              // found the email
              caseId = opxCaseI._id.toString();
              opxCase = opxCaseI;
              break;
            }
          }
        } else {
          return null;
        }
      } else {
        if (!opxCase) {
          opxCase = await this.caseService.findByIdDetail(caseId);
        }

        if (opxCase) {
          email = findMail(opxCase.caseDetails.emailConversations, emailId);
        }

        if (!email) {
          throw new Error(
            `email[${emailId}] is not found in the case[${caseId}]!`
          );
        }
      }

      if (!email) {
        this.logger.warn(
          `email[${emailId}] is not found in opsmatix store, hence it is not possible to find the new email id!`
        );
        return null;
      }

      const emailsInConversation = await this.emailService.searchEmails({
        mailbox: email.mailbox,
        receivedDateTime: email.receivedDateTime.toISOString(),
        conversationId: email.conversationId,
      });

      const emailFromCon = emailsInConversation?.value?.find(
        (em) =>
          new Date(em.receivedDateTime).getTime() ===
            email.receivedDateTime.getTime() &&
          em?.id &&
          emailId != em?.id // 'emailId != em?.id' is to prevent infinite recursion in the consumer.
      );

      if (emailFromCon?.id) {
        this.logger.log(
          `newEmailId is found (old-->new): ${emailId}-->${emailFromCon.id}`
        );
        return emailFromCon.id;
      }

      this.logger.debug(`no new email id is found for ${emailId}`);
      return null;
    } catch (error) {
      this.logger.error(
        `Error finding new email id for ${emailId}, error is: ${error.message}`,
        error.stack
      );
      return null;
    }
  }

  async enrichHtmlContent(content: string, mailbox: string, emailId: string) {
    try {
      let updatedContent = content;
      const cidMatches = content.match(/(?:cid:).*?([^\\"']*)/g);
      if (cidMatches && cidMatches.length > 0) {
        const attachmentsMetadata =
          await this.emailService.getAttachmentsMetadata(mailbox, emailId);
        for (const cidMatch of cidMatches) {
          const cidValue = cidMatch.substring(4);
          const cidAttachment = attachmentsMetadata.find(
            (c) => cidValue === c.contentId
          );
          if (cidAttachment) {
            updatedContent = updatedContent.replace(
              cidMatch,
              `/api/email/attachment/${mailbox}/${emailId}/${cidAttachment.id}`
            );
          }
        }
      }
      return updatedContent;
    } catch (error) {
      this.logger.error(
        `error enriching html content for mailuser:${mailbox} emailid: ${emailId}, error is: ${error.message}`
      );
      return content;
    }
  }

  /* async enrichHtmlContent(content: string, mailbox: string, emailId: string) {
    try {
      let updatedContent = content;
      const cidMatches = content.match(/(?:cid:).*?([^\\"']*)/g);
      if (cidMatches && cidMatches.length > 0) {
        const cidAttachments = await this.getEmailCidAttachments(
          mailbox,
          emailId,
          cidMatches.map((s) => s.substring(4))
        );

        if (cidAttachments && cidAttachments.length > 0) {
          for (const cidMatch of cidMatches) {
            const cidValue = cidMatch.substring(4);
            const res = cidAttachments.find((a) => a.contentId === cidValue);
            if (res && res.base64Content) {
              updatedContent = updatedContent.replace(
                cidMatch,
                `data:image;base64,${res.base64Content}`
              );
            }
          }
        }
      }
      return updatedContent;
    } catch (error) {
      this.logger.error(
        `error enriching html content for mailuser:${mailbox} emailid: ${emailId}`
      );
      return content;
    }
  } */

  async getEmailAttachment(
    mailbox: string,
    emailId: string,
    attachmentId: string,
    caseId?: string
  ): Promise<Omnichannel.Email.Attachment> {
    return this.emailService
      .getAttachment(mailbox, emailId, attachmentId)
      .catch((err) => {
        //if error, try finding new email id assuming it is moved to a different folder
        return this.findNewEmailId(emailId, caseId).then((newEmailId) => {
          if (newEmailId) {
            //found new email id
            return this.getEmailAttachment(mailbox, newEmailId, attachmentId);
          }
          throw err;
        });
      });
  }

  async getEmailCidAttachments(
    mailbox: string,
    emailId: string,
    contentIds: string[]
  ): Promise<Omnichannel.Email.Attachment[]> {
    const attachments = await this.emailService.getInlineAttachments(
      mailbox,
      emailId
    );
    const cidAttachments = attachments.filter((c) =>
      contentIds.includes(c.contentId)
    );
    return cidAttachments;
  }

  async getEmailAttachments(mailboxUser: string, emailId: string) {
    try {
      const response = await this.emailService.getAttachments(
        mailboxUser,
        emailId
      );
      if (response) {
        return response;
      } else {
        throw new Error(`email ${emailId} is not found!`);
      }
    } catch (err) {
      const message = `Email attachment could not be fetched! - ${err.message}`;
      this.logger.error(message, err.stack);
      throw err;
    }
  }

  async getEmailInMime(caseId: string, emailId: string): Promise<string> {
    try {
      const opxCase: Case = await this.caseService.findByIdDetail(caseId);

      const email = findMail(opxCase.caseDetails.emailConversations, emailId);

      if (!email) {
        throw new Error(`email ${emailId} is not found!`);
      }

      const response = await this.emailService
        .getEmailInMime(email.mailbox, emailId)
        .catch((err) => {
          //if error, try finding new email id assuming it is moved to a different folder
          return this.findNewEmailId(emailId, null, opxCase).then(
            (newEmailId) => {
              if (newEmailId) {
                //found new email id
                return this.emailService.getEmailInMime(
                  email.mailbox,
                  newEmailId
                );
              }
              throw err;
            }
          );
        })
        .catch((err) => {
          this.logger.warn(
            `error finding the email, hence trying with conversation id. the error is: ${err.message}!`
          );
          return undefined;
        });
      if (response) {
        return response;
      } else {
        throw new Error(`email ${emailId} is not found!`);
      }
    } catch (err) {
      const message = `Email could not be fetched! - ${err.message}`;
      this.logger.error(message, err.stack);
      throw err;
    }
  }

  async replyToEmail(
    caseId: string,
    replyEmailDto: ReplyEmail,
    requesterUserId?: string
  ) {
    const opxCase: CaseDocument = await this.caseService.findByIdDetail(caseId);

    if (!opxCase) {
      throw new Error(`opxCase[${caseId}] not found!`);
    }

    const message = this.emailContentProvider.getEmailBodyInHtml(
      caseId,
      replyEmailDto.body
    );

    const emailIdInput = replyEmailDto.emailId;
    const mailbox = findMailbox(
      opxCase.caseDetails.emailConversations,
      emailIdInput
    );

    const emailId = await this.currentOrNewEmailId(
      mailbox,
      emailIdInput,
      undefined,
      opxCase
    );

    if (!emailId) {
      throw new Error(
        `Email[${emailIdInput}] is not found in the vendor store any more!`
      );
    }

    const success = await this.emailService.reply(
      mailbox,
      emailId,
      message,
      replyEmailDto.toRecipients,
      replyEmailDto.attachments
    );
    if (success) {
      // get full details of requester
      const requesterDetails = await this.userDetails(requesterUserId);
      //Log Audit
      await this.activityNoteService.create({
        entity: BaseService.toObjectId(caseId),
        user: requesterUserId || 'System',
        title: 'Email Replied',
        details: `${requesterDetails} has replied to an email under this case`,
        component: 'email-api',
      });
    } else {
      this.logger.error(
        `Failed to reply email for the case: ${opxCase._id.toString()}`
      );
      throw new Error(`Failed to reply email for the opxCase: ${caseId}`);
    }
  }

  async replyToAllEmail(
    caseId: string,
    replyToAllEmailDto: ReplyToAllEmail,
    requesterUserId?: string
  ) {
    const opxCase: CaseDocument = await this.caseService.findByIdDetail(caseId);

    if (!opxCase) {
      throw new Error(`opxCase[${caseId}] not found!`);
    }

    const message = this.emailContentProvider.getEmailBodyInHtml(
      caseId,
      replyToAllEmailDto.body
    );
    const newBody = {
      contentType: 'html',
      content: message,
      includesQuote:
        replyToAllEmailDto?.body?.includesQuote == null
          ? replyToAllEmailDto.draftEmailId != null // if draftId is present, it is assumed that user is sending the full email with quoted text/history.
          : replyToAllEmailDto?.body?.includesQuote,
    };

    const emailIdInput = replyToAllEmailDto.emailId;
    const mailbox = findMailbox(
      opxCase.caseDetails.emailConversations,
      emailIdInput
    );

    const emailId = await this.currentOrNewEmailId(
      mailbox,
      emailIdInput,
      undefined,
      opxCase
    );

    if (!emailId) {
      throw new Error(
        `Email[${emailIdInput}] is not found in the vendor store any more!`
      );
    }
    replyToAllEmailDto.toRecipients =
      await this.contactListProvider.expandedRecipientEmails(
        replyToAllEmailDto.toRecipients
      );
    replyToAllEmailDto.ccRecipients =
      await this.contactListProvider.expandedRecipientEmails(
        replyToAllEmailDto.ccRecipients
      );
    replyToAllEmailDto.bccRecipients =
      await this.contactListProvider.expandedRecipientEmails(
        replyToAllEmailDto.bccRecipients
      );

    const emailServiceResponse = await this.emailService.sendReplyAllDraftEmail(
      {
        draftEmail: {
          ...replyToAllEmailDto,
          emailId: emailId,
          fromMailbox: mailbox,
          body: newBody,
        },
      }
    );

    if (emailServiceResponse.success) {
      // get full details of requester
      const requesterDetails = await this.userDetails(requesterUserId);
      //Log Audit
      await this.activityNoteService.create({
        entity: BaseService.toObjectId(caseId),
        user: requesterUserId || 'System',
        title: 'Email ReplyAll',
        details: `${requesterDetails} has replied to an email under this case`,
        component: 'email-api',
      });
    } else {
      const errorMessage = `Failed to replyAll email for the case: ${
        opxCase._id
      }, error is: ${JSON.stringify(emailServiceResponse?.error)}`;
      this.logger.error(errorMessage);
      throw new Error(errorMessage);
    }
  }

  async getMailbox(caseTypeStr: string): Promise<string | null> {
    const mailboxes = await this.mailboxService.findAllAsync({
      $or: [
        { defaultCaseType: caseTypeStr },
        { supportedCaseTypes: caseTypeStr },
      ],
    });

    if (mailboxes.length === 0) {
      this.logger.warn(`No mailbox found for caseType: ${caseTypeStr}!`);
      return null;
    } else if (mailboxes.length > 1) {
      this.logger.debug(
        `Multiple mailboxes found for caseType: ${caseTypeStr}, lets check if there is match for default one!`
      );
      const mailboxesWithDefaultCaseType =
        await this.mailboxService.findAllAsync({
          defaultCaseType: caseTypeStr,
        });
      if (mailboxesWithDefaultCaseType.length === 1) {
        this.logger.debug(
          `using the default mailbox: ${mailboxesWithDefaultCaseType[0].email} to send email for case type: ${caseTypeStr}!`
        );
        return mailboxesWithDefaultCaseType[0].email;
      }

      this.logger.warn(
        `Multiple mailboxes found for caseType: ${caseTypeStr}, not possible to filter them done to one!`
      );

      return null;
    } else {
      // coming here means there is only one mailbox, so safe to use it.
      this.logger.debug(
        `using the default mailbox: ${mailboxes[0].email} to send email for case type: ${caseTypeStr}!`
      );
      return mailboxes[0].email;
    }
  }

  async sendEmailNoCaseId(sendEmailDto: SendEmail, requesterUserId?: string) {
    const fromMailbox = sendEmailDto.fromMailbox;
    if (!fromMailbox) {
      const errorMessage = `Not possible to send new email as mailbox to send the email from is not found`;
      this.logger.error(errorMessage);
      throw new BadRequestException(errorMessage);
    }

    const message =
      this.emailContentProvider.getEmailBodyInHtmlForEmailNoCaseId(
        sendEmailDto.body
      );
    sendEmailDto.toRecipients =
      await this.contactListProvider.expandedRecipientEmails(
        sendEmailDto.toRecipients
      );
    sendEmailDto.ccRecipients =
      await this.contactListProvider.expandedRecipientEmails(
        sendEmailDto.ccRecipients
      );
    sendEmailDto.bccRecipients =
      await this.contactListProvider.expandedRecipientEmails(
        sendEmailDto.bccRecipients
      );
    const success = await this.emailService.sendEmail(
      sendEmailDto.fromMailbox,
      sendEmailDto.toRecipients,
      sendEmailDto.ccRecipients,
      sendEmailDto.bccRecipients,
      sendEmailDto.subject,
      message,
      sendEmailDto.attachments
    );
    if (!success) {
      this.logger.error(`Failed to send New email without CaseId`);
      throw new Error(`Failed to send New email without CaseId`);
    }
  }

  async sendNewEmail(
    caseId: string,
    sendEmailDto: SendEmail,
    requesterUserId?: string
  ) {
    const opxCase: CaseDocument = await this.caseService.findByIdDetail(caseId);

    if (!opxCase) {
      throw new Error(`opxCase[${caseId}] not found!`);
    }

    const message = this.emailContentProvider.getEmailBodyInHtml(
      caseId,
      sendEmailDto.body
    );
    const fromMailbox = sendEmailDto.fromMailbox;
    let mailbox =
      fromMailbox ?? findMailbox(opxCase.caseDetails.emailConversations);

    if (mailbox == null) {
      const caseType = opxCase.caseType;
      mailbox = await this.getMailbox(caseType);
    }

    if (mailbox == null) {
      const errorMessage = `Not possible to send new email as mailbox to send the email from is not found for case ${opxCase._id}`;
      this.logger.error(errorMessage);
      throw new BadRequestException(errorMessage);
    }

    sendEmailDto.toRecipients =
      await this.contactListProvider.expandedRecipientEmails(
        sendEmailDto.toRecipients
      );
    sendEmailDto.ccRecipients =
      await this.contactListProvider.expandedRecipientEmails(
        sendEmailDto.ccRecipients
      );
    sendEmailDto.bccRecipients =
      await this.contactListProvider.expandedRecipientEmails(
        sendEmailDto.bccRecipients
      );

    const success = await this.emailService.sendEmail(
      mailbox,
      sendEmailDto.toRecipients,
      sendEmailDto.ccRecipients,
      sendEmailDto.bccRecipients,
      sendEmailDto.subject,
      message,
      sendEmailDto.attachments
    );
    if (success) {
      // get full details of requester
      const requesterDetails = await this.userDetails(requesterUserId);
      //Log Audit
      await this.activityNoteService.create({
        entity: BaseService.toObjectId(caseId),
        user: requesterUserId || 'System',
        title: 'New Email Sent',
        details: `${requesterDetails} has sent a new email. \nTo: ${
          sendEmailDto.toRecipients || '-'
        } \nCc: ${sendEmailDto.ccRecipients || '-'} \nBcc: ${
          sendEmailDto.bccRecipients || '-'
        }`,
        component: 'email-api',
      });
    } else {
      this.logger.error(`Failed to send email for the case: ${caseId}`);
      throw new Error(`Failed to send email for the opxCase: ${caseId}`);
    }
  }

  async createDraftEmail(
    caseId: string,
    createDraftEmail: CreateDraftEmail,
    requesterUserId?: string //not used yet!
  ): Promise<CreateDraftEmailResponse> {
    const opxCase: CaseDocument = await this.caseService.findByIdDetail(caseId);

    if (!opxCase) {
      throw new Error(`opxCase[${caseId}] not found!`);
    }

    if (!createDraftEmail.emailId) {
      return {
        success: false,
        error: {
          code: EmailServiceErrorCodes.ITEM_NOT_FOUND,
          message: `Creating draft email for new email is not supported yet!`,
        },
      };
    }

    const emailIdInput = createDraftEmail.emailId;
    const mailbox = findMailbox(
      opxCase.caseDetails.emailConversations,
      emailIdInput
    );

    const emailId = await this.currentOrNewEmailId(
      mailbox,
      emailIdInput,
      undefined,
      opxCase
    );

    if (!emailId) {
      return {
        success: false,
        error: {
          code: EmailServiceErrorCodes.ITEM_NOT_FOUND,
          message: `Email[${emailIdInput}] is not found in the vendor store any more!`,
        },
      };
    }

    const draftEmail = await this.emailService.createDraftEmail({
      createDraftEmail: {
        ...createDraftEmail,
        emailId: emailId,
        fromMailbox: mailbox,
      },
    });

    return this.emailContentProvider.updateDraftWithCaseId(caseId, draftEmail);
  }

  async forwardEmail(
    caseId: string,
    forwardEmailDto: ForwardEmail,
    requesterUserId?: string
  ) {
    const opxCase: CaseDocument = await this.caseService.findByIdDetail(caseId);

    if (!opxCase) {
      throw new Error(`opxCase[${caseId}] not found!`);
    }

    const message = this.emailContentProvider.getEmailBodyInHtml(
      caseId,
      forwardEmailDto.body
    );
    const newBody = {
      contentType: 'html',
      content: message,
      includesQuote:
        forwardEmailDto?.body?.includesQuote == null
          ? forwardEmailDto.draftEmailId != null // if draftId is present, it is assumed that user is sending the full email with quoted text/history.
          : forwardEmailDto?.body?.includesQuote,
    };

    const emailIdInput = forwardEmailDto.emailId;
    const mailbox = findMailbox(
      opxCase.caseDetails.emailConversations,
      emailIdInput
    );

    const emailId = await this.currentOrNewEmailId(
      mailbox,
      emailIdInput,
      undefined,
      opxCase
    );

    if (!emailId) {
      throw new Error(
        `Email[${emailIdInput}] is not found in the vendor store any more!`
      );
    }

    forwardEmailDto.toRecipients =
      await this.contactListProvider.expandedRecipientEmails(
        forwardEmailDto.toRecipients
      );
    forwardEmailDto.ccRecipients =
      await this.contactListProvider.expandedRecipientEmails(
        forwardEmailDto.ccRecipients
      );
    forwardEmailDto.bccRecipients =
      await this.contactListProvider.expandedRecipientEmails(
        forwardEmailDto.bccRecipients
      );

    const emailServiceResponse = await this.emailService.sendForwardDraftEmail({
      draftEmail: {
        ...forwardEmailDto,
        emailId: emailId,
        fromMailbox: mailbox,
        body: newBody,
      },
    });

    if (emailServiceResponse.success) {
      // get full details of requester
      const requesterDetails = await this.userDetails(requesterUserId);
      //Log Audit
      await this.activityNoteService.create({
        entity: BaseService.toObjectId(caseId),
        user: requesterUserId || 'System',
        title: `Email Forwarded`,
        details: `${requesterDetails} has forwarded an email. \nTo: ${
          forwardEmailDto.toRecipients || '-'
        } \nCc: ${forwardEmailDto.ccRecipients || '-'} \nBcc: ${
          forwardEmailDto.bccRecipients || '-'
        }`,
        component: 'email-api',
      });
    } else {
      const errorMessage = `Failed to forward email for the case: ${
        opxCase._id
      }, error is: ${JSON.stringify(emailServiceResponse?.error)}`;
      this.logger.error(errorMessage);
      throw new Error(errorMessage);
    }
  }
}
