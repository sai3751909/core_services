import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import {
  AuthModule,
  AUTH_SERVICE_CONFIG,
  CoreModule,
  EmailModule,
  CaseModule,
  UserModule,
  UserTaskModule,
  ProcessInstanceModule,
  CaseTypeModule,
  HealthReporterModule,
  AuditModule,
  UserGuard,
  MailboxModule,
  ContactListModule,
  EmailIndexModule,
  OpsmatixPrometheusModule,
} from '@opsmatix/core';
import { environment } from '../environments/environment';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TerminusModule } from '@nestjs/terminus';
import { HealthController } from './health.controller';
import { EmailContentProvider } from './email-content.provider';
import { ContactListProvider } from './contact-list.provider';
import { TemplatesModule } from './templates/templates.module';
import EmailIdSuggestionService from './email-id-suggestion.service';
import { APP_GUARD } from '@nestjs/core';
import { ScheduleModule } from 'nest-schedule';
import { AppScheduler } from './app.scheduler';

@Module({
  imports: [
    TerminusModule,
    HealthReporterModule,
    CoreModule.forRoot(environment.mongodb.url),
    UserModule,
    AuthModule.forRoot(AUTH_SERVICE_CONFIG),
    CaseModule,
    CaseTypeModule,
    MailboxModule,
    ProcessInstanceModule,
    UserTaskModule,
    TemplatesModule,
    AuditModule,
    HttpModule,
    EmailModule.forRoot(
      environment.emailServiceConfig.provider,
      environment.emailServiceConfig.config,
      environment.mongodb.url
    ),
    ContactListModule,
    ScheduleModule,
    EmailIndexModule,
    OpsmatixPrometheusModule,
  ],
  controllers: [AppController, HealthController],
  providers: [
    AppService,
    AppScheduler,
    EmailContentProvider,
    ContactListProvider,
    EmailIdSuggestionService,
    {
      provide: APP_GUARD,
      useClass: UserGuard,
    },
  ],
})
export class AppModule {}
