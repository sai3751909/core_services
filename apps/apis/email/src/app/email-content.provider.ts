import { Injectable, Logger } from '@nestjs/common';
import { CreateDraftEmailResponse, OpsmatixLogger } from '@opsmatix/core';
import {
  EmailBody,
  CASE_ID_START_TAG,
  CASE_ID_END_TAG,
} from '@opsmatix/interfaces';
import { escape } from 'lodash';

@Injectable()
export class EmailContentProvider {
  private readonly logger: Logger = new OpsmatixLogger(
    EmailContentProvider.name
  );
  getEmailBodyInHtmlForEmailNoCaseId(emailBody: EmailBody): string {
    let message: string;
    if (emailBody.base64Content) {
      message = Buffer.from(emailBody.base64Content, 'base64').toString('utf8');
    } else if (emailBody.content) {
      message = emailBody.content;
    } else {
      throw new Error(`No email body found!`);
    }

    if (!emailBody.contentType.toLowerCase().includes('html')) {
      message =
        '<p>' +
        message.replace(/\n{2,}/g, '</p><p>').replace(/\n/g, '<br>') +
        '</p>';
    }
    return message;
  }

  getEmailBodyInHtml(caseId: string, emailBody: EmailBody): string {
    let message: string;
    if (emailBody.base64Content) {
      message = Buffer.from(emailBody.base64Content, 'base64').toString('utf8');
    } else if (emailBody.content) {
      message = emailBody.content;
    } else {
      throw new Error(`No email body found!`);
    }

    if (!emailBody.contentType.toLowerCase().includes('html')) {
      message =
        '<p>' +
        message.replace(/\n{2,}/g, '</p><p>').replace(/\n/g, '<br>') +
        '</p>';
    }

    const caseStamp = `${CASE_ID_START_TAG}${caseId}${CASE_ID_END_TAG}`;
    const caseStampEscaped = escape(caseStamp);

    if (!message.includes(caseStamp) && !message.includes(caseStampEscaped)) {
      message = message + this.getCaseIdHtml(caseId);
    }

    return message;
  }

  getCaseIdHtml(caseId: string): string {
    const caseStamp = `${CASE_ID_START_TAG}${caseId}${CASE_ID_END_TAG}`;
    return `<p style="font-size:1px">${caseStamp}</p>`;
  }

  updateDraftWithCaseId(
    caseId: string,
    draftResponse: CreateDraftEmailResponse
  ): CreateDraftEmailResponse {
    if (
      !draftResponse.success ||
      !caseId ||
      !draftResponse?.draftEmail?.body?.content
    ) {
      return draftResponse;
    }

    const orgContent = draftResponse.draftEmail.body.content;
    const caseIds = this.extractCaseIds(orgContent);
    if (
      !caseIds ||
      caseIds.length === 0 ||
      (caseIds.length > 0 && caseIds[0] !== caseId)
    ) {
      const newContent = this.getCaseIdHtml(caseId) + orgContent;
      draftResponse.draftEmail.body.content = newContent;
    }
    return draftResponse;
  }

  private extractCaseIds(strToExtractFrom: string) {
    const allMatchedArrays = [
      ...(' ' + strToExtractFrom + ' ').matchAll(/[^\w](\w{24})[^\w]/g),
    ]; //adding space to make regex work..
    const matched = allMatchedArrays
      .filter((a) => a.length > 1 && a[1])
      .map((a) => a[1]);

    this.logger.debug(`Extracted case ids: ${JSON.stringify(matched)}`);
    return matched ? matched : [];
  }
}
