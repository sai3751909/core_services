import { Injectable } from '@nestjs/common';
import { Interval, NestSchedule } from 'nest-schedule';
import EmailIdSuggestionService from './email-id-suggestion.service';

@Injectable()
export class AppScheduler extends NestSchedule {
  constructor(
    private readonly emailIdSuggestionService: EmailIdSuggestionService
  ) {
    super();
  }

  @Interval(5 * 60 * 1000, { waiting: true })
  async loadContactLists() {
    return this.emailIdSuggestionService.loadContactLists();
  }
}
