import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  NotFoundException,
  Param,
  Query,
  Post,
  Res,
  Req,
  Header,
  HttpStatus,
  Head,
  InternalServerErrorException,
} from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import {
  AllowUnauthorizedRequest,
  EmailService,
  EmailServiceErrorCodes,
  OpsmatixLogger,
  RequiredFeaturePermission,
} from '@opsmatix/core';
import {
  CreateDraftEmail,
  FeaturePermissions,
  ForwardEmail,
  Omnichannel,
  ReplyEmail,
  ReplyToAllEmail,
  SendEmail,
} from '@opsmatix/interfaces';
import { AppService } from './app.service';
import { Response } from 'express';
import EmailIdSuggestionService from './email-id-suggestion.service';
@Controller()
export class AppController {
  private readonly logger: Logger = new OpsmatixLogger(
    `Email Controller: ${AppController.name}`
  );

  constructor(
    private readonly appService: AppService,
    private readonly emailService: EmailService,
    private readonly emailIdSuggestionService: EmailIdSuggestionService
  ) {}

  @Get('latestEmailTimestamp/:mailboxUser')
  @Header(
    'Cache-Control',
    'max-age=0, no-cache, no-store, private, must-revalidate'
  )
  async getLatestEmailTimestamp(@Param('mailboxUser') mailboxUser: string) {
    this.logger.debug(`getLatestEmailTimestamp :${mailboxUser}`);
    return this.emailService.getLatestEmailTimestamp(mailboxUser);
  }

  @Get('mailFolders/:mailboxUser')
  @Header(
    'Cache-Control',
    'max-age=0, no-cache, no-store, private, must-revalidate'
  )
  @ApiBearerAuth()
  async getMailFolders(@Param('mailboxUser') mailboxUser: string) {
    this.logger.debug(`getMailFolders :${mailboxUser}`);
    return this.emailService.getMailFolders(mailboxUser);
  }

  @Get('mailFolders/:mailboxUser/:parentFolderId')
  @Header(
    'Cache-Control',
    'max-age=0, no-cache, no-store, private, must-revalidate'
  )
  @ApiBearerAuth()
  async getMailFoldersOfParentFolder(
    @Param('mailboxUser') mailboxUser: string,
    @Param('parentFolderId') parentFolderId: string
  ) {
    this.logger.debug(`getMailFolders :${mailboxUser}`);
    return this.emailService.getMailFolders(mailboxUser, parentFolderId);
  }

  @Get('poll/:mailboxUser/:fromTime')
  @Header(
    'Cache-Control',
    'max-age=0, no-cache, no-store, private, must-revalidate'
  )
  @ApiBearerAuth()
  async pollEmails(
    @Param('mailboxUser') mailboxUser: string,
    @Param('fromTime') fromTime: string
  ) {
    this.logger.debug(`poll emails :${mailboxUser} - ${fromTime}`);
    return this.emailService.getEmails(mailboxUser, fromTime);
  }

  @Get('poll-folder/:mailboxUser/:fromTime/:mailFolderId')
  @Header(
    'Cache-Control',
    'max-age=0, no-cache, no-store, private, must-revalidate'
  )
  @ApiBearerAuth()
  async pollEmailsFromFolder(
    @Param('mailboxUser') mailboxUser: string,
    @Param('fromTime') fromTime: string,
    @Param('mailFolderId') mailFolderId: string
  ) {
    this.logger.debug(
      `poll emails :${mailboxUser} - ${fromTime} - ${mailFolderId}`
    );
    return this.emailService.getEmailsFromMailFolder(
      mailboxUser,
      fromTime,
      mailFolderId
    );
  }

  @Get('poll-conversation/:mailboxUser/:conversationId')
  @Header(
    'Cache-Control',
    'max-age=0, no-cache, no-store, private, must-revalidate'
  )
  @ApiBearerAuth()
  async pollEmailsFromConversation(
    @Param('mailboxUser') mailboxUser: string,
    @Param('conversationId') conversationId: string
  ) {
    this.logger.debug(`poll emails :${mailboxUser} - ${conversationId}`);
    return this.emailService.getEmailsFromConversation(
      mailboxUser,
      conversationId
    );
  }

  /**
   * @deprecated Use getEmailContent() instead
   */
  @Get('html/:caseId/:emailId')
  @RequiredFeaturePermission(FeaturePermissions.Omnichannel.Email.Read)
  @ApiBearerAuth()
  async getEmailAsHtmlFormat(
    @Param('caseId') caseId: string,
    @Param('emailId') emailId: string,
    @Query() params: { replaceCidWithImageUrl: string },
    @Res() response
  ) {
    return this.getEmailContent(
      Omnichannel.ContentType.Html,
      caseId,
      emailId,
      params,
      response
    );
  }

  @Get('content/:format/:caseId/:emailId')
  @ApiBearerAuth()
  @AllowUnauthorizedRequest()
  async getEmailContent(
    @Param('format') format: Omnichannel.ContentType,
    @Param('caseId') caseId: string,
    @Param('emailId') emailId: string,
    @Query() params: { replaceCidWithImageUrl: string },
    @Res() response
  ) {
    try {
      this.logger.debug(`caseId:${caseId}, emailId:${emailId}`);
      const content = await this.appService.getEmailContent(
        caseId,
        emailId,
        format,
        params?.replaceCidWithImageUrl === 'true' ? true : false
      );
      return response.json({ content: content });
    } catch (err) {
      const message = `Email could not be fetched! - ${err.message}`;
      this.logger.log(message);
      throw new NotFoundException('Email could not be fetched!');
    }
  }

  @Get('/mailboxes/:mailbox/emails/:emailId/content/:format')
  @ApiBearerAuth()
  @AllowUnauthorizedRequest()
  async getEmailContentFromMailbox(
    @Param('format') format: Omnichannel.ContentType,
    @Param('mailbox') mailbox: string,
    @Param('emailId') emailId: string,
    @Query() params: { replaceCidWithImageUrl: string },
    @Res() response
  ) {
    try {
      this.logger.debug(`mailbox:${mailbox}, emailId:${emailId}`);
      const content = await this.appService.getEmailContentFromMailbox(
        mailbox,
        emailId,
        format,
        params.replaceCidWithImageUrl === 'true' ? true : false
      );
      return response.json({ content: content });
    } catch (err) {
      const message = `Email could not be fetched! - ${err.message}`;
      this.logger.log(message);
      throw new NotFoundException('Email could not be fetched!');
    }
  }
  @Get('/mailboxes/:mailbox/emails/:emailId/email-type-info')
  @RequiredFeaturePermission(FeaturePermissions.Omnichannel.Email.Read)
  @ApiBearerAuth()
  async getEmailTypeInfoFromMailbox(
    @Param('mailbox') mailbox: string,
    @Param('emailId') emailId: string
  ) {
    this.logger.verbose(`mailbox:${!!mailbox}, emailId:${!!emailId}`);
    if (
      mailbox == null ||
      mailbox.trim() == '' ||
      emailId == null ||
      emailId.trim() == ''
    ) {
      throw new BadRequestException('mailbox and emailId are required!');
    }

    try {
      // sanitise inputs
      mailbox = mailbox.trim();
      emailId = emailId.trim();
      this.logger.debug(
        `email-type-info request: mailbox:${mailbox}, emailId:${emailId}`
      );
      const response = await this.appService.getEmailTypeInfoFromMailbox(
        mailbox,
        emailId
      );
      this.logger.debug(
        `email-type-info response: ${JSON.stringify(response)}`
      );

      if (response == null) {
        throw new NotFoundException(
          `Email[${emailId}] is not found in the vendor store any more!`
        );
      }
      return response;
    } catch (err) {
      if (err instanceof NotFoundException) {
        throw err;
      }
      const message = `Email Type Info could not be fetched! - ${err.message}`;
      this.logger.error(message, err.stack);
      throw new NotFoundException('Email Type Info could not be fetched!');
    }
  }

  @Get('/mailboxes/:mailbox/emails/:emailId/internet-message-headers')
  @RequiredFeaturePermission(FeaturePermissions.Omnichannel.Email.Read)
  @ApiBearerAuth()
  async getEmailInternetMessageHeaders(
    @Param('mailbox') mailbox: string,
    @Param('emailId') emailId: string
  ) {
    this.logger.verbose(`mailbox:${!!mailbox}, emailId:${!!emailId}`);
    if (
      mailbox == null ||
      mailbox.trim() == '' ||
      emailId == null ||
      emailId.trim() == ''
    ) {
      throw new BadRequestException('mailbox and emailId are required!');
    }

    try {
      // sanitise inputs
      mailbox = mailbox.trim();
      emailId = emailId.trim();
      this.logger.debug(
        `internet-message-headers request: mailbox:${mailbox}, emailId:${emailId}`
      );
      const response = await this.appService.getEmailInternetMessageHeaders(
        mailbox,
        emailId
      );
      this.logger.debug(
        `internet-message-headers response: ${JSON.stringify(response)}`
      );

      if (response == null) {
        throw new NotFoundException(
          `Email[${emailId}] is not found in the vendor store any more!`
        );
      }
      return response;
    } catch (err) {
      if (err instanceof NotFoundException) {
        throw err;
      }
      const message = `Email Type Info could not be fetched! - ${err.message}`;
      this.logger.error(message, err.stack);
      throw new NotFoundException('Email Type Info could not be fetched!');
    }
  }

  @Get('email-id-suggestions')
  @Header(
    'Cache-Control',
    'max-age=0, no-cache, no-store, private, must-revalidate'
  )
  @RequiredFeaturePermission(FeaturePermissions.Omnichannel.Email.Read)
  @ApiBearerAuth()
  async getAutocompletedEmailAddresses(
    @Query('queryValue') queryValue: string
  ): Promise<string[]> {
    return this.emailIdSuggestionService.getSuggestions(queryValue);
  }

  @Head('check/:caseId/:mailboxUser/:emailId')
  @Header(
    'Cache-Control',
    'max-age=0, no-cache, no-store, private, must-revalidate'
  )
  @RequiredFeaturePermission(FeaturePermissions.Omnichannel.Email.Read)
  @ApiBearerAuth()
  async checkEmailExists(
    @Param('caseId') caseId: string,
    @Param('mailboxUser') mailboxUser: string,
    @Param('emailId') emailId: string,
    @Res() response
  ) {
    try {
      const emailIdIfExists = await this.appService.currentOrNewEmailId(
        mailboxUser,
        emailId,
        caseId,
        undefined
      );
      if (emailIdIfExists) {
        return response.status(HttpStatus.OK).json();
      }
    } catch (err) {
      const message = `Email could not be fetched! - ${err.message}`;
      this.logger.log(message);
    }

    return response.status(HttpStatus.NOT_FOUND).json();
  }

  @Get('attachments-metadata/:caseId/:mailboxUser/:emailId')
  @RequiredFeaturePermission(FeaturePermissions.Omnichannel.Email.Read)
  @ApiBearerAuth()
  async getEmailAttachmentsMetadataWithCaseId(
    @Param('caseId') caseId: string,
    @Param('mailboxUser') mailboxUser: string,
    @Param('emailId') emailId: string,
    @Res() response
  ) {
    try {
      this.logger.debug(
        `caseId:${caseId}mailboxUser:${mailboxUser}, emailId:${emailId}`
      );
      const attachments = await this.appService.getEmailAttachmentsMetadata(
        mailboxUser,
        emailId,
        caseId
      );
      return response.json({ attachments: attachments });
    } catch (err) {
      const message = `email attachment metadata could not be fetched! - ${err.message}`;
      this.logger.error(message, err.stack);

      throw new NotFoundException(message);
    }
  }

  /**
   * @deprecated Use getEmailAttachmentsMetadataWithCaseId() instead as this does not support emails which are moved to different folders in outlook.
   * This is kept around for backward compatibility.
   */
  @Get('attachments-metadata/:mailboxUser/:emailId')
  @RequiredFeaturePermission(FeaturePermissions.Omnichannel.Email.Read)
  @ApiBearerAuth()
  async getEmailAttachmentsMetadata(
    @Param('mailboxUser') mailboxUser: string,
    @Param('emailId') emailId: string,
    @Res() response
  ) {
    return this.getEmailAttachmentsMetadataWithCaseId(
      null,
      mailboxUser,
      emailId,
      response
    );
  }

  @Get('attachment/:caseId/:mailbox/:emailId/:attachmentId')
  @RequiredFeaturePermission(FeaturePermissions.Omnichannel.Email.Read)
  async getEmailAttachmentWithCaseId(
    @Param('caseId') caseId: string,
    @Param('mailbox') mailbox: string,
    @Param('emailId') emailId: string,
    @Param('attachmentId') attachmentId: string,
    @Res() response: Response
  ) {
    const cid = await this.appService.getEmailAttachment(
      mailbox,
      emailId,
      attachmentId,
      caseId
    );
    if (cid) {
      response.set({
        'Content-Type': cid.contentType ?? 'application/octet-stream',
        'Content-Disposition': `attachment; filename="${encodeURI(cid.name)}"`,
        'Response-Type': 'text',
      });
      response.end(Buffer.from(cid.base64Content, 'base64'));
    } else {
      throw new BadRequestException(`Could not find the requested content!`);
    }
  }

  /**
   * @deprecated Use getEmailAttachmentWithCaseId() instead as this does not support emails which are moved to different folders in outlook.
   * This is kept around for backward compatibility.
   */
  @Get('attachment/:mailbox/:emailId/:attachmentId')
  @RequiredFeaturePermission(FeaturePermissions.Omnichannel.Email.Read)
  async getEmailAttachment(
    @Param('mailbox') mailbox: string,
    @Param('emailId') emailId: string,
    @Param('attachmentId') attachmentId: string,
    @Res() response: Response
  ) {
    return this.getEmailAttachmentWithCaseId(
      null,
      mailbox,
      emailId,
      attachmentId,
      response
    );
  }

  @Get('attachments/:mailboxUser/:emailId')
  @RequiredFeaturePermission(FeaturePermissions.Omnichannel.Email.Read)
  @ApiBearerAuth()
  async getEmailAttachments(
    @Param('mailboxUser') mailboxUser: string,
    @Param('emailId') emailId: string,
    @Res() response
  ) {
    try {
      this.logger.debug(`mailboxUser:${mailboxUser}, emailId:${emailId}`);
      const attachments = await this.appService.getEmailAttachments(
        mailboxUser,
        emailId
      );
      return response.json({ attachments: attachments });
    } catch (err) {
      const message = `Email attachment could not be fetched! - ${err.message}`;
      this.logger.error(message, err.stack);
      throw new NotFoundException('email could not be fetched!');
    }
  }

  @Get('mime/:caseId/:emailId')
  @RequiredFeaturePermission(FeaturePermissions.Omnichannel.Email.Read)
  @ApiBearerAuth()
  async getEmailInMime(
    @Param('caseId') caseId: string,
    @Param('emailId') emailId: string
  ) {
    try {
      this.logger.debug(`caseId:${caseId}, emailId:${emailId}`);
      const content = await this.appService.getEmailInMime(caseId, emailId);
      return content;
    } catch (err) {
      const message = `Email Mime could not be fetched! - ${err.message}`;
      this.logger.log(message);
      throw new NotFoundException('email could not be fetched!');
    }
  }

  @Post('sendNew/:caseId')
  @RequiredFeaturePermission(FeaturePermissions.Omnichannel.Email.Update)
  @ApiBearerAuth()
  async sendNewEmail(
    @Param('caseId') caseId: string,
    @Body() sendEmailDto: SendEmail,
    @Req() request
  ) {
    return this.appService.sendNewEmail(
      caseId,
      sendEmailDto,
      request.params.auth_user._id
    );
  }
  @Post('sendEmailNoCaseId')
  @RequiredFeaturePermission(FeaturePermissions.Omnichannel.Email.Update)
  @ApiBearerAuth()
  async sendEmailNoCaseId(@Body() sendEmailDto: SendEmail, @Req() request) {
    return this.appService.sendEmailNoCaseId(
      sendEmailDto,
      request.params.auth_user._id
    );
  }

  @Post('forward/:caseId')
  @RequiredFeaturePermission(FeaturePermissions.Omnichannel.Email.Update)
  @ApiBearerAuth()
  async forwardEmail(
    @Param('caseId') caseId: string,
    @Body() forwardEmailDto: ForwardEmail,
    @Req() request
  ) {
    return this.appService.forwardEmail(
      caseId,
      forwardEmailDto,
      request.params.auth_user._id
    );
  }

  @Post('replyToAll/:caseId')
  @RequiredFeaturePermission(FeaturePermissions.Omnichannel.Email.Update)
  @ApiBearerAuth()
  async replyToAllEmail(
    @Param('caseId') caseId: string,
    @Body() replyToAllEmailDto: ReplyToAllEmail,
    @Req() request
  ) {
    return this.appService.replyToAllEmail(
      caseId,
      replyToAllEmailDto,
      request.params.auth_user._id
    );
  }

  @Post('reply/:caseId')
  @RequiredFeaturePermission(FeaturePermissions.Omnichannel.Email.Update)
  @ApiBearerAuth()
  async replyToEmail(
    @Param('caseId') caseId: string,
    @Body() replyEmailDto: ReplyEmail,
    @Req() request
  ) {
    return this.appService.replyToEmail(
      caseId,
      replyEmailDto,
      request.params.auth_user._id
    );
  }

  @Post('createDraft/:caseId')
  @RequiredFeaturePermission(FeaturePermissions.Omnichannel.Email.Update)
  @ApiBearerAuth()
  async createDraftEmail(
    @Param('caseId') caseId: string,
    @Body() createDraftEmail: CreateDraftEmail,
    @Req() request
  ) {
    const response = await this.appService
      .createDraftEmail(caseId, createDraftEmail, request.params.auth_user._id)
      .catch((error) => {
        return {
          success: false,
          error: {
            code: EmailServiceErrorCodes.UNKNOWN,
            message: `Failed to create draft email for ${createDraftEmail?.emailId}]! error is: ${error.message}`,
          },
        };
      });

    if (response?.success) {
      return response;
    } else {
      if (response?.error?.code === EmailServiceErrorCodes.ITEM_NOT_FOUND) {
        // throw new NotFoundException(response);
        //FIXME: UI is not currently handling 404 errors properly, hence responding.
        return response;
      }
      if (
        response?.error?.code === EmailServiceErrorCodes.SEND_EMAILS_DISABLED
      ) {
        throw new BadRequestException(response);
      } else {
        throw new InternalServerErrorException(response);
      }
    }
  }

  @Delete('drafts/:mailbox/:draftId')
  @RequiredFeaturePermission(FeaturePermissions.Omnichannel.Email.Update)
  @ApiBearerAuth()
  async deleteDraftEmail(
    @Param('mailbox') mailbox: string,
    @Param('draftId') draftId: string
  ) {
    return this.emailService.deleteDraftEmail({
      deleteDraftEmail: {
        fromMailbox: mailbox,
        draftEmailId: draftId,
      },
    });
  }
}
