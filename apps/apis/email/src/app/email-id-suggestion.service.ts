import { Logger, Injectable, NotFoundException } from '@nestjs/common';
import { GlobalConfig, OpsmatixLogger } from '@opsmatix/core';
import { Omnichannel } from '@opsmatix/interfaces';
import {
  Consumer,
  ConsumerSubscribeTopic,
  EachBatchPayload,
  Kafka,
} from 'kafkajs';
import { environment } from '../environments/environment';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { Document } = require('flexsearch');

import { ContactListService } from '@opsmatix/core';

@Injectable()
export default class EmailIdSuggestionService {
  private readonly logger: Logger = new OpsmatixLogger(
    `${EmailIdSuggestionService.name}`
  );

  private kafkaConsumer: Consumer;
  private healthy = true;
  private undeliverableEmailSet = new Set<string>();
  private readonly index = new Document({
    document: {
      id: 'email',
      index: ['email', 'name'],
    },
    tokenize: 'full',
    optimize: true,
  });

  public constructor(private readonly contactListService: ContactListService) {
    if (environment.emailIdSuggestionsEnabled) {
      this.kafkaConsumer = this.createKafkaConsumer();
      this.startConsumer().catch((err) => {
        this.logger.error(`Failed to start consumer`, err.stack);
      });
      this.logger.log(`EmailId Suggestions Service is enabled!`);
    } else {
      this.logger.log(`EmailId Suggestions Service is disabled!`);
    }
    this.loadContactLists().catch((err) => {
      this.logger.error(`Failed to load contact lists`, err.stack);
    });
  }

  async loadContactLists() {
    if (environment.emailIdSuggestionsEnabled) {
      this.logger.log(`Loading contact lists...`);
      return this.contactListService
        .findAllAsync()
        .then((contactLists) => {
          contactLists.forEach((contactList) => {
            this.index.add({
              email: contactList.name,
              name: '',
            });
          });
          this.logger.log(`Contact lists loaded successfully!`);
        })
        .catch((err) => {
          this.logger.error(`Failed to load contact lists`, err.stack);
        });
    }
  }

  private async startConsumer(): Promise<void> {
    const topic: ConsumerSubscribeTopic = {
      topic: GlobalConfig.kafka_topics.omnichannel_ingestion,
      fromBeginning: true,
    };

    try {
      await this.kafkaConsumer.connect();
      this.kafkaConsumer.on(this.kafkaConsumer.events.CRASH, (e) => {
        this.logger.error(`Kafka consumer crashed.`, null);
        this.healthy = false;
      });
      this.kafkaConsumer.on(this.kafkaConsumer.events.GROUP_JOIN, (e) => {
        this.logger.log(`Kafka consumer joined group.`, null);
        this.healthy = true;
      });
      await this.kafkaConsumer.subscribe(topic);

      await this.kafkaConsumer.run({
        autoCommit: false,
        eachBatch: async (eachBatchPayload: EachBatchPayload) => {
          const { batch } = eachBatchPayload;
          for (const message of batch.messages) {
            try {
              this.processKafkaMessage(
                JSON.parse(message.value.toString()) as any
              );
            } catch (error) {
              //ignore parsing errors.
            }
          }
          this.logger.debug(
            `No of emails processed: ${batch?.messages?.length}`
          );
        },
      });
    } catch (error) {
      this.healthy = false;
      this.logger.error('Error: ', error);
    }
  }

  getSuggestions(query: string) {
    const searchResults = this.index.search(query, {
      limit: 20,
      enrich: true, //this is not working, not returning the full document
    });

    let result =
      searchResults
        .filter((a) => a.result && a.result.length > 0)
        .flatMap((a) => a.result) ?? [];
    result = [...new Set(result)].slice(0, 10);

    return result;
  }
  private processKafkaMessage(emailMessage: Omnichannel.Email) {
    const subject = emailMessage.subject || '';
    if (subject.toLowerCase().includes('undeliverable:')) {
      emailMessage?.toRecipients?.forEach((r) => {
        this.depopulateIndex(r);
      });
      this.depopulateIndex(emailMessage?.from);
    } else {
      this.populateIndex(emailMessage?.from);
      emailMessage?.toRecipients?.forEach((r) => {
        this.populateIndex(r);
      });

      emailMessage?.ccRecipients?.forEach((r) => {
        this.populateIndex(r);
      });

      emailMessage?.bccRecipients?.forEach((r) => {
        this.populateIndex(r);
      });
    }
  }

  populateIndex(address: Omnichannel.Email.Address): void {
    if (address?.emailAddress?.address) {
      if (
        !this.undeliverableEmailSet.has(
          address?.emailAddress?.address.toLowerCase()
        )
      ) {
        this.index.add({
          email: address.emailAddress.address.toLowerCase(),
          name: address.emailAddress.name ?? '',
        });
      }
    }
  }
  depopulateIndex(address: Omnichannel.Email.Address): void {
    const lowerCaseEmail = address?.emailAddress?.address?.toLowerCase();
    if (lowerCaseEmail) {
      this.undeliverableEmailSet.add(lowerCaseEmail);
      const searchMatches = this.index.search({
        field: 'email',
        query: lowerCaseEmail,
      });
      for (const match of searchMatches) {
        const emails = match.result;
        for (const email of emails) {
          if (this.undeliverableEmailSet.has(email)) {
            this.index.remove(email);
          }
        }
      }
    }
  }

  public async shutdown(): Promise<void> {
    await this.kafkaConsumer.disconnect();
  }

  private createKafkaConsumer(): Consumer {
    const consumerGroupId = GlobalConfig.namespace + '-email-id-suggestions';
    const kafka = new Kafka({
      clientId: consumerGroupId,
      brokers: GlobalConfig.kafka.brokers,
      ssl: GlobalConfig.kafka.sslEnabled,
    });
    const consumer = kafka.consumer({
      groupId: consumerGroupId + '-' + new Date().getTime(), //this allows multiple cache apis to consume from same topic.,
    });
    return consumer;
  }

  public isHealthy() {
    return this.healthy;
  }
}
