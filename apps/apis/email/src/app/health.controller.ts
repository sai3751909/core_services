import { Controller, Get } from '@nestjs/common';
import {
  HealthCheck,
  HealthCheckService,
  MongooseHealthIndicator,
} from '@nestjs/terminus';
import {
  AllowUnauthorizedRequest,
  HealthReporterService,
} from '@opsmatix/core';
import EmailIdSuggestionService from './email-id-suggestion.service';

@Controller('health')
export class HealthController {
  constructor(
    private health: HealthCheckService,
    private reporter: HealthReporterService,
    private mongooseHealth: MongooseHealthIndicator,
    private emailIdSuggestionService: EmailIdSuggestionService
  ) {}

  @Get()
  @HealthCheck()
  @AllowUnauthorizedRequest()
  async check() {
    const checkResult = await this.health.check([
      () => this.mongooseHealth.pingCheck('mongoDB'),
    ]);

    this.reporter.publish(
      checkResult,
      !this.emailIdSuggestionService.isHealthy()
    );

    return checkResult;
  }
}
