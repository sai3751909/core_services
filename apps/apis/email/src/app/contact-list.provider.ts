import { Injectable, Logger } from '@nestjs/common';
import {
  OpsmatixLogger,
  ContactListService,
  ContactListDocument,
} from '@opsmatix/core';

@Injectable()
export class ContactListProvider {
  private readonly logger: Logger = new OpsmatixLogger(
    ContactListProvider.name
  );
  private readonly emailRegex =
    /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

  constructor(private readonly contactListService: ContactListService) {}
  private separateEmailsAndContactLists(input: string): {
    emails: string[];
    contactLists: string[];
  } {
    const emailAndContactLists = input.split(';').map((item) => item.trim());

    const emails: string[] = [];
    const contactLists: string[] = [];

    emailAndContactLists.forEach((item) => {
      if (item) {
        if (this.emailRegex.test(item)) {
          emails.push(item);
        } else {
          contactLists.push(item);
        }
      }
    });

    return { emails, contactLists };
  }

  async getContactListEmailIDs(contactListName: string): Promise<string[]> {
    const response: Partial<ContactListDocument> | null =
      await this.contactListService.findOneAsync({
        name: contactListName,
      });
    if (!response) {
      // If the contact list with the given name is not found, throw an error
      this.logger.error(
        `Contact list with name "${contactListName}" not found.`
      );
      throw new Error(`Contact list with name "${contactListName}" not found.`);
    }
    return response.members.map((member) => member.email);
  }

  async expandContactLists(contactLists: string[]): Promise<string[]> {
    const promises: Promise<string[]>[] = contactLists.map((contactListName) =>
      this.getContactListEmailIDs(contactListName)
    );

    const emailArrays: string[][] = await Promise.all(promises);

    const expandedEmails: string[] = emailArrays.flat();
    return expandedEmails;
  }

  async expandContactListsAndEmails(input: string): Promise<string[]> {
    const { emails, contactLists } = this.separateEmailsAndContactLists(input);

    if (contactLists.length === 0) {
      return emails;
    }

    try {
      const expandedContactLists = await this.expandContactLists(contactLists);
      return [...emails, ...expandedContactLists];
    } catch (error) {
      this.logger.error(`Error expanding contact list strings`, error);
      throw new Error(`Error expanding contact list strings`);
    }
  }

  async expandedRecipientEmails(
    recipient: string | null
  ): Promise<string | null> {
    if (recipient) {
      const expandedRecipient: string[] =
        await this.expandContactListsAndEmails(recipient);
      return expandedRecipient.join(';');
    }
    return recipient; // original input should be returned without any modification as empty string and null have different meanings.
  }
}
