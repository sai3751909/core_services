import { Module } from '@nestjs/common';
import {
  AuthModule,
  AUTH_SERVICE_CONFIG,
  CoreModule,
  EmailTemplateModule,
  UserModule,
} from '@opsmatix/core';
import { environment } from '../../environments/environment';
import { TemplatesController } from './templates.controller';
import { TemplatesService } from './templates.service';

@Module({
  imports: [
    CoreModule.forRoot(environment.mongodb.url),
    UserModule,
    AuthModule.forRoot(AUTH_SERVICE_CONFIG),
    EmailTemplateModule,
  ],
  controllers: [TemplatesController],
  providers: [TemplatesService],
})
export class TemplatesModule {}
