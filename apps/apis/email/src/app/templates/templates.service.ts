import { Injectable, Logger } from '@nestjs/common';
import { OpsmatixLogger, EmailTemplateService } from '@opsmatix/core';
import { EmailTemplate } from '@opsmatix/interfaces';

@Injectable()
export class TemplatesService {
  private readonly logger: Logger = new OpsmatixLogger(TemplatesService.name);

  constructor(private readonly emailTemplateService: EmailTemplateService) {}

  async getEmailTemplates(): Promise<EmailTemplate[]> {
    return await this.emailTemplateService.findAll().catch((err) => {
      const message = `Failed to get email templates `;
      this.logger.error(message, err.stack);
      throw new Error(message);
    });
  }
  async updateEmailTemplate(
    Id: string,
    updateData: Partial<EmailTemplate>
  ): Promise<void> {
    try {
      await this.emailTemplateService.updateEmailTemplate(Id, updateData);
    } catch (err) {
      const message = `Failed to update email template`;
      this.logger.error(message, err.stack);
      throw new Error(message);
    }
  }
  async createEmailTemplate(
    createData: Partial<EmailTemplate>
  ): Promise<EmailTemplate> {
    try {
      return await this.emailTemplateService.create(createData);
    } catch (err) {
      const message = `Failed to create email template`;
      this.logger.error(message, err.stack);
      throw new Error(message);
    }
  }
  async deleteEmailTemplate(Id: string): Promise<void> {
    try {
      await this.emailTemplateService.deleteMany({ _id: Id });
    } catch (err) {
      const message = `Failed to delete email template`;
      this.logger.error(message, err.stack);
      throw new Error(message);
    }
  }
}
