import {
  Controller,
  Get,
  Header,
  Logger,
  InternalServerErrorException,
  NotFoundException,
  Post,
  Put,
  Body,
  Delete,
  Param,
} from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import {
  OpsmatixLogger,
  UserGuard,
  EmailTemplateService,
} from '@opsmatix/core';
import {
  EmailTemplate,
  GetEmailTemplatesResponse,
  UpdateEmailTemplateRequest,
  DeleteEmailTemplateRequest,
} from '@opsmatix/interfaces';
import { TemplatesService } from './templates.service';

@Controller('templates')
export class TemplatesController {
  private readonly logger: Logger = new OpsmatixLogger(
    TemplatesController.name
  );

  constructor(
    private readonly templatesService: TemplatesService,
    private readonly emailTemplateService: EmailTemplateService
  ) {}

  @Get()
  @Header('Cache-Control', 'private, max-age=86400')
  @ApiBearerAuth()
  async getEmailTemplates(): Promise<GetEmailTemplatesResponse> {
    try {
      return {
        emailTemplates: await this.templatesService.getEmailTemplates(),
      };
    } catch (err) {
      const message = `Error getting email templates`;
      this.logger.error(message, err.stack);

      throw new NotFoundException(message);
    }
  }
  @Post()
  @ApiBearerAuth()
  async createEmailTemplate(
    @Body() emailTemplate: EmailTemplate
  ): Promise<{ message: string; emailTemplates: EmailTemplate[] }> {
    try {
      await this.templatesService.createEmailTemplate(emailTemplate);
      // Get all email templates
      const emailTemplates = await this.emailTemplateService.findAllAsync();
      return {
        message: 'Email template created successfully',
        emailTemplates: emailTemplates,
      };
    } catch (err) {
      const message = `Error creating email template`;
      this.logger.error(message, err.stack);
      throw new InternalServerErrorException(message);
    }
  }

  @Put(':Id')
  @ApiBearerAuth()
  async updateEmailTemplate(
    @Param('Id') Id: string,
    @Body() updateEmailTemplateRequest: EmailTemplate
  ): Promise<{ message: string; emailTemplates: EmailTemplate[] }> {
    const existingEmailTemplate = await this.emailTemplateService.findOneAsync({
      _id: Id,
    });

    if (!existingEmailTemplate) {
      throw new NotFoundException(`Email template with ID ${Id} not found`);
    }
    try {
      await this.templatesService.updateEmailTemplate(
        Id,
        updateEmailTemplateRequest
      );
      // Get all email templates
      const emailTemplates = await this.emailTemplateService.findAllAsync();
      return {
        message: 'Email template updated successfully',
        emailTemplates: emailTemplates,
      };
    } catch (err) {
      const message = `Error updating email template`;
      this.logger.error(message, err.stack);

      throw new InternalServerErrorException(message);
    }
  }

  @Delete(':Id')
  @ApiBearerAuth()
  async deleteEmailTemplate(
    @Param('Id') Id: string
  ): Promise<{ message: string; emailTemplates: EmailTemplate[] }> {
    const existingEmailTemplate = await this.emailTemplateService.findOneAsync({
      _id: Id,
    });

    if (!existingEmailTemplate) {
      throw new NotFoundException(`Email template with ID ${Id} not found`);
    }
    try {
      await this.templatesService.deleteEmailTemplate(Id);
      // Get all email templates
      const emailTemplates = await this.emailTemplateService.findAllAsync();
      return {
        message: 'Email template deleted successfully',
        emailTemplates: emailTemplates,
      };
    } catch (error) {
      this.logger.error('Error while deleting email template.', error.stack);
      throw new InternalServerErrorException(
        'Error while deleting email template'
      );
    }
  }
}
