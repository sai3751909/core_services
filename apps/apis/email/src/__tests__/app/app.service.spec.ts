import {
  CaseDetailsModule,
  CaseDetailsService,
  CaseModule,
  CaseService,
  EmailIndexModule,
  EmailIndexService,
  EmailService,
} from '@opsmatix/core';
import { mock } from 'jest-mock-extended';
import { Case, Omnichannel } from '@opsmatix/interfaces';
import { ConsoleLogger } from '@nestjs/common';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { Test, TestingModule } from '@nestjs/testing';
import { MongooseModule } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { AppService } from '../../app/app.service';

jest.mock('../../environments/environment', () => ({
  environment: {},
}));

const emailService = mock<EmailService>();

describe('App Service tests', () => {
  let appService: AppService;
  let caseService: CaseService;
  let caseDetailsService: CaseDetailsService;
  let emailIndexService: EmailIndexService;
  let mongod: MongoMemoryServer;
  beforeAll(async () => {
    mongod = await MongoMemoryServer.create();
    const uri = mongod.getUri();

    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(uri),
        CaseModule,
        CaseDetailsModule,
        EmailIndexModule,
      ],
      providers: [],
    })
      .setLogger(new ConsoleLogger())
      .compile();

    caseService = module.get<CaseService>(CaseService);
    caseDetailsService = module.get<CaseDetailsService>(CaseDetailsService);
    emailIndexService = module.get<EmailIndexService>(EmailIndexService);
    appService = new AppService(
      caseService,
      null,
      null,
      emailService,
      null,
      null,
      null,
      null,
      emailIndexService
    );
  });

  afterAll(async () => {
    await mongoose.disconnect();
    await mongod.stop();
  });

  beforeEach(async () => {
    jest.resetAllMocks();
    await caseService.deleteMany({});
    await caseDetailsService.deleteMany({});
    await emailIndexService.deleteMany({});
  });

  it('should be defined', () => {
    expect(appService).toBeDefined();
  });

  describe('findNewEmailId', () => {
    it('should find email using EmailIndex when case is not present', async () => {
      const caseDetails = await caseDetailsService.create({
        title: 'title',
        emailConversations: [
          {
            conversationId: '1',
            emails: [
              {
                emailId: '1',
                mailbox: 'abc@opsmatix.io',
                receivedDateTime: new Date('2021-01-01T00:00:00.000Z'),
                direction: Omnichannel.Email.Direction.Incoming,
              } as any as Omnichannel.Email,
            ],
          },
        ],
        asOf: new Date(),
      });

      const newCase = await caseService.create({
        orgUnit: 'Operations',
        caseDetails: caseDetails._id,
        caseDetailsAsOf: new Date(),
        caseType: 'General Query',
        title: 'test case',
        origin: 'Email',
        receivedDateTime: new Date('2021-01-01T00:00:00.000Z'),
        attributes: {},
        state: {
          status: Case.State.Status.ACCEPTED,
        },
      });

      await emailIndexService.create({
        mailbox: 'doesnot@matter.com',
        conversationId: '1',
        firstReceivedDateTime: new Date('2021-01-01T00:00:00.000Z'),
        latestReceivedDateTime: new Date('2021-01-01T00:00:00.000Z'),
        subject: 'subject',
        emails: [
          {
            emailId: '1',
            intMsgId: '1',
          },
        ],
      });

      emailService.searchEmails.mockResolvedValue({
        value: [
          {
            id: '2',
            receivedDateTime: new Date('2021-01-01T00:00:00.000Z'),
            direction: Omnichannel.Email.Direction.Incoming,
          } as any as Omnichannel.ProviderEmail,
        ],
      });
      const newEmailId = await appService.findNewEmailId('1');

      expect(newEmailId).toEqual('2');
    });

    it('should find email from the passed in case id', async () => {
      const caseDetails = await caseDetailsService.create({
        title: 'title',
        emailConversations: [
          {
            conversationId: '1',
            emails: [
              {
                emailId: '1',
                mailbox: 'abc@opsmatix.io',
                receivedDateTime: new Date('2021-01-01T00:00:00.000Z'),
                direction: Omnichannel.Email.Direction.Incoming,
              } as any as Omnichannel.Email,
            ],
          },
        ],
        asOf: new Date(),
      });

      const newCase = await caseService.create({
        orgUnit: 'Operations',
        caseDetails: caseDetails._id,
        caseDetailsAsOf: new Date(),
        caseType: 'General Query',
        title: 'test case',
        origin: 'Email',
        receivedDateTime: new Date('2021-01-01T00:00:00.000Z'),
        attributes: {},
        state: {
          status: Case.State.Status.ACCEPTED,
        },
      });

      emailService.searchEmails.mockResolvedValue({
        value: [
          {
            id: '2',
            receivedDateTime: new Date('2021-01-01T00:00:00.000Z'),
            direction: Omnichannel.Email.Direction.Incoming,
          } as any as Omnichannel.ProviderEmail,
        ],
      });

      // passing id
      expect(
        await appService.findNewEmailId('1', newCase._id.toString())
      ).toEqual('2');

      // passing case
      await newCase.populate('caseDetails');
      expect(await appService.findNewEmailId('1', null, newCase)).toEqual('2');
    });

    it('should return null when no emails found', async () => {
      const caseDetails = await caseDetailsService.create({
        title: 'title',
        emailConversations: [
          {
            conversationId: '1',
            emails: [
              {
                emailId: '1',
                mailbox: 'abc@opsmatix.io',
                receivedDateTime: new Date('2021-01-01T00:00:00.000Z'),
                direction: Omnichannel.Email.Direction.Incoming,
              } as any as Omnichannel.Email,
            ],
          },
        ],
        asOf: new Date(),
      });

      const newCase = await caseService.create({
        orgUnit: 'Operations',
        caseDetails: caseDetails._id,
        caseDetailsAsOf: new Date(),
        caseType: 'General Query',
        title: 'test case',
        origin: 'Email',
        receivedDateTime: new Date('2021-01-01T00:00:00.000Z'),
        attributes: {},
        state: {
          status: Case.State.Status.ACCEPTED,
        },
      });

      emailService.getEmailsFromConversation.mockResolvedValue({
        value: [], // no emails found
      });

      // passing id
      expect(
        await appService.findNewEmailId('1', newCase._id.toString())
      ).toBeNull();
    });

    it('should return null when EmailIndex is not found and also case is not passed', async () => {
      const caseDetails = await caseDetailsService.create({
        title: 'title',
        emailConversations: [
          {
            conversationId: '1',
            emails: [
              {
                emailId: '1',
                mailbox: 'abc@opsmatix.io',
                receivedDateTime: new Date('2021-01-01T00:00:00.000Z'),
                direction: Omnichannel.Email.Direction.Incoming,
              } as any as Omnichannel.Email,
            ],
          },
        ],
        asOf: new Date(),
      });

      const newCase = await caseService.create({
        orgUnit: 'Operations',
        caseDetails: caseDetails._id,
        caseDetailsAsOf: new Date(),
        caseType: 'General Query',
        title: 'test case',
        origin: 'Email',
        receivedDateTime: new Date('2021-01-01T00:00:00.000Z'),
        attributes: {},
        state: {
          status: Case.State.Status.ACCEPTED,
        },
      });

      emailService.getEmailsFromConversation.mockResolvedValue({
        value: [
          {
            id: '2',
            receivedDateTime: new Date('2021-01-01T00:00:00.000Z'),
            direction: Omnichannel.Email.Direction.Incoming,
          } as any as Omnichannel.ProviderEmail,
        ],
      });
      const newEmailId = await appService.findNewEmailId('1');

      expect(newEmailId).toBeNull();
    });
  });
});
