import { Test, TestingModule } from '@nestjs/testing';
import { ContactListService } from '@opsmatix/core';
import EmailIdSuggestionService from '../../app/email-id-suggestion.service';
import { Omnichannel } from '@opsmatix/interfaces';
describe('EmailIdSuggestionService', () => {
  let emailIdSuggestionService: EmailIdSuggestionService;
  let contactListService: ContactListService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EmailIdSuggestionService,
        {
          provide: ContactListService,
          useValue: {
            findAllAsync: jest.fn(),
          },
        },
      ],
    }).compile();

    emailIdSuggestionService = module.get<EmailIdSuggestionService>(
      EmailIdSuggestionService
    );
    contactListService = module.get<ContactListService>(ContactListService);
  });
  it('emailIdSuggestionService should be defined', () => {
    expect(emailIdSuggestionService).toBeDefined();
  });
  it('contactListService should be defined', () => {
    expect(contactListService).toBeDefined();
  });

  describe('populateIndex', () => {
    it('should add email to index if not in undeliverable set', () => {
      const emailAddress = {
        emailAddress: {
          address: 'test@example.com',
          name: 'Test User',
        },
      };

      emailIdSuggestionService.populateIndex(
        emailAddress as Omnichannel.Email.Address
      );

      // Assert that the email was added to the index
      expect(
        emailIdSuggestionService['index'].search('test@example.com')
      ).toHaveLength(1);
    });

    it('should not add email to index if in undeliverable set', () => {
      // Add an email to the undeliverable set
      emailIdSuggestionService['undeliverableEmailSet'].add('test@example.com');

      const emailAddress = {
        emailAddress: {
          address: 'test@example.com',
          name: 'Test User',
        },
      };

      emailIdSuggestionService.populateIndex(
        emailAddress as Omnichannel.Email.Address
      );

      // Assert that the email was not added to the index
      expect(
        emailIdSuggestionService['index'].search('test@example.com')
      ).toHaveLength(0);
    });
  });

  describe('depopulateIndex', () => {
    it('should remove email from index if in undeliverable set', () => {
      // Add an email to the index
      emailIdSuggestionService['index'].add({
        email: 'test@example.com',
        name: 'Test User',
      });

      // Add an email to the undeliverable set
      emailIdSuggestionService['undeliverableEmailSet'].add('test@example.com');

      const emailAddress = {
        emailAddress: {
          address: 'test@example.com',
        },
      };

      emailIdSuggestionService.depopulateIndex(
        emailAddress as Omnichannel.Email.Address
      );

      // Assert that the email was removed from the index
      expect(
        emailIdSuggestionService['index'].search('test@example.com')
      ).toHaveLength(0);
    });
  });
});
