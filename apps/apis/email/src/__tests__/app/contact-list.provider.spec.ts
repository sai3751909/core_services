import { Test, TestingModule } from '@nestjs/testing';
import { ContactListService, ContactListDocument } from '@opsmatix/core';
import { ContactListProvider } from '../../app/contact-list.provider';

describe('ContactListProvider', () => {
  let contactListProvider: ContactListProvider;
  let contactListService: ContactListService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ContactListProvider,
        {
          provide: ContactListService,
          useValue: {
            findOneAsync: jest.fn(),
          },
        },
      ],
    }).compile();

    contactListProvider = module.get<ContactListProvider>(ContactListProvider);
    contactListService = module.get<ContactListService>(ContactListService);
  });
  describe('separateEmailsAndContactLists', () => {
    it('should separate emails and contact lists correctly', () => {
      const input =
        'test1@example.com; Contact List 1; test2@example.com; Contact List 2';
      const expectedResult = {
        emails: ['test1@example.com', 'test2@example.com'],
        contactLists: ['Contact List 1', 'Contact List 2'],
      };

      const result =
        contactListProvider['separateEmailsAndContactLists'](input);

      expect(result).toEqual(expectedResult);
    });

    it('should handle input with only valid emails', () => {
      const input = 'test1@example.com; test2@example.com; test3@example.com';
      const expectedResult = {
        emails: ['test1@example.com', 'test2@example.com', 'test3@example.com'],
        contactLists: [],
      };

      const result =
        contactListProvider['separateEmailsAndContactLists'](input);

      expect(result).toEqual(expectedResult);
    });

    it('should handle input with only contact lists', () => {
      const input = 'Contact List 1; Contact List 2; Contact List 3';
      const expectedResult = {
        emails: [],
        contactLists: ['Contact List 1', 'Contact List 2', 'Contact List 3'],
      };

      const result =
        contactListProvider['separateEmailsAndContactLists'](input);

      expect(result).toEqual(expectedResult);
    });

    it('should handle input with an empty string', () => {
      const input = '';
      const expectedResult = {
        emails: [],
        contactLists: [],
      };

      const result =
        contactListProvider['separateEmailsAndContactLists'](input);
      expect(result).toEqual(expectedResult);
    });
  });
  describe('getContactListEmailIDs', () => {
    it('should throw an error if contact list not found', async () => {
      const contactListName = 'NonExistentList';
      jest
        .spyOn(contactListService, 'findOneAsync')
        .mockResolvedValueOnce(null);
      try {
        await contactListProvider.getContactListEmailIDs(contactListName);
      } catch (error) {
        expect(error.message).toBe(
          `Contact list with name "${contactListName}" not found.`
        );
      }
    });

    it('should return an array of email IDs from the contact list', async () => {
      const contactListName = 'Jaid Contact List';
      const mockContactList = {
        _id: '64c243f8816d6025a50d10d2',
        name: 'Jaid Contact List',
        members: [
          {
            name: 'Temesgen Daniel',
            email: 'temesgen@opsmatix.onmicrosoft.com',
          },
          {
            name: 'Kenneth Ezuroma',
            email: 'kenneth@opsmatix.onmicrosoft.com',
          },
          { name: 'Benjamin Lev', email: 'benjamin@opsmatix.onmicrosoft.com' },
        ],
      };
      jest
        .spyOn(contactListService, 'findOneAsync')
        .mockResolvedValueOnce(mockContactList as ContactListDocument);
      const result = await contactListProvider.getContactListEmailIDs(
        contactListName
      );
      expect(result).toEqual([
        'temesgen@opsmatix.onmicrosoft.com',
        'kenneth@opsmatix.onmicrosoft.com',
        'benjamin@opsmatix.onmicrosoft.com',
      ]);
    });
  });
  describe('expandContactLists', () => {
    it('should expand contact lists correctly', async () => {
      const contactLists = ['Contact List 1', 'Contact List 2'];
      const mockContactList1 = {
        _id: '64c243f8816d6025a50d10d2',
        name: 'Contact List 1',
        members: [
          {
            name: 'Temesgen Daniel',
            email: 'temesgen@opsmatix.onmicrosoft.com',
          },
          {
            name: 'Kenneth Ezuroma',
            email: 'kenneth@opsmatix.onmicrosoft.com',
          },
        ],
      };
      const mockContactList2: Partial<ContactListDocument> = {
        _id: '64c243f8816d6025a50d10d2',
        name: 'Contact List 2',
        members: [
          { name: 'Benjamin Lev', email: 'benjamin@opsmatix.onmicrosoft.com' },
        ],
      };
      jest
        .spyOn(contactListProvider, 'getContactListEmailIDs')
        .mockResolvedValueOnce(
          mockContactList1.members.map((member) => member.email)
        );
      jest
        .spyOn(contactListProvider, 'getContactListEmailIDs')
        .mockResolvedValueOnce(
          mockContactList2.members.map((member) => member.email)
        );
      const result = await contactListProvider.expandContactLists(contactLists);
      expect(result).toEqual([
        'temesgen@opsmatix.onmicrosoft.com',
        'kenneth@opsmatix.onmicrosoft.com',
        'benjamin@opsmatix.onmicrosoft.com',
      ]);
    });
  });

  describe('expandContactListsAndEmails', () => {
    it('should handle empty input', async () => {
      const input = '';
      const expectedOutput: string[] = [];

      const result = await contactListProvider.expandContactListsAndEmails(
        input
      );
      expect(result).toEqual(expectedOutput);
    });

    it('should throw an error in case of expansion failure', async () => {
      const input = 'test@test.com;Contact List 1';
      jest
        .spyOn(contactListProvider, 'expandContactLists')
        .mockRejectedValue(new Error('Failed to expand'));
      try {
        await contactListProvider.expandContactListsAndEmails(input);
      } catch (error) {
        expect(error.message).toBe(`Error expanding contact list strings`);
      }
    });

    it('should handle input with only valid emails', async () => {
      const input = 'test1@example.com; test2@example.com; test3@example.com';
      const expectedResult = [
        'test1@example.com',
        'test2@example.com',
        'test3@example.com',
      ];

      // Mock the separateEmailsAndContactLists method
      jest
        .spyOn(contactListProvider, 'separateEmailsAndContactLists' as any)
        .mockReturnValueOnce({
          emails: [
            'test1@example.com',
            'test2@example.com',
            'test3@example.com',
          ],
          contactLists: [],
        });

      const result = await contactListProvider.expandContactListsAndEmails(
        input
      );

      expect(result).toEqual(expectedResult);
    });

    it('should handle input with only contact lists', async () => {
      const input = 'Contact List 1; Contact List 2; Contact List 3';
      const contactList1Members = [
        { name: 'John Doe', email: 'john.doe@example.com' },
        { name: 'Jane Doe', email: 'jane.doe@example.com' },
      ];
      const contactList2Members = [
        { name: 'Alice', email: 'alice@example.com' },
        { name: 'Bob', email: 'bob@example.com' },
      ];
      const contactList3Members = []; // Empty contact list

      // Mock the separateEmailsAndContactLists method
      jest
        .spyOn(contactListProvider, 'separateEmailsAndContactLists' as any)
        .mockReturnValueOnce({
          emails: [],
          contactLists: ['Contact List 1', 'Contact List 2', 'Contact List 3'],
        });

      // Mock the getContactListEmailIDs method to return the email IDs for each contact list
      jest
        .spyOn(contactListProvider, 'getContactListEmailIDs')
        .mockResolvedValueOnce(
          contactList1Members.map((member) => member.email)
        )
        .mockResolvedValueOnce(
          contactList2Members.map((member) => member.email)
        )
        .mockResolvedValueOnce(
          contactList3Members.map((member) => member.email)
        );

      const result = await contactListProvider.expandContactListsAndEmails(
        input
      );

      expect(result).toEqual([
        'john.doe@example.com',
        'jane.doe@example.com',
        'alice@example.com',
        'bob@example.com',
      ]);
    });
    it('should handle input with a mix of valid emails and contact lists', async () => {
      const input =
        'test1@example.com; Contact List 1; test2@example.com; Contact List 2';
      const expectedResult = [
        'test1@example.com',
        'test2@example.com',
        'john.doe@example.com',
        'jane.doe@example.com',
        'alice@example.com',
        'bob@example.com',
      ];

      // Mock the separateEmailsAndContactLists method
      jest
        .spyOn(contactListProvider, 'separateEmailsAndContactLists' as any)
        .mockReturnValueOnce({
          emails: ['test1@example.com', 'test2@example.com'],
          contactLists: ['Contact List 1', 'Contact List 2'],
        });

      // Mock the expandContactLists method to return the expanded email list
      jest
        .spyOn(contactListProvider, 'expandContactLists')
        .mockResolvedValueOnce([
          'john.doe@example.com',
          'jane.doe@example.com',
          'alice@example.com',
          'bob@example.com',
        ]);

      const result = await contactListProvider.expandContactListsAndEmails(
        input
      );

      expect(result).toEqual(expectedResult);
    });
  });

  describe('expandedRecipientEmails', () => {
    it('should handle a null recipient', async () => {
      const recipient = null;
      const result = await contactListProvider.expandedRecipientEmails(
        recipient
      );

      expect(result).toBeNull();
    });

    it('should return null if recipient is an empty string', async () => {
      const recipient = '';

      const result = await contactListProvider.expandedRecipientEmails(
        recipient
      );
      expect(result).toBe(recipient);
    });

    it('should handle a single valid email', async () => {
      const recipient = 'test1@example.com';
      const expectedResult = 'test1@example.com';

      // Mock the expandContactListsAndEmails method to return the email directly
      jest
        .spyOn(contactListProvider, 'expandContactListsAndEmails')
        .mockResolvedValueOnce([recipient]);

      const result = await contactListProvider.expandedRecipientEmails(
        recipient
      );

      expect(result).toEqual(expectedResult);
    });

    it('should handle multiple valid emails', async () => {
      const recipient = 'test1@example.com;test2@example.com;test3@example.com';
      const expectedResult =
        'test1@example.com;test2@example.com;test3@example.com';

      // Mock the expandContactListsAndEmails method to return the emails directly
      jest
        .spyOn(contactListProvider, 'expandContactListsAndEmails')
        .mockResolvedValueOnce(recipient.split(';'));

      const result = await contactListProvider.expandedRecipientEmails(
        recipient
      );

      expect(result).toEqual(expectedResult);
    });

    it('should handle a single contact list', async () => {
      const recipient = 'Contact List 1';
      const contactList1Members = [
        { name: 'John Doe', email: 'john.doe@example.com' },
        { name: 'Jane Doe', email: 'jane.doe@example.com' },
      ];
      const expectedResult = 'john.doe@example.com;jane.doe@example.com';

      jest
        .spyOn(contactListProvider, 'expandContactListsAndEmails')
        .mockResolvedValueOnce(
          contactList1Members.map((member) => member.email)
        );

      const result = await contactListProvider.expandedRecipientEmails(
        recipient
      );

      expect(result).toEqual(expectedResult);
    });

    it('should handle multiple contact lists', async () => {
      const recipient = 'Contact List 1;Contact List 2';
      const contactList1Members = [
        { name: 'John Doe', email: 'john.doe@example.com' },
        { name: 'Jane Doe', email: 'jane.doe@example.com' },
      ];
      const contactList2Members = [
        { name: 'Alice', email: 'alice@example.com' },
        { name: 'Bob', email: 'bob@example.com' },
      ];
      const expectedResult =
        'john.doe@example.com;jane.doe@example.com;alice@example.com;bob@example.com';

      // Mock the expandContactListsAndEmails method to return the contact list members' emails
      jest
        .spyOn(contactListProvider, 'expandContactListsAndEmails')
        .mockResolvedValueOnce(
          contactList1Members
            .map((member) => member.email)
            .concat(contactList2Members.map((member) => member.email))
        );

      const result = await contactListProvider.expandedRecipientEmails(
        recipient
      );
      expect(result).toEqual(expectedResult);
    });
    it('should handle a mix of valid emails and contact lists', async () => {
      const recipient = 'test1@example.com; Contact List 1; test2@example.com';
      const contactList1Members = [
        { name: 'John Doe', email: 'john.doe@example.com' },
        { name: 'Jane Doe', email: 'jane.doe@example.com' },
      ];

      const expectedResult =
        'test1@example.com;john.doe@example.com;jane.doe@example.com;test2@example.com';

      // Mock the expandContactListsAndEmails method to return the combined list of emails and contact list members' emails
      jest
        .spyOn(contactListProvider, 'expandContactListsAndEmails')
        .mockResolvedValueOnce([
          'test1@example.com',
          ...contactList1Members.map((member) => member.email),
          'test2@example.com',
        ]);

      const result = await contactListProvider.expandedRecipientEmails(
        recipient
      );
      expect(result).toEqual(expectedResult);
    });
  });
});
