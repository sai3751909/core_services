import { TemplatesService } from '../../../app/templates/templates.service';
import { EmailTemplateService } from '@opsmatix/core';

jest.mock('@opsmatix/core');

describe('Templates Service', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should get the email templates from the email template service and return them if the cache is empty', async () => {
    const emailTemplateServiceMock = new EmailTemplateService(
      {} as any
    ) as jest.Mocked<EmailTemplateService>;

    const findAll = () => ({
      catch: async () => ['template1', 'template2'],
    });

    emailTemplateServiceMock.findAll.mockImplementation(findAll as any);

    const templatesService = new TemplatesService(emailTemplateServiceMock);
    const templates = await templatesService.getEmailTemplates();

    expect(emailTemplateServiceMock.findAll).toHaveBeenCalledWith();
    expect(templates).toStrictEqual(['template1', 'template2']);
  });
});
