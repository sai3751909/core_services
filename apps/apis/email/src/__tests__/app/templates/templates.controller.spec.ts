import { EmailTemplateService } from '@opsmatix/core';
import { TemplatesService } from '../../../app/templates/templates.service';
import { TemplatesController } from '../../../app/templates/templates.controller';
import { EmailTemplate } from '@opsmatix/interfaces';
import { NotFoundException } from '@nestjs/common';

jest.mock('../../../app/templates/templates.service');
jest.mock('@opsmatix/core');

describe('Templates Controller', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should return 2 email templates when the templates service returns 2 templates', async () => {
    const templateServiceMock = new TemplatesService(
      {} as EmailTemplateService
    ) as jest.Mocked<TemplatesService>;
    const emailTemplateServiceMock = new EmailTemplateService(
      {} as any
    ) as jest.Mocked<EmailTemplateService>;

    templateServiceMock.getEmailTemplates.mockImplementation(
      async () => ['template1', 'template2'] as unknown as EmailTemplate[]
    );
    const templatesController = new TemplatesController(
      templateServiceMock,
      emailTemplateServiceMock
    );
    const templates = await templatesController.getEmailTemplates();

    expect(templateServiceMock.getEmailTemplates).toHaveBeenCalledWith();
    expect(templates).toStrictEqual({
      emailTemplates: ['template1', 'template2'],
    });
  });

  it('should return 0 email templates when the templates service returns 0 templates', async () => {
    const templateServiceMock = new TemplatesService(
      {} as EmailTemplateService
    ) as jest.Mocked<TemplatesService>;
    const emailTemplateServiceMock = new EmailTemplateService(
      {} as any
    ) as jest.Mocked<EmailTemplateService>;

    templateServiceMock.getEmailTemplates.mockImplementation(async () => []);
    const templatesController = new TemplatesController(
      templateServiceMock,
      emailTemplateServiceMock
    );
    const templates = await templatesController.getEmailTemplates();

    expect(templateServiceMock.getEmailTemplates).toHaveBeenCalledWith();
    expect(templates).toStrictEqual({ emailTemplates: [] });
  });

  it('should throw a NotFoundException error when the service throws an error', async () => {
    const templateServiceMock = new TemplatesService(
      {} as EmailTemplateService
    ) as jest.Mocked<TemplatesService>;
    const emailTemplateServiceMock = new EmailTemplateService(
      {} as any
    ) as jest.Mocked<EmailTemplateService>;

    templateServiceMock.getEmailTemplates.mockImplementation(() => {
      throw new Error();
    });
    const templatesController = new TemplatesController(
      templateServiceMock,
      emailTemplateServiceMock
    );

    return expect(templatesController.getEmailTemplates()).rejects.toThrow(
      new NotFoundException('Error getting email templates')
    );
  });
});
