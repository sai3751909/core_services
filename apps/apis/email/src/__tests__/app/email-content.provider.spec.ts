import { OpsmatixLogger } from '@opsmatix/core';
import { EmailBody, Omnichannel } from '@opsmatix/interfaces';
import { mock } from 'jest-mock-extended';
import { EmailContentProvider } from '../../app/email-content.provider';

const mockOpsmatixLogger = mock<OpsmatixLogger>();

jest.mock(
  '@opsmatix/core',
  () => ({
    OpsmatixLogger: jest.fn(() => mockOpsmatixLogger),
  }),
  { virtual: true }
);

const emailContentProvider = new EmailContentProvider();

describe('Email Content Provider tests', () => {
  it('should convert text body to html and add case id', async () => {
    const emailBody: EmailBody = {
      contentType: 'text',
      content: 'sample text',
    };
    const caseId = '6321d88d7fd4c5e9c5fb8f8a';

    const response = emailContentProvider.getEmailBodyInHtml(caseId, emailBody);
    expect(response).toEqual(
      '<p>sample text</p><p style="font-size:1px"><<<6321d88d7fd4c5e9c5fb8f8a>>></p>'
    );
  });

  it('should convert base64 body to html and add case id', async () => {
    const emailBody: EmailBody = {
      contentType: 'text',
      base64Content: 'c2FtcGxlIHRleHQ=',
    };
    const caseId = '6321d88d7fd4c5e9c5fb8f8a';

    const response = emailContentProvider.getEmailBodyInHtml(caseId, emailBody);
    expect(response).toEqual(
      '<p>sample text</p><p style="font-size:1px"><<<6321d88d7fd4c5e9c5fb8f8a>>></p>'
    );
  });

  it('should leave the html body as it is and add case id', async () => {
    const emailBody: EmailBody = {
      contentType: 'html',
      content: '<html>sample text</html>',
    };
    const caseId = '6321d88d7fd4c5e9c5fb8f8a';

    const response = emailContentProvider.getEmailBodyInHtml(caseId, emailBody);
    expect(response).toEqual(
      '<html>sample text</html><p style="font-size:1px"><<<6321d88d7fd4c5e9c5fb8f8a>>></p>'
    );
  });

  it('should not add case id tag if there is already one in the email body with the same case id', async () => {
    const emailBody: EmailBody = {
      contentType: 'html',
      content:
        'sample text<p style="font-size:1px"><<<6321d88d7fd4c5e9c5fb8f8a>>></p>',
    };
    const caseId = '6321d88d7fd4c5e9c5fb8f8a';

    const response = emailContentProvider.getEmailBodyInHtml(caseId, emailBody);
    expect(response).toEqual(
      'sample text<p style="font-size:1px"><<<6321d88d7fd4c5e9c5fb8f8a>>></p>'
    );
  });

  it('should add case id tag if it is not found in the email body', async () => {
    const emailBody: EmailBody = {
      contentType: 'html',
      content:
        'sample text<p style="font-size:1px"><<<6321d88d7fd4c5e9c5fbold>>></p>',
    };
    const caseId = '6321d88d7fd4c5e9c5fb8new';

    const response = emailContentProvider.getEmailBodyInHtml(caseId, emailBody);
    expect(response).toEqual(
      'sample text<p style="font-size:1px"><<<6321d88d7fd4c5e9c5fbold>>></p><p style="font-size:1px"><<<6321d88d7fd4c5e9c5fb8new>>></p>'
    );
  });

  it('should throw error when content and base64Content is missing', async () => {
    const emailBody: EmailBody = {
      contentType: 'html',
    };
    const caseId = '6321d88d7fd4c5e9c5fb8new';

    return expect(() => {
      emailContentProvider.getEmailBodyInHtml(caseId, emailBody);
    }).toThrow(new Error(`No email body found!`));
  });

  it('should not add case id to draft email if case id is already present', async () => {
    const emailBody: EmailBody = {
      contentType: 'html',
      content: '<html>sample text</html>',
    };
    const caseId = '6321d88d7fd4c5e9c5fb8f8a';

    const response = emailContentProvider.updateDraftWithCaseId(caseId, {
      success: true,
      draftEmail: {
        id: '',
        createdDateTime: '',
        receivedDateTime: '',
        sentDateTime: '',
        hasAttachments: false,
        subject: '',
        parentFolderId: '',
        conversationId: '',
        isDeliveryReceiptRequested: false,
        isReadReceiptRequested: false,
        isRead: false,
        isDraft: false,
        body: {
          contentType: 'html' as any,
          content:
            '<html>sample text</html><p style="font-size:1px"><<<6321d88d7fd4c5e9c5fb8f8a>>></p>',
        },
        sender: undefined,
        from: undefined,
        toRecipients: [],
        ccRecipients: [],
        bccRecipients: [],
        replyTo: [],
      },
    });

    expect(response.draftEmail.body.content).toEqual(
      '<html>sample text</html><p style="font-size:1px"><<<6321d88d7fd4c5e9c5fb8f8a>>></p>'
    );
  });

  it('should add case id to draft email even if case id is missing', async () => {
    const emailBody: EmailBody = {
      contentType: 'html',
      content: '<html>sample text</html>',
    };
    const caseId = '6321d88d7fd4c5e9c5fb8f8a';

    const response = emailContentProvider.updateDraftWithCaseId(caseId, {
      success: true,
      draftEmail: {
        id: '',
        createdDateTime: '',
        receivedDateTime: '',
        sentDateTime: '',
        hasAttachments: false,
        subject: '',
        parentFolderId: '',
        conversationId: '',
        isDeliveryReceiptRequested: false,
        isReadReceiptRequested: false,
        isRead: false,
        isDraft: false,
        body: {
          contentType: 'html' as any,
          content: '<html>sample text</html>',
        },
        sender: undefined,
        from: undefined,
        toRecipients: [],
        ccRecipients: [],
        bccRecipients: [],
        replyTo: [],
      },
    });

    expect(response.draftEmail.body.content).toEqual(
      '<p style="font-size:1px"><<<6321d88d7fd4c5e9c5fb8f8a>>></p><html>sample text</html>'
    );
  });

  it('should add case id to draft email even if case id is already present but not the first one', async () => {
    const emailBody: EmailBody = {
      contentType: 'html',
      content: '<html>sample text</html>',
    };
    const caseId = '6321d88d7fd4c5e9c5fb8f8a';

    const response = emailContentProvider.updateDraftWithCaseId(caseId, {
      success: true,
      draftEmail: {
        id: '',
        createdDateTime: '',
        receivedDateTime: '',
        sentDateTime: '',
        hasAttachments: false,
        subject: '',
        parentFolderId: '',
        conversationId: '',
        isDeliveryReceiptRequested: false,
        isReadReceiptRequested: false,
        isRead: false,
        isDraft: false,
        body: {
          contentType: 'html' as any,
          content:
            '<html>sample text</html><p style="font-size:1px"><<<6321d88d7fd4c5e9c5fb8f8b>>></p><p style="font-size:1px"><<<6321d88d7fd4c5e9c5fb8f8a>>></p>',
        },
        sender: undefined,
        from: undefined,
        toRecipients: [],
        ccRecipients: [],
        bccRecipients: [],
        replyTo: [],
      },
    });

    expect(response.draftEmail.body.content).toEqual(
      '<p style="font-size:1px"><<<6321d88d7fd4c5e9c5fb8f8a>>></p><html>sample text</html><p style="font-size:1px"><<<6321d88d7fd4c5e9c5fb8f8b>>></p><p style="font-size:1px"><<<6321d88d7fd4c5e9c5fb8f8a>>></p>'
    );
  });

  it('should return same draft when no caseId is supplied', async () => {
    const emailBody: EmailBody = {
      contentType: 'html',
      content: '<html>sample text</html>',
    };

    const response = emailContentProvider.updateDraftWithCaseId(null, {
      success: true,
      draftEmail: {
        id: '',
        createdDateTime: '',
        receivedDateTime: '',
        sentDateTime: '',
        hasAttachments: false,
        subject: '',
        parentFolderId: '',
        conversationId: '',
        isDeliveryReceiptRequested: false,
        isReadReceiptRequested: false,
        isRead: false,
        isDraft: false,
        body: {
          contentType: 'html' as any,
          content: '<html>sample text</html>',
        },
        sender: undefined,
        from: undefined,
        toRecipients: [],
        ccRecipients: [],
        bccRecipients: [],
        replyTo: [],
      },
    });

    expect(response.draftEmail.body.content).toEqual(
      '<html>sample text</html>'
    );
  });

  it('should return same draft when CreateDraftEmailResponse is a failure', async () => {
    const emailBody: EmailBody = {
      contentType: 'html',
      content: '<html>sample text</html>',
    };

    const caseId = '6321d88d7fd4c5e9c5fb8f8a';

    const response = emailContentProvider.updateDraftWithCaseId(caseId, {
      success: false,
      draftEmail: {
        id: '',
        createdDateTime: '',
        receivedDateTime: '',
        sentDateTime: '',
        hasAttachments: false,
        subject: '',
        parentFolderId: '',
        conversationId: '',
        isDeliveryReceiptRequested: false,
        isReadReceiptRequested: false,
        isRead: false,
        isDraft: false,
        body: {
          contentType: 'html' as any,
          content: '<html>sample text</html>',
        },
        sender: undefined,
        from: undefined,
        toRecipients: [],
        ccRecipients: [],
        bccRecipients: [],
        replyTo: [],
      },
    });

    expect(response.draftEmail.body.content).toEqual(
      '<html>sample text</html>'
    );
  });
});
