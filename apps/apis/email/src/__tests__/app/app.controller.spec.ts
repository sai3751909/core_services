import { EmailService } from '@opsmatix/core';
import * as request from 'supertest';
import { mock } from 'jest-mock-extended';
import {
  CanActivate,
  ConsoleLogger,
  ExecutionContext,
  INestApplication,
} from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppService } from '../../app/app.service';
import { AppController } from '../../app/app.controller';
import EmailIdSuggestionService from '../../app/email-id-suggestion.service';
import { APP_GUARD } from '@nestjs/core';
import { Observable } from 'rxjs';

jest.mock('../../environments/environment', () => ({
  environment: {},
}));
export class MockAuthGuard implements CanActivate {
  canActivate(
    context: ExecutionContext
  ): boolean | Promise<boolean> | Observable<boolean> {
    return true;
  }
}
const emailService = mock<EmailService>();
const appService = mock<AppService>();
const emailIdSuggestionService = mock<EmailIdSuggestionService>();

describe('App Controller tests', () => {
  let app: INestApplication;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      controllers: [AppController],
      providers: [
        {
          provide: EmailService,
          useValue: emailService,
        },
        {
          provide: AppService,
          useValue: appService,
        },
        {
          provide: EmailIdSuggestionService,
          useValue: emailIdSuggestionService,
        },
        {
          provide: APP_GUARD,
          useClass: MockAuthGuard,
        },
      ],
    })
      .setLogger(new ConsoleLogger())
      .compile();
    app = module.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  beforeEach(async () => {
    jest.resetAllMocks();
  });

  describe('getEmailTypeInfoFromMailbox', () => {
    it(`/GET email type info`, () => {
      appService.getEmailTypeInfoFromMailbox.mockResolvedValue({
        success: true,
        isAutoReplyOrUndeliverable: true,
      });
      return request(app.getHttpServer())
        .get('/mailboxes/abc@outlook.com/emails/1/email-type-info')
        .expect(200)
        .expect({
          success: true,
          isAutoReplyOrUndeliverable: true,
        });
    });

    it(`/GET email type info throws NotFoundException when emailId does not exists`, () => {
      appService.getEmailTypeInfoFromMailbox.mockResolvedValue(null);
      return request(app.getHttpServer())
        .get('/mailboxes/abc@outlook.com/emails/1/email-type-info')
        .expect(404)
        .expect({
          statusCode: 404,
          message: 'Email[1] is not found in the vendor store any more!',
          error: 'Not Found',
        });
    });

    it(`/GET email type info throws BadRequestException when mailbox is not valid`, () => {
      return request(app.getHttpServer())
        .get('/mailboxes/ /emails/1/email-type-info')
        .expect(400)
        .expect({
          statusCode: 400,
          message: 'mailbox and emailId are required!',
          error: 'Bad Request',
        });
    });

    it(`/GET email type info throws BadRequestException when email id is not valid`, () => {
      return request(app.getHttpServer())
        .get('/mailboxes/abc@outlook.com/emails/ /email-type-info')
        .expect(400)
        .expect({
          statusCode: 400,
          message: 'mailbox and emailId are required!',
          error: 'Bad Request',
        });
    });
  });

  describe('getEmailInternetMessageHeaders', () => {
    it(`/GET email internetMessageHeaders`, () => {
      const headers = {
        'Thread-Topic': 'internet-message-headers',
        'Thread-Index': 'AQHaGPeo9tiF7LQ1oUGxIwVbWHbXRw==',
        'Message-ID':
          '<LNXP123MB24431642AD798626E6C5831ABBB7A@LNXP123MB2443.GBRP123.PROD.OUTLOOK.COM>',
      };
      appService.getEmailInternetMessageHeaders.mockResolvedValue(headers);
      return request(app.getHttpServer())
        .get('/mailboxes/abc@outlook.com/emails/1/internet-message-headers')
        .expect(200)
        .expect(headers);
    });
  });
});
