import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import {
  ConfigService,
  GlobalConfig,
  MainLogger,
  setupCoreExitHandlers,
  winstonConfig,
} from '@opsmatix/core';
import { json, urlencoded } from 'express';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { WinstonModule } from 'nest-winston';
import * as cookieParser from 'cookie-parser';
import { RequestMethod } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: WinstonModule.createLogger(winstonConfig()),
  });
  setupCoreExitHandlers(app);

  const globalPrefix = 'api/email';
  app.setGlobalPrefix(globalPrefix, {
    exclude: [
      { path: 'health', method: RequestMethod.GET },
      { path: 'metrics', method: RequestMethod.GET },
    ],
  });

  app.enableCors();
  app.use(cookieParser());

  const configService = ConfigService.getInstance();
  const port = parseInt(
    configService.getOrDefault(
      'API_PORT_OVERRIDE',
      GlobalConfig.ports.api.email + ''
    )
  );
  if (!environment.production) {
    const options = new DocumentBuilder()
      .addServer('http://localhost:' + port)
      .setTitle('Opsmatix Email API')
      .setDescription('Opsmatix Email API description')
      .setVersion('1.0')
      .addTag('Email api')
      .build();
    try {
      const document = SwaggerModule.createDocument(app, options);
      SwaggerModule.setup('api-doc', app, document);
    } catch (e) {
      console.log(e);
    }
  }

  //the following setting added to fix the error 'PayloadTooLargeError: request entity too large'.
  //50mb to support attachments upto 30mb
  app.use(json({ limit: '50mb' }));
  app.use(urlencoded({ extended: true, limit: '50mb' }));

  await app.listen(port, () => {
    MainLogger.log('Global prefix is: ' + globalPrefix);
    MainLogger.log('Listening at http://localhost:' + port + '/');
  });
}

bootstrap().catch((err) => {
  MainLogger.error(err);
});
