import { ConfigService, emailServiceConfig } from '@opsmatix/core';

const configService = ConfigService.getInstance();

export const environment = {
  production: configService.getBoolean('IS_PRODUCTION'),
  mongodb: {
    url: configService.get('MONGODB_URL'),
  },
  emailServiceConfig: emailServiceConfig,
  emailIdSuggestionsEnabled: configService.getBoolean(
    'EMAIL_ID_SUGGESTIONS_ENABLED',
    'false'
  ),
};
