import { ConfigService } from '@opsmatix/core';

const configService = ConfigService.getInstance();

export const environment = {
  production: configService.getBoolean('IS_PRODUCTION'),
  mongodb: {
    url: configService.get('MONGODB_URL'),
  },
  slack: {
    token: configService.getOrThrowError('SLACK_TOKEN'),
    signingSecret: configService.getOrThrowError('SLACK_SIGNING_SECRET'),
  },
};
