import { Controller, Get } from '@nestjs/common';
import {
  HealthCheck,
  HealthCheckService,
  MongooseHealthIndicator,
} from '@nestjs/terminus';
import { AllowUnauthorizedRequest } from '@opsmatix/core';

@Controller('health')
export class HealthController {
  constructor(
    private health: HealthCheckService,
    private mongooseHealth: MongooseHealthIndicator
  ) {}

  @Get()
  @HealthCheck()
  @AllowUnauthorizedRequest()
  async check() {
    return this.health.check([() => this.mongooseHealth.pingCheck('mongoDB')]);
  }
}
