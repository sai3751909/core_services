import { Injectable, Logger } from '@nestjs/common';
import {
  OpsmatixLogger,
  CaseService,
  SLACK_METADATA_EVENT_TYPE,
  ContentUtil,
  MessageChannelService,
} from '@opsmatix/core';
import {
  GetRelatedMessagesResponse,
  Omnichannel,
  PollMessagesResponse,
  PostMessageRequest,
} from '@opsmatix/interfaces';
import { App as SlackAPP } from '@slack/bolt';
import { environment } from '../environments/environment';

@Injectable()
export class AppService {
  private readonly logger: Logger = new OpsmatixLogger(AppService.name);

  readonly channelNameToChannelId = new Map();
  readonly userIdToProfileMap: Map<string, any> = new Map();
  readonly slackApp: SlackAPP;
  readonly contentUtil = new ContentUtil();
  private readonly REPLIES_LOOK_BACK_PERIOD_IN_MS = 86400000;

  constructor(
    private readonly caseService: CaseService,
    private readonly messageChannelService: MessageChannelService
  ) {
    this.slackApp = new SlackAPP({
      token: environment.slack.token,
      signingSecret: environment.slack.signingSecret,
    });
  }

  async findChannelId(channelName: string) {
    let channelId = this.channelNameToChannelId.get(channelName);
    if (!channelId) {
      channelId = await this.slackApp.client.conversations
        .list({
          types: 'public_channel,private_channel',
        })
        .then((result) => {
          let channelId = undefined;
          for (const channel of result.channels) {
            if (channel.name === channelName) {
              channelId = channel.id;

              // Print result
              console.log('Found Channel, Channel Id is: ' + channelId);
              // Break from for loop
              break;
            }
          }
          if (!channelId) {
            throw new Error(`no channel found with name: ${channelName}`);
          } else {
            return channelId;
          }
        })
        .catch((err) => {
          this.logger.error(
            `failed to find channelId for ${channelName}, error is: ${err.message}`
          );
          throw err;
        });
      this.channelNameToChannelId.set(channelName, channelId);
    }

    return channelId;
  }

  async getUser(userId: string) {
    let userProfile = this.userIdToProfileMap.get(userId);
    if (!userProfile) {
      const result = await this.slackApp.client.users.profile.get({
        user: userId,
      });
      if (result.ok === false) {
        userProfile = {
          real_name: 'Unknown',
          email: 'unknown@unknown.com',
        };
      } else {
        userProfile = result.profile;
        this.userIdToProfileMap.set(userId, userProfile);
      }
    }

    return userProfile;
  }

  private async getUserProfile(message) {
    let userProfileFromEmail;
    const postedByUsername = (message?.metadata?.event_payload as any)
      ?.postedByUsername; //opsmatix username(email)
    if (
      message?.metadata?.event_type === SLACK_METADATA_EVENT_TYPE &&
      postedByUsername
    ) {
      //lookup the user with email in slack and use that user if possible.
      const userFromEmail = await this.slackApp.client.users
        .lookupByEmail({
          email: postedByUsername,
        })
        .catch((err) => {
          //failed to find user.
          return undefined;
        });
      if (userFromEmail?.user?.profile) {
        userProfileFromEmail = userFromEmail.user.profile;
      }
    }
    const userProfile =
      userProfileFromEmail ?? (await this.getUser(message.user));
    return userProfile;
  }

  async pollMessages(
    channelName: string,
    startTimeStamp: string
  ): Promise<PollMessagesResponse> {
    // Call the conversations.history method using WebClient
    const result = await this.slackApp.client.conversations.history({
      channel: await this.findChannelId(channelName),
      oldest: startTimeStamp,
      include_all_metadata: true,
      //limit: 100
    });

    //this.logger.verbose(JSON.stringify(result, null, 2));

    if (result.ok === false) {
      throw new Error(result.error);
    } else {
      const messages: Omnichannel.ChatMessage[] = [];
      for (const message of result.messages) {
        // this.logger.debug(JSON.stringify(message, null, 2));
        try {
          const userProfile = await this.getUserProfile(message);
          const omniMessage: Omnichannel.ChatMessage = {
            communicationType: Omnichannel.Communication.Type.ChatMessage,
            ...(message?.metadata?.event_type === SLACK_METADATA_EVENT_TYPE //adds metadata if available
              ? {
                  metadata: {
                    caseId: (message?.metadata?.event_payload as any)?.caseId,
                  },
                }
              : {}),
            timestamp: new Date(
              parseInt(
                message.ts.substring(0, message.ts.length - 3).replace('.', '')
              )
            ),
            channelName: channelName,
            conversationId: message.thread_ts ? message.thread_ts : message.ts, //parent thread id (for slack)
            messageId: message.ts,
            body: {
              contentType: 'html',
              content: this.getHtml(message.text),
            },
            userId: message.user,
            userFullName: userProfile?.real_name ?? 'Unknown',
            userEmail: userProfile?.email ?? 'unknown@unknown.com',
          };
          messages.push(omniMessage);
        } catch (error) {
          //ignore errors for now
          this.logger.error(
            `Failed to process message[${channelName}-${message?.ts}] - ${error.message}`,
            error.stack
          );
        }
      }

      // now load the replies
      const replies = await this.pollReplies(
        channelName,
        startTimeStamp,
        messages && messages.length > 0
          ? messages[messages.length - 1].messageId
          : undefined
      );

      const fromTimeInt = Math.floor(
        parseInt(startTimeStamp.replace('.', '')) / 1000
      );
      replies.messages.forEach((message) => {
        if (message.timestamp.getTime() > fromTimeInt) {
          messages.push(message);
        }
      });
      // sort messages by message id
      messages.sort((a, b) => {
        return a.messageId.localeCompare(b.messageId);
      });

      return {
        messages: messages,
      };
    }
  }

  async getParentMessageIdsOfLatestReplies(
    channelName: string,
    watermarkTimeStamp: string,
    startTimeStamp: string,
    toTimeStamp?: string
  ): Promise<string[]> {
    if (startTimeStamp === toTimeStamp) {
      return [];
    }

    // Call the conversations.history method using WebClient
    const result = await this.slackApp.client.conversations.history({
      channel: await this.findChannelId(channelName),
      oldest: startTimeStamp,
      latest: toTimeStamp,
      include_all_metadata: true,
      //limit: 100
    });

    if (result.ok === false) {
      throw new Error(result.error);
    }

    if (result.messages.length === 0) {
      return [];
    }

    // get the last element to continue the search if there are more messages
    const oldestId = result.messages[result.messages.length - 1].ts; //i.e. results are returned in descending order

    const ids = result.messages
      .filter(
        (message) =>
          message.ts &&
          message.latest_reply &&
          message.ts !== message.latest_reply &&
          message.latest_reply > watermarkTimeStamp
      )
      .map((message) => {
        return message.ts;
      });

    const newIds = await this.getParentMessageIdsOfLatestReplies(
      channelName,
      watermarkTimeStamp,
      startTimeStamp,
      oldestId
    );
    const finalResult = [...ids, ...newIds];

    //sort the string result in ascending order
    return finalResult.sort();
  }

  //FIXME: very inefficient code: https://opsmatix.atlassian.net/browse/ENG-1646
  async pollReplies(
    channelName: string,
    watermarkTimeStamp: string, //startTimeStamp of pollMessages
    toTimeStamp?: string
  ): Promise<PollMessagesResponse> {
    const startTimeStamp = Math.floor(
      new Date(Date.now() - this.REPLIES_LOOK_BACK_PERIOD_IN_MS).getTime() /
        1000
    ).toString(); //derived from look back period

    this.logger.debug(`poll replies :${channelName} - ${startTimeStamp}`);
    const threads: Omnichannel.ChatMessage[] = [];
    for (const messageId of await this.getParentMessageIdsOfLatestReplies(
      channelName,
      watermarkTimeStamp,
      startTimeStamp,
      toTimeStamp
    )) {
      //latest_reply
      const result = await this.slackApp.client.conversations.replies({
        channel: await this.findChannelId(channelName),
        ts: messageId,
        oldest: startTimeStamp,
        include_all_metadata: true,
      });

      //this.logger.verbose(JSON.stringify(result, null, 2));

      if (result.ok === false) {
        throw new Error(result.error);
      } else {
        for (const thread of result.messages) {
          if (toTimeStamp && thread.ts > toTimeStamp) {
            // this is to ignore replies that are later than toTimeStamp, will be read in the next poll.
            continue;
          }
          // if the thread is not the main message
          if (thread.thread_ts !== thread.ts) {
            //this.logger.debug(JSON.stringify(thread, null, 2));
            try {
              let userProfileFromEmail;
              if (
                thread?.metadata?.event_type === SLACK_METADATA_EVENT_TYPE &&
                (thread?.metadata?.event_payload as any)?.postedByUsername
              ) {
                const postedByUsername = (
                  thread?.metadata?.event_payload as any
                )?.postedByUsername; //opsmatix username(email)

                //lookup the user with email in slack and use that user if possible.
                const userFromEmail = await this.slackApp.client.users
                  .lookupByEmail({
                    email: (thread?.metadata?.event_payload as any)
                      ?.postedByUsername,
                  })
                  .catch((err) => {
                    //failed to find user.
                    return undefined;
                  });
                if (userFromEmail?.user?.profile) {
                  userProfileFromEmail = userFromEmail.user.profile;
                }
              }
              const userProfile =
                userProfileFromEmail ?? (await this.getUser(thread.user));
              const omniMessage: Omnichannel.ChatMessage = {
                communicationType: Omnichannel.Communication.Type.ChatMessage,
                ...(thread?.metadata?.event_type === SLACK_METADATA_EVENT_TYPE //adds metadata if available
                  ? {
                      metadata: {
                        caseId: (thread?.metadata?.event_payload as any)
                          ?.caseId,
                      },
                    }
                  : {}),
                timestamp: new Date(
                  parseInt(
                    thread.ts
                      .substring(0, thread.ts.length - 3)
                      .replace('.', '')
                  )
                ),
                channelName: channelName,
                conversationId: thread.thread_ts,
                messageId: thread.ts,
                body: {
                  contentType: 'html',
                  content: this.getHtml(thread.text),
                },
                userId: thread.user,
                userFullName: userProfile?.real_name ?? 'Unknown',
                userEmail: userProfile?.email ?? 'unknown@unknown.com',
              };
              threads.push(omniMessage);
            } catch (error) {
              //ignore errors for now
              this.logger.error(
                `Failed to process thread[${channelName}-${messageId}-${thread?.ts}] - ${error.message}`,
                error.stack
              );
            }
          }
        }
      }
    }

    return { messages: threads };
  }

  getHtml(slackMarkDown: string): string {
    return this.contentUtil.transform(
      slackMarkDown,
      Omnichannel.ContentType.SlackMarkdown,
      Omnichannel.ContentType.Html
    );
  }

  async getMessage(
    channelName: string,
    messageTs: string
  ): Promise<Omnichannel.ChatMessage> {
    // Call the conversations.history method using WebClient
    const result = await this.slackApp.client.conversations.history({
      channel: await this.findChannelId(channelName),
      latest: messageTs,
      inclusive: true,
      include_all_metadata: true,
      limit: 1,
    });

    //this.logger.verbose(JSON.stringify(result, null, 2));

    if (result.ok === false) {
      throw new Error(result.error);
    } else {
      if (result.messages.length < 1) {
        throw new Error(
          `No message found with id ${messageTs} in channel ${channelName}`
        );
      }

      const message = result.messages[0];
      // this.logger.debug(JSON.stringify(message, null, 2));
      const userProfile = await this.getUserProfile(message);
      const omniMessage: Omnichannel.ChatMessage = {
        communicationType: Omnichannel.Communication.Type.ChatMessage,
        ...(message?.metadata?.event_type === SLACK_METADATA_EVENT_TYPE //adds metadata if available
          ? {
              metadata: {
                caseId: (message?.metadata?.event_payload as any)?.caseId,
              },
            }
          : {}),
        timestamp: new Date(
          parseInt(
            message.ts.substring(0, message.ts.length - 3).replace('.', '')
          )
        ),
        channelName: channelName,
        conversationId: message.thread_ts ? message.thread_ts : message.ts, //parent thread id (for slack)
        messageId: message.ts,
        body: {
          contentType: 'html',
          content: this.getHtml(message.text),
        },
        userId: message.user,
        userFullName: userProfile.real_name ? userProfile.real_name : 'Unknown',
        userEmail: userProfile.email
          ? userProfile.email
          : 'unknown@unknown.com',
      };
      return omniMessage;
    }
  }

  /**
   * Finds all the related messages(could vary depending on underlying chat platform)
   */
  async getRelatedMessages(
    channelName: string,
    messageTs: string
  ): Promise<GetRelatedMessagesResponse> {
    const relatedMessages: Omnichannel.ChatMessage[] = [];
    // Call the conversations.history method using WebClient
    const result = await this.slackApp.client.conversations.history({
      channel: await this.findChannelId(channelName),
      include_all_metadata: true,
      latest: messageTs,
      inclusive: true,
      limit: 1,
    });

    if (result.ok === true || result.messages.length > 0) {
      const message = result.messages[0];
      //1. find messages in attachments (example share-message in slack)
      if (message.attachments) {
        for (const a of message.attachments) {
          if (a.ts && a.channel_name) {
            const attachmentMessage = await this.getMessage(
              a.channel_name,
              a.ts
            ).catch((_) => {
              //ignore any errors
              return undefined;
            });
            if (attachmentMessage) {
              relatedMessages.push(attachmentMessage);
            }
          }
        }
      }

      //2. in case of slack, include parent message as well if exists.
      if (message.thread_ts && message.thread_ts != message.ts) {
        const parentMessage = await this.getMessage(
          channelName,
          message.thread_ts
        ).catch((_) => {
          //ignore any errors
          return undefined;
        });
        if (parentMessage) {
          relatedMessages.push(parentMessage);
        }
      }
    }

    return {
      messages: relatedMessages,
    };
  }

  // async getHistoricalData(channelName: string, messageTs: string) {
  //   // Call the conversations.history method using WebClient
  //   const result = await this.slackApp.client.conversations.history({
  //     channel: await this.findChannelId(channelName),
  //     include_all_metadata: true,
  //     latest: messageTs,
  //     inclusive: true,
  //     limit: 1,
  //   });
  //   let historical_data = '';

  //   if (result.ok === true || result.messages.length > 0) {
  //     const message = result.messages[0];
  //     if (message.attachments) {
  //       for (const a of message.attachments) {
  //         historical_data =
  //           historical_data +
  //           ' \n' +
  //           a.text +
  //           ' \n' +
  //           (await (
  //             await this.getHistoricalData(a.channel_name, a.ts)
  //           ).historical_data);
  //       }
  //     }
  //   }

  //   return { historical_data: historical_data };
  // }

  async postMessage(
    messageToPost: PostMessageRequest,
    userFullName: string,
    username: string
  ) {
    await this.slackApp.client.chat.postMessage({
      channel: await this.findChannelId(messageToPost.channelName),
      text: this.contentUtil.transform(
        messageToPost.body.content,
        messageToPost.body.contentType,
        Omnichannel.ContentType.SlackMarkdown
      ),
      username: userFullName,
      metadata: {
        event_type: SLACK_METADATA_EVENT_TYPE,
        event_payload: {
          caseId: messageToPost.caseId,
          postedByUsername: username,
          //postedByUsername: 'sateesh.pinnamaneni@opsmatix.io', //for testing
        },
      },
      ...(messageToPost.conversationId
        ? { thread_ts: messageToPost.conversationId }
        : {}),
      // text: '<http://www.example.com|link> any idea what it is no text: <http://www.example.com|.>',
      // text: 'does any one know what is customer asking about here?\n\nOpsmatix Case: <http://ip-172-31-12-24.eu-north-1.compute.internal/610070a5b2886f9ee80d5e85|Fimatix Trade Cancel>',
      // blocks: [
      //   {"type": "section", "text": {"type": "plain_text", "text": "block message"}}
      // ]
      // You could also use a blocks[] array to send richer content
    });
  }

  async getChannelsByTeam(teamId: string): Promise<string[]> {
    return this.messageChannelService.findChannelsByTeamID(teamId);
  }
}
