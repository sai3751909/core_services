import { Module } from '@nestjs/common';
import {
  AuthModule,
  AUTH_SERVICE_CONFIG,
  CoreModule,
  CaseModule,
  UserModule,
  UserGuard,
  MessageChannelModule,
  OpsmatixPrometheusModule,
} from '@opsmatix/core';
import { environment } from '../environments/environment';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TerminusModule } from '@nestjs/terminus';
import { HealthController } from './health.controller';
import { APP_GUARD } from '@nestjs/core';

@Module({
  imports: [
    TerminusModule,
    CoreModule.forRoot(environment.mongodb.url),
    UserModule,
    AuthModule.forRoot(AUTH_SERVICE_CONFIG),
    CaseModule,
    MessageChannelModule,
    OpsmatixPrometheusModule,
  ],
  controllers: [AppController, HealthController],
  providers: [
    AppService,
    {
      provide: APP_GUARD,
      useClass: UserGuard,
    },
  ],
})
export class AppModule {}
