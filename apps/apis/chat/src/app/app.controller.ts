import {
  Body,
  Controller,
  Get,
  Logger,
  Param,
  Post,
  Req,
} from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import {
  AllowUnauthorizedRequest,
  OpsmatixLogger,
  RequiredFeaturePermission,
} from '@opsmatix/core';
import {
  FeaturePermissions,
  GetLatestMessageTimestampResponse,
  GetRelatedMessagesResponse,
  Omnichannel,
  PollMessagesResponse,
  PostMessageRequest,
  User,
} from '@opsmatix/interfaces';
import { AppService } from './app.service';

@Controller('message')
@ApiBearerAuth()
export class AppController {
  private readonly logger: Logger = new OpsmatixLogger(
    `Email Controller: ${AppController.name}`
  );

  constructor(private readonly appService: AppService) {}

  @Get('latest-timestamp/:channelName')
  async getLatestTimestamp(
    @Param('channelName') channelName: string
  ): Promise<GetLatestMessageTimestampResponse> {
    this.logger.debug(`get latest timestamp :${channelName}`);
    // temp solution for now

    //time to microseconds (ex: 1687297335898000)
    const microseconds = new Date().getTime() + '000';
    // convert to slack marker(ex: 1687297335.898000)
    const slackMarker =
      microseconds.substring(0, microseconds.length - 6) +
      '.' +
      microseconds.substring(microseconds.length - 6);

    return {
      channelName,
      timestamp: slackMarker,
    };
  }

  @Get('poll/:channelName/:fromTime')
  async pollMessages(
    @Param('channelName') channelName: string,
    @Param('fromTime') fromTime: string
  ): Promise<PollMessagesResponse> {
    const response = await this.appService.pollMessages(channelName, fromTime);
    this.logger.debug(
      `poll messages[${channelName} - ${fromTime}] response size :${
        response?.messages?.length ?? 0
      }`
    );
    return response;
  }

  //helper method to get replies for testing.
  /* @Get('pollthread/:channelName')
  async pollThreadsInADay(
    @Param('channelName') channelName: string
  ): Promise<PollMessagesResponse> {
    const ONE_DAY_IN_MS = 86400000;
    const dateADayAgo = new Date(Date.now() - ONE_DAY_IN_MS);
    const unixTimeStamp = Math.floor(dateADayAgo.getTime() / 1000);
    this.logger.debug(`poll threads :${channelName} - ${unixTimeStamp}`);
    return this.appService.pollThread(channelName, `${unixTimeStamp}`);
  } */

  @Get('teams/:teamId/channels')
  getChannelsByTeam(@Param('teamId') teamId: string) {
    this.logger.debug(`Get channels by team: ${teamId}`);
    return this.appService.getChannelsByTeam(teamId);
  }

  @Get(':channelName/:messageId')
  async getMessage(
    @Param('channelName') channelName: string,
    @Param('messageId') messageId: string
  ): Promise<Omnichannel.ChatMessage> {
    this.logger.debug(`get message :${channelName} - ${messageId}`);
    return this.appService.getMessage(channelName, messageId);
  }

  @Get(':channelName/:messageId/related')
  async findRelatedMessages(
    @Param('channelName') channelName: string,
    @Param('messageId') messageId: string
  ): Promise<GetRelatedMessagesResponse> {
    this.logger.debug(`related messages :${channelName} - ${messageId}`);
    return this.appService.getRelatedMessages(channelName, messageId);
  }

  // @Get('historical-data/:channelName/:messageId')
  // async getHistoricalData(
  //   @Param('channelName') channelName: string,
  //   @Param('messageId') messageId: string
  // ) {
  //   this.logger.debug(`getHistoricalData :${channelName} - ${messageId}`);
  //   return this.appService.getHistoricalData(channelName, messageId);
  // }

  /* @Get('postmessage')
  // @UseGuards(UserGuard)
  // @ApiBearerAuth()
  async postmessage() {
    return this.appService.postMessage({
      channelName: 'engineering',
      text: 'some test message ',
    }, 'Test User');
  } */

  @Post()
  @RequiredFeaturePermission(FeaturePermissions.Omnichannel.Chat.Update)
  async postMessage(@Req() request, @Body() messageToPost: PostMessageRequest) {
    return this.appService.postMessage(
      messageToPost,
      User.getFullName(request.params.auth_user),
      request.params.auth_user.username
    );
  }

  // below is not required any more as above covers this.
  // @Post(':caseId/replyTo/:parentTs')
  // @RequiredFeaturePermission(FeaturePermissions.Omnichannel.Chat.Update)
  // async postReplyMessage(
  //   @Req() request,
  //   @Param('caseId') caseId: string,
  //   @Param('parentTs') parentTs: string,
  //   @Body() messageToPost: PostChatMessage
  // ) {
  //   return this.appService.postReplyMessage(
  //     messageToPost,
  //     parentTs,
  //     User.getFullName(request.params.auth_user),
  //     caseId
  //   );
  // }
}
