import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import {
  ConfigService,
  GlobalConfig,
  MainLogger,
  setupCoreExitHandlers,
  winstonConfig,
} from '@opsmatix/core';
import { json, urlencoded } from 'express';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { WinstonModule } from 'nest-winston';
import * as cookieParser from 'cookie-parser';
import { RequestMethod } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: WinstonModule.createLogger(winstonConfig()),
  });
  setupCoreExitHandlers(app);

  const globalPrefix = 'api/chat';
  app.setGlobalPrefix(globalPrefix, {
    exclude: [
      { path: 'health', method: RequestMethod.GET },
      { path: 'metrics', method: RequestMethod.GET },
    ],
  });

  app.enableCors();
  app.use(cookieParser());

  const configService = ConfigService.getInstance();
  const port = parseInt(
    configService.getOrDefault(
      'API_PORT_OVERRIDE',
      GlobalConfig.ports.api.chat + ''
    )
  );
  if (!environment.production) {
    const options = new DocumentBuilder()
      .addServer('http://localhost:' + port)
      .setTitle('Opsmatix Chat API')
      .setDescription('Opsmatix Chat API description')
      .setVersion('1.0')
      .addTag('Chat api')
      .build();
    try {
      const document = SwaggerModule.createDocument(app, options);
      SwaggerModule.setup('api-doc', app, document);
    } catch (e) {
      console.log(e);
    }
  }

  //the following setting added to fix the error 'PayloadTooLargeError: request entity too large'.
  app.use(json({ limit: '5mb' }));
  app.use(urlencoded({ extended: true, limit: '5mb' }));

  await app.listen(port, () => {
    MainLogger.log('Global prefix is: ' + globalPrefix);
    MainLogger.log('Listening at http://localhost:' + port + '/');
  });
}

bootstrap().catch((err) => {
  console.error(`bootstrap failed - ${err.message}`, err.stack);
});
