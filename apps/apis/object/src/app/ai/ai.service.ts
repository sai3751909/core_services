import { Injectable, Logger } from '@nestjs/common';
import { OpsmatixLogger, CaseAiDataDao } from '@opsmatix/core';
import { CaseAiData } from '@opsmatix/interfaces';

@Injectable()
export class AiService {
  private readonly logger: Logger = new OpsmatixLogger(
    `Object API: ${AiService.name}`
  );

  constructor(private readonly caseAiDataDao: CaseAiDataDao) {}

  async getCaseAnnotations(caseId: string): Promise<CaseAiData> {
    return this.caseAiDataDao.findOneAsync({ case: caseId });
  }
}
