import { Module } from '@nestjs/common';
import {
  AuthModule,
  AUTH_SERVICE_CONFIG,
  UserModule,
  CaseAiDataModule,
} from '@opsmatix/core';
import { AiService } from './ai.service';
import { AiController } from './ai.controller';

@Module({
  imports: [
    AuthModule.forRoot(AUTH_SERVICE_CONFIG),
    CaseAiDataModule,
    UserModule,
  ],
  providers: [AiService],
  controllers: [AiController],
})
export class AiModule {}
