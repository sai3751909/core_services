import {
  Controller,
  Get,
  Logger,
  Param,
  Res,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { OpsmatixLogger } from '@opsmatix/core';
import { ErrorHandlerInterceptor } from './../error-handler.interceptor';
import { AiService } from './ai.service';
import { CaseAiData } from '@opsmatix/interfaces';

@ApiTags('ai')
@Controller('ai')
@UseInterceptors(ErrorHandlerInterceptor)
export class AiController {
  private readonly logger: Logger = new OpsmatixLogger(
    `Object API: ${AiController.name}`
  );

  constructor(private readonly aiService: AiService) {}

  @Get('/cases/:caseId/annotations')
  @ApiBearerAuth()
  async getCaseAnnotations(
    @Param() param,
    @Res() response
  ): Promise<CaseAiData> {
    return await response.json(
      (await this.aiService.getCaseAnnotations(param.caseId)) ?? {
        case: param.caseId,
        annotatedContent: {
          contentType: 'text/plain',
        },
      }
    );
  }
}
