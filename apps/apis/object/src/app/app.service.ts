import {
  BadRequestException,
  Inject,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import {
  ActivityNoteService,
  ArchivedActivityNoteService,
  ArchivedCaseDetailsService,
  ArchivedCaseService,
  BaseService,
  CaseDetailsService,
  convertArrayToObject,
  getElapsedTime,
  getStartTime,
  GlobalConfig,
  InvalidArguments,
  OpsmatixLogger,
  ProcessInstanceService,
  ProcessService,
  CaseDocument,
  CaseService,
  TeamService,
  UserDocument,
  UserNotActiveError,
  UserService,
  UserTaskService,
  CaseTypeService,
  ExceptionTypes,
  StreamUtil,
  SimpleCache,
  CaseTypeDocument,
} from '@opsmatix/core';
import {
  BulkAssignmentRequest,
  BulkIgnoreRequest,
  CaseDetails,
  ProcessInstance,
  Case,
  CaseAssignmentRequest,
  UnarchivedCaseResponse,
  UnArchiveCasesDto,
  UnarchiveCasesResult,
  User,
  UserTask,
  NewChildCaseRequestEvent,
  EVENT_MESSAGE_TYPE_NEW_CHILD_CASE,
  NewCaseRequestEvent,
  EVENT_MESSAGE_TYPE_NEW_CASE,
  RoleNameEnum,
  UserType,
  DashboardSearchDto,
  ObjectsDiff,
  Difference,
  TransferCasesRequest,
  TransferCasesTeamsRequest,
  UserStatus,
  StreamMessage,
} from '@opsmatix/interfaces';
import { ZEEBE_CONNECTION_PROVIDER } from 'nestjs-zeebe';
import { find, assign, cloneDeep, map, every, isEmpty } from 'lodash';
import { ClientSession, Types } from 'mongoose';
import { Duration, ZBClient } from 'zeebe-node';
import { environment } from '../environments/environment';
import { caseFinders } from './dashboard/case-finders';
import { unarchiveUtility } from './unarchive-utility';
import IsMerged = Case.State.Status.IsMerged;
import State = Case.State;
import LastSeen = Case.LastSeen;
import CaseLastReceivedMessageTime = Case.LastReceivedMessageTime;
import { Client, ClientKafka, Transport } from '@nestjs/microservices';
import { v4 as uuid } from 'uuid';
import { buildProjectStage } from './dashboard/aggregation-utility/build-project-stage';
import { addIdCriteria } from './dashboard/aggregation-utility/add-id-criteria';
import { flow, filter, compact, uniqWith } from 'lodash/fp';
import { OrchestrationHelper } from './cases/orchestration-helper';
import { SchemaValidationService } from './schema-validation/schema-validation.service';

@Injectable()
export class AppService {
  private static CASES_PROJECTION =
    'title origin receivedDateTime state snoozeState caseDetailsAsOf updatedAt requestedBy parentCase caseType';

  private readonly logger = new OpsmatixLogger(
    `Object API: ${AppService.name}`
  );

  //ENG-956 - below cache is temporary fix, should be removed after implementing ENG-1534.
  private readonly userIdToUserRefCache = new SimpleCache();
  private readonly streamUtil: StreamUtil;

  private readonly pollInterval = 500;
  private readonly pollMaxTimes = 20;
  private readonly DEFAULT_CASE_STATUS_FILTERS = 'initiated,accepted';

  @Client({
    transport: Transport.KAFKA,
    options: {
      client: {
        clientId: GlobalConfig.namespace + '-object-api',
        brokers: GlobalConfig.kafka.brokers,
        ssl: GlobalConfig.kafka.sslEnabled,
      },
      consumer: {
        groupId: GlobalConfig.namespace + '-object-api',
      },
    },
  })
  omnichannel_ingestion_kafka: ClientKafka;

  @Client({
    transport: Transport.KAFKA,
    options: {
      client: {
        clientId: GlobalConfig.namespace + '-object-api-stream',
        brokers: GlobalConfig.kafka.brokers,
        ssl: GlobalConfig.kafka.sslEnabled,
      },
      consumer: {
        groupId: GlobalConfig.namespace + '-object-api-stream',
      },
    },
  })
  stream_kafka: ClientKafka;

  constructor(
    private readonly processService: ProcessService,
    private readonly processInstanceService: ProcessInstanceService,
    private readonly userCaseService: UserTaskService,
    private readonly caseService: CaseService,
    private readonly caseDetailsService: CaseDetailsService,
    private readonly activityNoteService: ActivityNoteService,
    private readonly userService: UserService,
    private readonly teamService: TeamService,
    @Inject(ZEEBE_CONNECTION_PROVIDER) private readonly zbClient: ZBClient,
    private readonly archivedCaseService: ArchivedCaseService,
    private readonly archivedCaseDetailsService: ArchivedCaseDetailsService,
    private readonly archivedActivityNoteService: ArchivedActivityNoteService,
    private readonly caseTypeService: CaseTypeService,
    private readonly orchestrationHelper: OrchestrationHelper,
    private readonly schemaValidationService: SchemaValidationService
  ) {
    this.streamUtil = new StreamUtil();
  }

  async createProcess(processName: string) {
    await this.processService.create({
      processName: processName,
      displayName: processName, //FIXME-ENG223:
    });
  }

  //FIXME-ENG-223: duplicate code
  async triggerCreateProcessInstance(caseId: string, processName: string) {
    try {
      await this.processInstanceService.validateCreateProcessInstance(
        caseId,
        processName
      );
    } catch (error) {
      throw new BadRequestException(error.message);
    }

    const processDoc = await this.processService.findOneAsync({
      processName: processName,
    });
    if (!processDoc) {
      throw new BadRequestException(`Process[${processName}] does not exist`);
    }

    const fullyQualifiedProcessName =
      GlobalConfig.namespace + '-' + processName;
    await this.zbClient.publishStartMessage({
      correlationKey: caseId + '_' + fullyQualifiedProcessName,
      messageId: uuid(),
      name: GlobalConfig.namespace + '-' + 'OPX-CREATE-PROCESS-INSTANCE',
      variables: {
        caseId: caseId,
        processName: processName,
        fullyQualifiedProcessName: fullyQualifiedProcessName,
      },
      timeToLive: Duration.seconds.of(10), // seconds
    });

    //FIXME-ENG-223: wait for processInstance to be created (by polling db)

    this.logger.debug(`ProcessInstance is created!`);
  }

  async cancelProcessInstance(processInstanceId: string) {
    const processInstance = await this.processInstanceService.findById(
      processInstanceId
    );
    if (!processInstance) {
      throw new BadRequestException(
        `ProcessInstance with id[${processInstanceId}] is not found!`
      );
    }

    if (
      processInstance.status === ProcessInstance.Status.COMPLETED ||
      processInstance.status === ProcessInstance.Status.FAILED ||
      processInstance.status === ProcessInstance.Status.CANCELLED
    ) {
      throw new BadRequestException(
        `ProcessInstance with id[${processInstanceId}] is already completed, can not be cancelled!`
      );
    }

    const zeebeProcessInstanceKey =
      processInstance.attributes['parentZeebeProcessInstanceKey'];
    if (zeebeProcessInstanceKey) {
      await this.zbClient.cancelProcessInstance(zeebeProcessInstanceKey);
      this.logger.log(
        `ProcessInstance[${zeebeProcessInstanceKey}] in zeebe is cancelled.`
      );
    }

    const userCases = await this.userCaseService.findAll({
      status: {
        $nin: [
          UserTask.State.Status.COMPLETED,
          UserTask.State.Status.CANCELLED,
        ],
      },
      processInstance: processInstance,
    });
    if (userCases) {
      for (const u of userCases) {
        await this.userCaseService.cancelUserTask(u.id);
      }
      this.logger.log(
        `All UserTasks for ProcessInstance[${processInstanceId}] are cancelled successfully!`
      );
    }

    await this.processInstanceService.cancelProcessInstance(processInstanceId);

    this.logger.log(
      `ProcessInstance[${processInstanceId}] is cancelled successfully!`
    );
  }

  async getCases(user: UserDocument, queryParams: any): Promise<any[]> {
    const statusFilters =
      queryParams.statusFilters || this.DEFAULT_CASE_STATUS_FILTERS;
    console.log(`getCases queryParams:${JSON.stringify(queryParams)}`);

    const caseOwnership = queryParams.caseOwnership ?? 'teamOrUser';
    const assignee = queryParams.assignee;
    const team = queryParams.team;
    const projection =
      AppService.CASES_PROJECTION +
      (queryParams.additional_fields
        ? ' ' + queryParams.additional_fields.split(',').join(' ')
        : '');
    const limit: number = queryParams.limit
      ? parseInt(queryParams.limit)
      : environment.cases.maxResultsCaseView;
    const sort = queryParams.sort === 'asc' ? 1 : -1;
    const caseQueries = [];

    if (statusFilters.includes('closed') || statusFilters.includes('ignored')) {
      const lookBackMultiplier = assignee != null ? 4 : 1; // i.e. searching with assignee can support longer look-back.
      const closedCasesQuery = {
        updatedAt: {
          $gt: new Date(
            Date.now() -
              lookBackMultiplier *
                environment.cases.terminatedMaxLookBackCaseViewHrs *
                60 *
                60 *
                1000
          ),
        },
      };
      if (
        statusFilters.includes('closed') &&
        statusFilters.includes('ignored')
      ) {
        closedCasesQuery['state.status'] = {
          $in: [State.Status.CLOSED, State.Status.IGNORED],
        };
      } else if (statusFilters.includes('closed')) {
        closedCasesQuery['state.status'] = State.Status.CLOSED;
      } else if (statusFilters.includes('ignored')) {
        closedCasesQuery['state.status'] = State.Status.IGNORED;
      }

      //comment it because when the case is closed or ignored status the object in db:"status" : "Ignored", "assigned_to" : "Operations:*" or "status" : "Closed", "assigned_to" : "Operations:*"
      if (assignee) {
        // closedCasesQuery['state.assignedTo.previousUser'] = assignee;
        closedCasesQuery['state.assignedTo.userRef.user'] = assignee;
      }

      caseQueries.push(closedCasesQuery);
    }

    // not required any more
    /* if (statusFilters.includes('assigned')) {
      const assignedCasesQuery = {
        'state.status': State.Status.INITIATED,
      };
      caseQueries.push(assignedCasesQuery);
    } */

    if (
      statusFilters.includes('accepted') ||
      statusFilters.includes('initiated')
    ) {
      const claimedAndAcceptedCasesQuery = {};

      if (
        statusFilters.includes('initiated') &&
        statusFilters.includes('accepted')
      ) {
        claimedAndAcceptedCasesQuery['state.status'] = {
          $in: [State.Status.INITIATED, State.Status.ACCEPTED],
        };
      } else if (statusFilters.includes('initiated')) {
        claimedAndAcceptedCasesQuery['state.status'] = State.Status.INITIATED;
      } else if (statusFilters.includes('accepted')) {
        claimedAndAcceptedCasesQuery['state.status'] = State.Status.ACCEPTED;
      }

      if (caseOwnership === 'user' && assignee) {
        claimedAndAcceptedCasesQuery['state.assignedTo.userRef.user'] =
          assignee;
      } else if (caseOwnership === 'team') {
        // there should not be an assignee -- i.e. open cases for the team.
        claimedAndAcceptedCasesQuery['state.assignedTo.userRef'] = {
          $exists: false,
        };
      } else if (caseOwnership === 'teamOrUser') {
        //i.e. cases with assignee or no assignee. team filter will be applied later if team param supplied.
        claimedAndAcceptedCasesQuery['$or'] = [
          {
            'state.assignedTo.userRef.user': assignee,
          },
          {
            'state.assignedTo.userRef': { $exists: false },
          },
        ];
      }

      caseQueries.push(claimedAndAcceptedCasesQuery);
    }

    // not required any more
    // this is to ensure user gets to see cases without owner.
    /* if (assignee === user.id) {
      if (
        statusFilters.includes('assigned') &&
        statusFilters.includes('accepted')
      ) {
        const acceptedAndUnclaimedCases = {
          'state.assignedTo.userRef': { $exists: false },
          'state.status': State.Status.ACCEPTED,
        };
        caseQueries.push(acceptedAndUnclaimedCases);
      }
    } */

    const originFilters = queryParams.originFilters
      ? queryParams.originFilters.split(',')
      : [];
    const originFilterQuery =
      originFilters.length > 1
        ? { $in: originFilters }
        : originFilters.length > 0
        ? originFilters[0]
        : undefined;
    this.logger.debug(
      `originFilters:${queryParams.originFilters} length:${
        originFilters.length
      }, originFilterQuery: ${JSON.stringify(originFilterQuery)}`
    );

    for (const query of caseQueries) {
      if (team) {
        if (!user.teams.find((t) => t._id.toString() === team)) {
          throw new UnauthorizedException(
            `User[${user.username}] is not a member of the team[${team}]!`
          );
        }
        query['state.assignedTo.teamRef.team'] = team;
      } else {
        query['state.assignedTo.teamRef.team'] = { $in: user.teams };
      }
      if (originFilterQuery) {
        query['origin'] = originFilterQuery;
      }
      if (!statusFilters.includes('snoozed')) {
        query['snoozeState.status'] = { $ne: 'ACTIVE' };
      }
    }

    this.logger.debug(`caseQueries:${JSON.stringify(caseQueries)}`);

    if (caseQueries.length > 0) {
      const cases = await this.caseService
        .findAll({ $or: caseQueries }, { projection: projection })
        .sort({ updatedAt: sort })
        .limit(limit);
      this.logger.debug(`cases count:${cases.length}`);
      return map(cases, this.mapCase);
    }

    return [];
  }

  async getCasesByParentId(parentId: string): Promise<CaseDocument[]> {
    const projection = AppService.CASES_PROJECTION;
    return this.caseService.findAll({ parentCase: parentId }, { projection });
  }

  private mapCase = ({
    _id,
    parentCase: pid,
    title: t,
    origin: o,
    receivedDateTime: rdt,
    attributes: attr,
    state: s,
    snoozeState,
    caseType: ct,
    tags: tg,
    caseDetailsAsOf: cdao,
    updatedAt: uat,
    requestedBy,
  }) => ({
    _id,
    pid,
    t,
    o,
    rdt,
    attr,
    s,
    ct,
    tg,
    sn: snoozeState.status === Case.SnoozeState.Status.ACTIVE,
    cdao,
    uat,
    requester: {
      email: requestedBy?.email,
    },
  });

  async getUpdatedCase(caseId: string): Promise<Record<string, any>> {
    return map([await this.caseService.findById(caseId)], this.mapCase);
  }

  async getCaseCounts(user: UserDocument, queryParams: any): Promise<any> {
    const originFilters = queryParams.originFilters
      ? queryParams.originFilters.split(',')
      : [];
    const assignee = queryParams.assignee;
    const team = queryParams.team;

    const aggregation: any[] = [
      {
        $match: {
          'state.status': {
            $in: [Case.State.Status.ACCEPTED, Case.State.Status.INITIATED],
          },
          'state.assignedTo.teamRef.team': { $in: user.teams },
        },
      },
    ];

    if (assignee) {
      aggregation.push({
        $match: {
          'state.assignedTo.userRef.user': new Types.ObjectId(assignee),
        },
      });
    }

    if (team) {
      aggregation.push({
        $match: {
          'state.assignedTo.teamRef.team': new Types.ObjectId(team),
        },
      });
    }

    if (originFilters.length > 0) {
      if (originFilters.length > 1) {
        aggregation.push({
          $match: {
            origin: { $in: originFilters },
          },
        });
      } else {
        aggregation.push({
          $match: {
            origin: originFilters[0],
          },
        });
      }
    }

    aggregation.push({
      $group: {
        _id: '$state.status',
        count: { $sum: 1 },
      },
    });
    this.logger.log(
      `case status count aggregation:${JSON.stringify(aggregation)}`
    );
    return this.caseService
      .aggregate(aggregation)
      .then((statuses) => convertArrayToObject(statuses, '_id', 'count'));
  }

  async getDashboardCases(
    user: UserDocument,
    params: DashboardSearchDto,
    additionalCaseFilters: any = null
  ): Promise<any[]> {
    console.log(`getDashboardCases params:${JSON.stringify(params)}`);
    if (params.search && Types.ObjectId.isValid(params.search.trim())) {
      return this.caseService.aggregate([
        addIdCriteria(params.search),
        buildProjectStage(),
      ]);
    }
    // some clean up, if one of start/end date is not provided, then use the other one.
    if (params.startDate != null) {
      if (params.endDate == null) {
        params.endDate = params.startDate;
      }
    } else if (params.endDate != null) {
      params.startDate = params.endDate;
    }

    // if days and start/end dates are not provided, then use 7 days as default.
    if (
      params.days == null &&
      (params.startDate == null || params.endDate == null)
    ) {
      params.days = 7;
    }

    const startTime = getStartTime();
    const statusFilters =
      params.statusFilters || this.DEFAULT_CASE_STATUS_FILTERS;
    const assignee = params.assignee;
    const team = params.team;
    const caseType = params.caseType;
    const tags = params.tags;
    const searchText = params.search;
    const startDate = params.startDate;
    const endDate = params.endDate;
    const limit = params.limit;
    const updatedAfter = params.updatedAfter;

    if (team && !user.teams.find((t) => t._id.toString() === team)) {
      throw new UnauthorizedException(
        `User[${user.username}] is not a member of the team[${team}]!`
      );
    }

    const dashboardAggregations = [];

    const { findFromCases } = caseFinders(this.logger, this.caseService);
    dashboardAggregations.push(
      findFromCases(
        statusFilters,
        assignee,
        searchText,
        user.teams,
        team,
        caseType,
        tags,
        limit,
        additionalCaseFilters,
        params.days,
        startDate,
        endDate,
        updatedAfter
      )
    );

    const dashboardAggregationResult = await Promise.all(dashboardAggregations);
    this.logger.debug(
      `dashboard query results:${JSON.stringify(
        dashboardAggregationResult.map((result) => result.length)
      )}`
    );

    if (dashboardAggregationResult.length > 1) {
      this.logger.debug(`removing duplicate ones`);
      //the fastest way to remove duplicates, so using this ugly solution
      for (let i = 0; i < dashboardAggregationResult[1].length; i++) {
        if (
          !dashboardAggregationResult[0].some(
            (opxCase) =>
              String(opxCase._id) ===
              String(dashboardAggregationResult[1][i]._id)
          )
        ) {
          dashboardAggregationResult[0].push(dashboardAggregationResult[1][i]);
        }
      }
    }

    const results = dashboardAggregationResult[0];

    this.logger.log(
      `getDashboardCases method takes ${getElapsedTime(
        startTime
      )} seconds. params:${JSON.stringify(params)}`
    );
    return results;
  }

  async getCase(id): Promise<CaseDocument> {
    return this.caseService.findByIdDetail(id);
  }

  //TODO:Fix it later, merge it with the above function. GetCase used many places, so don't want to change it for now.
  async getCaseDetail(
    caseId: string,
    loggedInUser: User
  ): Promise<CaseDocument> {
    const opxCase = await this.caseService.findByIdDetail(caseId);
    // Check if the user belongs to the team assigned to this case
    if (
      opxCase &&
      loggedInUser.userType === UserType.GENERAL &&
      !find(
        loggedInUser.teams,
        (team) =>
          team._id.toString() ===
          opxCase.state.assignedTo.teamRef.team._id.toString()
      )
    ) {
      let userCanViewCase = !!find(
        loggedInUser.roles,
        (role) => role.name === RoleNameEnum.TEAMOWNER
      );
      // Check if the user if the owner of the team assigned to this case
      if (userCanViewCase) {
        const team = await this.teamService.findOneAsync({
          _id: opxCase.state.assignedTo.teamRef.team._id,
        });
        userCanViewCase = !!find(
          team.owners,
          (owner) => owner.toString() === loggedInUser._id.toString()
        );
      }
      if (!userCanViewCase) {
        this.badRequest(
          `User ${loggedInUser.username} is not authorised to access case with id ${caseId}`,
          'You are not allowed to view this case.'
        );
      }
    }
    if (IsMerged(opxCase.state.status)) {
      const parentCaseDetail = await this.findTargetCaseToMerge(
        opxCase.caseDetails._id
      );
      const parentCase = await this.caseService.findByCaseDetailsId(
        parentCaseDetail.id
      );
      opxCase['_doc']['mergedToCaseId'] = parentCase.id;
    }
    return opxCase;
  }

  private badRequest(debugMessage: string, errorMessage: string) {
    this.logger.debug(debugMessage);
    throw new BadRequestException({
      success: false,
      message: errorMessage,
    });
  }

  private async findTargetCaseToMerge(calastoneCaseDetailsId: string) {
    return this.findTargetCaseToMergeWithCount(calastoneCaseDetailsId, 0);
  }

  private async findTargetCaseToMergeWithCount(
    calastoneCaseDetailsId: string,
    count: number
  ) {
    count = count + 1;
    if (count > 20) {
      //cheap hack for preventing circular relationships.
      throw new Error(
        `Can not find the parent, looped more than 20 times already!`
      );
    }

    const ccd = await this.caseDetailsService
      .findById(calastoneCaseDetailsId)
      .populate('mergedTo')
      .exec();
    if (!ccd) {
      throw new Error(
        `Failed to find CalastoneCaseDetails with id: ${calastoneCaseDetailsId}`
      );
    }

    if (ccd.mergedTo) {
      this.logger.debug(
        `found the parent[${ccd.mergedTo._id}], getting the partent now!`
      );
      return this.findTargetCaseToMergeWithCount(ccd.mergedTo._id, count);
    }

    return ccd;
  }

  async claimCase(requesterUserId: string, caseId: string) {
    const opxCase = await this.caseService
      .validateClaimCaseRequest(caseId)
      .catch((error) => {
        throw new BadRequestException(error.message);
      });

    if (
      opxCase.state.assignedTo?.userRef &&
      opxCase.state.assignedTo?.userRef?.user?._id?.toString() !=
        requesterUserId
    ) {
      throw new BadRequestException(
        `The case ${caseId} is already claimed by a different user[${opxCase.state.assignedTo?.userRef?.user?.username}].`
      );
    }

    const updatedCase = await this.caseService
      .claimCase(caseId, requesterUserId)
      .catch((error) => {
        if (error instanceof InvalidArguments) {
          throw new BadRequestException(error.message);
        } else {
          throw error;
        }
      });

    const reason = StreamMessage.CaseUpdatedMessageReason.USER_ASSIGNED;
    await this.streamUtil.publishCaseUpdatedStreamMessage(
      this.stream_kafka,
      reason,
      caseId
    );

    return updatedCase;
  }

  //NOTE: this allows claiming already claimed cases as long the user is same, typically used by backend services.
  async tryClaimCase(caseId: string, userId: string) {
    const opxCase: CaseDocument = await this.caseService.findByIdAsync(caseId);

    if (
      [
        // Case.State.Status.ASSIGNED,
        // Case.State.Status.CLAIMED,
        Case.State.Status.INITIATED,
        Case.State.Status.ACCEPTED,
      ].includes(opxCase.state.status)
    ) {
      await this.caseService.claimCaseWithCaseObj(opxCase, userId);
    } else {
      throw new Error(
        `Case[${caseId}] is not in claimable state[${opxCase.state.status}]!`
      );
    }
  }

  async sendCaseUpdatedMessageToZeebe(id: string) {
    const zeebeMessage = await this.zbClient.publishMessage({
      correlationKey: id,
      messageId: Date.now() + '',
      name: GlobalConfig.namespace + '-' + 'OPX-CASE-UPDATED',
      variables: {
        caseId: id,
      },
      timeToLive: Duration.seconds.of(60), // 60 seconds - no particular reason, but do not want to hold up too many events..
    });

    this.logger.debug(
      `zeebe message for case updated(${id}) is published: ${JSON.stringify(
        zeebeMessage
      )}`
    );
  }

  async unclaimCase(caseId: string, requesterUserId: string) {
    const opxCase = await this.caseService
      .validateUnclaimCaseRequest(caseId)
      .catch((error) => {
        throw new BadRequestException(error.message);
      });

    if (!opxCase) {
      throw new NotFoundException(`Case with id ${caseId} not found`);
    }

    if (
      opxCase.state.assignedTo?.userRef?.user?._id?.toString() !=
      requesterUserId
    ) {
      throw new UnauthorizedException(
        `User ${requesterUserId} is not the owner of the case ${caseId}.`
      );
    }

    const updatedCase = await this.caseService
      .unclaimCase(caseId, requesterUserId)
      .catch((error) => {
        throw new BadRequestException(error.message);
      });

    const reason = StreamMessage.CaseUpdatedMessageReason.USER_UNASSIGNED;
    await this.streamUtil.publishCaseUpdatedStreamMessage(
      this.stream_kafka,
      reason,
      caseId
    );

    return updatedCase;
  }

  async bulkAssignUser(
    bulkAssignmentRequest: BulkAssignmentRequest,
    requestedUser: UserDocument
  ) {
    const requestedUserId = requestedUser.id;
    await this.validateTeamOwnershipForBulkCasesProcessing(
      bulkAssignmentRequest.caseIds,
      requestedUserId
    );

    /**
     * A user cannot select a user for a team this user is not a member of, when assigning bulk cases.
     */
    if (bulkAssignmentRequest.userId) {
      if (
        !requestedUser.teams
          .map((team) => team._id.toString())
          .includes(bulkAssignmentRequest.teamId)
      ) {
        this.logAndThrowException(
          ExceptionTypes.BAD_REQUEST,
          `User cannot assign a user to the selected case(s) to a team this user is not a member of. TeamId: ${bulkAssignmentRequest.teamId}, UserId:${requestedUserId}`,
          'User cannot assign a user to the selected case(s) to a team this user is not a member of'
        );
      }
    }

    /**
     * Replicate case assignment functionality to that used by an individual user.
     */
    const promises = bulkAssignmentRequest.caseIds.map((caseId) =>
      this.assign(
        caseId,
        {
          userId: bulkAssignmentRequest.userId,
          teamId: bulkAssignmentRequest.teamId,
        },
        requestedUser
      ).then(() => this.sendCaseUpdatedMessageToZeebe(caseId))
    );

    await this.settlePromises(
      promises,
      `Not all case(s) are reassigned. TeamId: ${bulkAssignmentRequest.teamId}, UserId:${requestedUserId}`,
      'There was a problem assigning the selected case(s). Please try again!'
    );
  }

  async assign(
    caseId: string,
    caseAssignRequest: CaseAssignmentRequest,
    requestedUser: UserDocument
  ) {
    const opxCase = await this.caseService
      .validateAssignCaseRequest(caseId)
      .catch((error) => {
        throw new BadRequestException(error.message);
      });

    if (!opxCase) {
      throw new NotFoundException(`Case with id ${caseId} not found`);
    }

    const currentTeamId = opxCase.state.assignedTo.teamRef.team._id.toString();
    if (!requestedUser.teams.find((t) => t._id.toString() === currentTeamId)) {
      throw new UnauthorizedException(
        `User [${requestedUser.username}] is not authorized to access the case [${caseId}] as user is not a member of the respective team [${currentTeamId}].`
      );
    }

    const currentAssignee = opxCase.state?.assignedTo?.userRef?.username;
    const updatedCase = await this.caseService.assignCase(
      caseId,
      caseAssignRequest,
      requestedUser.id
    );

    const reason = currentAssignee
      ? StreamMessage.CaseUpdatedMessageReason.USER_CHANGED
      : StreamMessage.CaseUpdatedMessageReason.USER_ASSIGNED;
    await this.streamUtil.publishCaseUpdatedStreamMessage(
      this.stream_kafka,
      reason,
      caseId
    );

    return updatedCase;
  }

  async assignUser(
    userId: string,
    caseId: string,
    userPerformingTransfer: string
  ) {
    const opxCase: Case = await this.getCase(caseId);
    const previousUser = opxCase.state?.assignedTo?.userRef?.username;

    const updatedCase = this.caseService.assignCase(
      caseId,
      {
        userId: userId,
        teamId: opxCase.state.assignedTo.teamRef.team._id,
      },
      userPerformingTransfer
    );
    await this.sendCaseUpdatedMessageToZeebe(opxCase._id);

    const reason = previousUser
      ? StreamMessage.CaseUpdatedMessageReason.USER_CHANGED
      : StreamMessage.CaseUpdatedMessageReason.USER_ASSIGNED;
    await this.streamUtil.publishCaseUpdatedStreamMessage(
      this.stream_kafka,
      reason,
      caseId
    );

    return updatedCase;
  }

  async getTransferCasesTeams(
    transferCasesTeamsRequest: TransferCasesTeamsRequest,
    userPerformingTransfer: string
  ): Promise<State.AssignedToTeam[]> {
    const { fromUserId, toUserId } = transferCasesTeamsRequest;

    const users = await this.userService.findAll(
      {
        _id: { $in: [fromUserId, toUserId] },
        status: UserStatus.ACTIVE,
      },
      { projection: 'teams' }
    );

    if (users.length !== 2) {
      this.logAndThrowException(
        ExceptionTypes.USER_NOT_ACTIVE,
        `Inactive To (${toUserId}) or From (${fromUserId}) user!`,
        'Active To or From User cannot be found in the system'
      );
    }

    const toUser = users.find((user) => user.id === toUserId);

    const cases: Case[] = await this.caseService.findAllAsync({
      'state.assignedTo.userRef.user': BaseService.toObjectId(fromUserId),
    });

    const fromUserTeams: State.AssignedToTeam[] = map(
      cases,
      (c) => c.state.assignedTo.teamRef
    );

    const teamsUserIsOwnerOf = await this.teamService.filterTeamsByOwner(
      fromUserTeams.map((t) => t.team._id),
      userPerformingTransfer
    );

    return flow([
      filter(
        (assignedTeam: State.AssignedToTeam) =>
          teamsUserIsOwnerOf.find(
            (team) => team._id.toString() === assignedTeam.team._id.toString()
          ) && toUser.teams.includes(assignedTeam.team)
      ),
      compact,
      uniqWith(
        (
          assignedTeam: State.AssignedToTeam,
          cmpAssignedTeam: State.AssignedToTeam
        ) => {
          return (
            assignedTeam.team._id.toString() ===
            cmpAssignedTeam.team._id.toString()
          );
        }
      ),
    ])(fromUserTeams);
  }
  async getTransferCasesList(fromUserId, teamIds): Promise<Case[]> {
    const cases: Case[] = await this.caseService.findAllAsync({
      'state.assignedTo.userRef.user': BaseService.toObjectId(fromUserId),
      'state.assignedTo.teamRef.team': {
        $in: teamIds.map((id) => BaseService.toObjectId(id)),
      },
    });
    return cases;
  }

  async transferAllCases(
    transferCaseRequest: TransferCasesRequest,
    userPerformingTransfer: string
  ) {
    const {
      fromUser: fromUserId,
      toUser: toUserId,
      teamIds,
    } = transferCaseRequest;

    const allowedCaseTeams = await this.getTransferCasesTeams(
      {
        fromUserId: transferCaseRequest.fromUser,
        toUserId: transferCaseRequest.toUser,
      },
      userPerformingTransfer
    );

    if (!allowedCaseTeams.length || !teamIds.length) {
      this.logAndThrowException(
        ExceptionTypes.BAD_REQUEST,
        `Either allowed teams or selected teamIds list is empty ${
          allowedCaseTeams.length
        }, ${teamIds.join(',')}`,
        'Invalid teams selected to transfer cases'
      );
    }

    if (
      !every(teamIds, (teamId: string) =>
        allowedCaseTeams.find(
          (assignedTeam: State.AssignedToTeam) =>
            assignedTeam.team._id.toString() === teamId
        )
      )
    ) {
      this.logAndThrowException(
        ExceptionTypes.BAD_REQUEST,
        `Invalid teams ${teamIds.join(
          ','
        )} selected, to transfer From User ${fromUserId} to To User ${toUserId}`,
        'Invalid teams selected to transfer cases'
      );
    }

    const cases: Case[] = await this.caseService.findAllAsync({
      'state.assignedTo.userRef.user': BaseService.toObjectId(fromUserId),
      'state.assignedTo.teamRef.team': {
        $in: teamIds.map((id) => BaseService.toObjectId(id)),
      },
    });

    const resultPromises = cases.map((opxCase) =>
      this.assignUser(toUserId, opxCase._id, userPerformingTransfer)
    );

    await this.settlePromises(
      resultPromises,
      `Not all case(s) are transferred to this new user. To User Id: ${toUserId}`,
      'There was a problem transferring the cases. Please try again!'
    );
  }

  logAndThrowException(
    exceptionType: ExceptionTypes,
    logMessage: string,
    exceptionMessage: string
  ) {
    this.logger.error(logMessage);
    switch (exceptionType) {
      case ExceptionTypes.ERROR:
        throw new Error(exceptionMessage);

      case ExceptionTypes.BAD_REQUEST:
        throw new BadRequestException(exceptionMessage);

      case ExceptionTypes.INTERNAL_SERVER:
        throw new InternalServerErrorException(exceptionMessage);

      case ExceptionTypes.USER_NOT_ACTIVE:
        throw new UserNotActiveError(exceptionMessage);
    }
  }

  private async settlePromises(
    promises: any,
    logMessage: string,
    errorMessage: string
  ) {
    if (promises.length) {
      const results = await Promise.allSettled(promises);
      if (results.find((result) => result.status === 'rejected')) {
        this.logAndThrowException(
          ExceptionTypes.INTERNAL_SERVER,
          logMessage,
          errorMessage
        );
      }
    }
  }

  async acceptClassification(
    caseId: string,
    sendAck: boolean,
    requesterUserId: string
  ) {
    //send zeebe message
    return this.orchestrationHelper.publishZeebeMessageAndWaitForCompletion(
      'OPX-CASE-ACCEPT',
      caseId,
      requesterUserId,
      [State.Status.ACCEPTED],
      { sendAcceptAck: sendAck }
    );
  }

  /**
   * REDUNDANT CODE: duplicated function as undoAcceptCase function written below.
   * Please review and verify before removing.
   */

  // async unacceptClassification(
  //   caseId: string,
  //   sendAck: boolean,
  //   requesterUserId: string
  // ) {
  //   //send zeebe message
  //   return this.orchestrationHelper.publishZeebeMessageAndWaitForCompletion(
  //     'OPX-CASE-UNACCEPT',
  //     caseId,
  //     requesterUserId,
  //     [State.Status.CLAIMED, State.Status.ASSIGNED],
  //     { sendAcceptAck: sendAck }
  //   );
  // }

  async undoAcceptCase(caseId: string, requesterUserId: string) {
    try {
      await this.caseService.validateUndoAcceptCase(caseId);
    } catch (error) {
      throw new BadRequestException(error.message);
    }

    //send zeebe message
    return this.orchestrationHelper.publishZeebeMessageAndWaitForCompletion(
      'OPX-CASE-UNDO-ACCEPT',
      caseId,
      requesterUserId,
      [State.Status.INITIATED]
      // [State.Status.CLAIMED, State.Status.ASSIGNED]
    );
  }
  async acceptAndCloseCase(caseId: string, requesterUserId: string) {
    await this.acceptClassification(caseId, true, requesterUserId)
      .then(async () => {
        try {
          await this.closeCase(caseId, requesterUserId);
        } catch (error) {
          throw new InternalServerErrorException(
            `Failed to close the case: ${error.message}`
          );
        }
      })
      .catch((error) => {
        throw new InternalServerErrorException(
          `Failed to accept the case: ${error.message}`
        );
      });
  }

  async closeCase(caseId: string, requesterUserId: string) {
    const activeInstances =
      await this.processInstanceService.getActiveProcessInstances(caseId);
    if (activeInstances.length > 0) {
      throw new InternalServerErrorException(
        `Case[${caseId}] has active process instances, hence it can not be closed!`
      );
    }

    try {
      await this.caseService.validateCloseCaseRequest(caseId);
    } catch (error) {
      throw new BadRequestException(error.message);
    }

    //send zeebe message
    return this.orchestrationHelper.publishZeebeMessageAndWaitForCompletion(
      'OPX-CASE-CLOSE',
      caseId,
      requesterUserId,
      [State.Status.CLOSED]
    );
  }

  //FIXME: caseId is not used in this function yet.
  //FIXME: this should be cached too.
  async getTriageCaseTypes(caseId: string): Promise<string[]> {
    const caseTypes: CaseTypeDocument[] =
      await this.caseTypeService.findAllAsync({});

    return caseTypes.map((caseType) => caseType.name);
  }

  async updateCaseTitle(id: string, newTitle: string, requesterUserId: string) {
    // no validation for now, not sure terminated cases are allowed to updated.

    return this.caseService
      .updateCaseTitle(id, newTitle, requesterUserId)
      .catch((error) => {
        if (error instanceof InvalidArguments) {
          throw new BadRequestException(error.message);
        } else {
          throw error;
        }
      });
  }

  async getCaseTypes() {
    return this.caseTypeService.findAllAsync({}, {});
  }

  async updateCaseType(id: string, caseType: string, requesterUserId: string) {
    try {
      await this.caseService.validateCaseTypeUpdateRequest(
        id,
        caseType,
        requesterUserId
      );
    } catch (error) {
      throw new BadRequestException(error.message);
    }

    return this.caseService
      .updateCaseType(id, caseType, requesterUserId)
      .catch((error) => {
        if (error instanceof InvalidArguments) {
          throw new BadRequestException(error.message);
        } else {
          throw error;
        }
      });
  }

  async undoIgnoreCase(caseId: string, requesterUserId: string) {
    try {
      await this.caseService.validateUnIgnoreCaseRequest(caseId);
    } catch (error) {
      throw new BadRequestException(error.message);
    }

    //send zeebe message
    return this.orchestrationHelper.publishZeebeMessageAndWaitForCompletion(
      'OPX-CASE-UNDO-IGNORE',
      caseId,
      requesterUserId,
      [State.Status.INITIATED]
      // [State.Status.CLAIMED, State.Status.ASSIGNED]
    );
  }

  async reopenCase(caseId: string, requesterUserId: string) {
    try {
      await this.caseService.validateReopenCaseRequest(caseId);
    } catch (error) {
      throw new BadRequestException(error.message);
    }

    if ((await this.getCase(caseId)).state.status === State.Status.IGNORED) {
      //to hande undo of ignore
      return this.undoIgnoreCase(caseId, requesterUserId);
    }

    //send zeebe message
    return this.orchestrationHelper.publishZeebeMessageAndWaitForCompletion(
      'OPX-CASE-REOPEN',
      caseId,
      requesterUserId,
      [State.Status.ACCEPTED]
    );
  }

  async validateMergeCaseRequest(caseId: string, newCaseId: string) {
    try {
      await this.caseService.validateMergeCaseRequest(caseId, newCaseId);
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async mergeCase(caseId: string, newCaseId: string, requesterUserId: string) {
    await this.validateMergeCaseRequest(caseId, newCaseId);

    //send zeebe message
    return this.orchestrationHelper.publishZeebeMessageAndWaitForCompletion(
      'OPX-CASE-MERGE',
      caseId,
      requesterUserId,
      [State.Status.MERGED],
      { mergeTargetCaseId: newCaseId }
    );
  }

  async bulkIgnore(bulkIgnoreRequest: BulkIgnoreRequest, userId?: string) {
    await this.validateTeamOwnershipForBulkCasesProcessing(
      bulkIgnoreRequest.caseIds,
      userId
    );

    const promises = bulkIgnoreRequest.caseIds.map((caseId) =>
      this.ignoreCase(caseId, userId)
    );

    await this.settlePromises(
      promises,
      `Not all case(s) are ignored. User Id: ${userId}, cases: ${bulkIgnoreRequest.caseIds.join(
        ', '
      )}`,
      'There was a problem ignoring the cases. Please try again!'
    );
  }

  async validateTeamOwnershipForBulkCasesProcessing(
    caseIds: string[],
    loggedInUserId: string
  ) {
    const cases: Case[] = await this.caseService.findAllAsync({
      _id: { $in: caseIds },
    });

    if (isEmpty(cases)) {
      this.logAndThrowException(
        ExceptionTypes.BAD_REQUEST,
        `Invalid case list sent in the request ${caseIds.join(', ')}`,
        'Invalid case(s) sent for bulk assignment!'
      );
    }
    const teamIds: string[] = [];
    const teamIdsCheck = [];

    cases.forEach((c) => {
      const teamId = c.state.assignedTo.teamRef.team._id;
      if (teamIdsCheck.indexOf(teamId.toString()) === -1) {
        teamIdsCheck.push(teamId.toString());
        teamIds.push(teamId);
      }
    });

    /**
     * A logged in user can only bulk process the cases, which belong to the team this user is owner of.
     */
    const isOwner = await this.teamService.isOwner(teamIds, loggedInUserId);

    if (!isOwner) {
      this.logAndThrowException(
        ExceptionTypes.BAD_REQUEST,
        `User is not the owner of the team that cases belong to. userId: ${loggedInUserId} cases: ${caseIds.join(
          ', '
        )}`,
        `User is not the owner of the team that selected case(s) belong to`
      );
    }
  }

  async ignoreCase(caseId: string, requesterUserId: string) {
    try {
      await this.caseService.validateIgnoreCase(caseId);
    } catch (error) {
      throw new BadRequestException(error.message);
    }

    //send zeebe message
    return this.orchestrationHelper.publishZeebeMessageAndWaitForCompletion(
      'OPX-CASE-IGNORE',
      caseId,
      requesterUserId,
      [State.Status.IGNORED]
    );
  }

  async createChildCase(request: NewChildCaseRequestEvent) {
    //validation
    const opxCase: Case = await this.caseService.findByIdDetail(
      request.parentCaseId
    );
    if (!opxCase) {
      throw new Error(`Parent Case not found - ${request.parentCaseId}`);
    }

    const kafkaPayload = {
      headers: {
        message_type: EVENT_MESSAGE_TYPE_NEW_CHILD_CASE,
        namespace: GlobalConfig.namespace,
      },
      key: request.parentCaseId,
      value: request,
    };

    const result = await this.omnichannel_ingestion_kafka
      .emit(GlobalConfig.kafka_topics.omnichannel_ingestion, kafkaPayload)
      .toPromise();

    this.logger.log(
      `Child case request submitted to kafka. - ${JSON.stringify(result)}`
    );
  }

  async createCase(newCase: NewCaseRequestEvent) {
    //validation
    const existingCase = await this.caseService
      .findOne({
        externalRef: newCase.externalRef,
        caseType: newCase.caseType,
      })
      .exec();

    if (existingCase) {
      //i.e. there is already a case with same externalRef
      throw new Error(
        `There is already an existing case[${existingCase._id}] with the supplied reference[${newCase.externalRef}], hence not creating a new case!`
      );
    }

    const kafkaPayload = {
      headers: {
        message_type: EVENT_MESSAGE_TYPE_NEW_CASE,
        namespace: GlobalConfig.namespace,
      },
      key: newCase.externalRef,
      value: newCase,
    };

    const result = await this.omnichannel_ingestion_kafka
      .emit(GlobalConfig.kafka_topics.omnichannel_ingestion, kafkaPayload)
      .toPromise();

    this.logger.log(
      `New Case request submitted to kafka. - ${JSON.stringify(result)}`
    );
  }

  async findSimilarCases(caseId: string): Promise<Case.DuplicateCaseInfo[]> {
    return this.caseService.findSimilarCases(caseId);
  }

  async findRelatedCases(caseId: string, limit?: number): Promise<Case[]> {
    return this.caseService.findRelatedCases(caseId, limit);
  }

  async findAllProcesses() {
    return this.processService.findAllAsync();
  }

  async findProcessInstances(caseId: string) {
    return this.processInstanceService.getExistingProcessInstances(caseId);
  }

  async addUserNote(
    caseId: string,
    requesterUserId: string,
    content: {
      contentType: string;
      content: string;
    }
  ) {
    try {
      const response = await this.caseService.addUserNote(
        caseId,
        requesterUserId,
        content.contentType,
        content.content
      );
      this.logger.debug(`User Note is added to the case ${caseId}`);
      const reason = StreamMessage.CaseUpdatedMessageReason.NEW_USER_NOTE;
      await this.streamUtil.publishCaseUpdatedStreamMessage(
        this.stream_kafka,
        reason,
        caseId
      );
      return response;
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }
  async deleteUserNote(
    caseId: string,
    requesterUserId: string,
    userNoteIndex: number
  ) {
    try {
      const response = await this.caseService.deleteUserNote(
        caseId,
        requesterUserId,
        userNoteIndex
      );
      this.logger.debug(`User Note is deleted from the case ${caseId}`);
      const reason = StreamMessage.CaseUpdatedMessageReason.USER_NOTE_DELETED;
      await this.streamUtil.publishCaseUpdatedStreamMessage(
        this.stream_kafka,
        reason,
        caseId
      );
      return response;
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async getActivityNotes(caseId: string) {
    return this.activityNoteService.findByCaseId(caseId);
    //const case = await this.opsmatixCaseModel.findById(caseId, 'activity_notes');
    //return this.activityNoteModel.find().select('createdDate title details').where("_id").in(opxCase.activity_notes.map(activityNote=>activityNote["_id"]));
  }

  async updateAttributes(
    loggedInUser: User,
    caseId: string,
    attributes: any,
    ifMatch: string
  ) {
    const opxCase: CaseDocument = await this.caseService.findByIdAsync(caseId);

    if (environment.cases.schemaDrivenDataModel) {
      // i.e. if schema driven data model is enabled
      await this.schemaValidationService.validateCaseAttributes(
        opxCase.caseType,
        attributes
      );
    }

    const original = cloneDeep(opxCase.attributes);

    assign(opxCase.attributes, attributes);

    const updatedCase = await this.caseService.updateAsync(opxCase, {
      modifiedField: 'attributes',
      ifMatch: ifMatch,
    });

    await this.auditAttributeUpdates(
      loggedInUser,
      caseId,
      original,
      opxCase.attributes
    ).catch((error) => {
      this.logger.error(
        `Failed to audit updates to attributes. error is: ${error.message}`
      );
    });

    await this.streamUtil.publishCaseUpdatedStreamMessage(
      this.stream_kafka,
      StreamMessage.CaseUpdatedMessageReason.CASE_ATTRIBUTES_UPDATED,
      caseId
    );

    return updatedCase;
  }

  async auditAttributeUpdates(
    loggedInUser: User,
    caseId: string,
    original: Case.Attributes,
    updatedAttributes: Case.Attributes
  ) {
    const details = this.getAuditLogDetails(
      loggedInUser,
      original,
      updatedAttributes
    );
    if (details.indexOf('No changes detected in') === -1) {
      const note = {
        entity: BaseService.toObjectId(caseId),
        user: 'system',
        title: 'Attributes Update',
        details,
        component: 'apis-object',
      };
      await this.activityNoteService.updateAsync(note);
    }
  }

  private getAuditLogDetails(
    loggedInUser: User,
    original: Case.Attributes,
    updatedAttributes: Case.Attributes
  ): string {
    const differences = ObjectsDiff.map(
      original,
      updatedAttributes,
      new Map<string, Difference>()
    ) as Map<string, Difference>;

    let details: string;
    if (differences.size) {
      details = `The following ${differences.size > 1 ? 'values' : 'value'} ${
        differences.size > 1 ? 'are' : 'is'
      } updated by ${loggedInUser.username} \n`;
      for (const [key, value] of differences) {
        details += `${key
          .replace(/([A-Z])/g, ' $1')
          .replace(/^./, function (str) {
            return str.toUpperCase();
          })} is ${value.type} ${value.data} \n`;
      }
    } else {
      details = `No changes detected in this attributes update by ${loggedInUser.username}.`;
    }
    this.logger.debug(details);
    return details;
  }

  async updateSnooze(
    caseId: string,
    snoozeState: Case.SnoozeState,
    ifMatch: string,
    userId: string
  ) {
    return this.caseService.updateSnooze(caseId, snoozeState, ifMatch, userId);
  }

  async unarchive(
    unarchiveCasesDto: UnArchiveCasesDto
  ): Promise<UnarchiveCasesResult> {
    const uniqueUnarchivedCases = [...new Set(unarchiveCasesDto.caseIds)];

    const archivedCases = await this.archivedCaseService.findAllAsync({
      _id: { $in: uniqueUnarchivedCases },
    });
    const archivedCaseIds = archivedCases.map(
      (archivedCase) => archivedCase.id
    );

    const casesNotFoundInArchive = uniqueUnarchivedCases.filter(
      (x) => !archivedCaseIds.includes(x)
    );
    const caseFoundInArchive = uniqueUnarchivedCases.filter(
      (x) => !casesNotFoundInArchive.includes(x)
    );

    const cases = await this.caseService.findAllAsync({
      _id: { $in: caseFoundInArchive },
    });
    const caseIds = cases.map((opxCase) => opxCase.id);

    const casesFoundInOrig = caseFoundInArchive.filter((x) =>
      caseIds.includes(x)
    );
    const casesToBeUnarchived = caseFoundInArchive.filter(
      (x) => !casesFoundInOrig.includes(x)
    );

    if (casesToBeUnarchived.length === 0) {
      throw new NotFoundException('No case found to be unarchived');
    }

    const session: ClientSession = await this.caseService.startSession();

    await session.withTransaction(async () => {
      const { unarchiveCases, unarchiveCaseDetails, unarchiveActivityNotes } =
        unarchiveUtility(
          this.logger,
          this.caseService,
          this.archivedCaseService,
          this.caseDetailsService,
          this.archivedCaseDetailsService,
          this.activityNoteService,
          this.archivedActivityNoteService
        );

      const unarchivedCaseResponse: UnarchivedCaseResponse =
        await unarchiveCases(casesToBeUnarchived, session);

      await unarchiveCaseDetails(
        unarchivedCaseResponse.caseDetailsIds,
        session
      );

      await unarchiveActivityNotes(unarchivedCaseResponse.caseIds, session);
    });

    return {
      casesNotFoundInArchive: casesNotFoundInArchive,
      casesFoundInOrig,
      successfullyUnarchivedCases: casesToBeUnarchived,
    };
  }

  async updateLastSeen(caseId: string, lastSeen: LastSeen, userId: string) {
    try {
      await this.caseService.validateUpdateLastSeenRequest(caseId, userId);
    } catch (error) {
      throw new BadRequestException(error.message);
    }

    return this.caseService.updateLastSeen(caseId, lastSeen);
  }

  async getLastReceivedMessages(
    userId: string
  ): Promise<CaseLastReceivedMessageTime[]> {
    const cases: Case[] = await this.caseService.findAllAsync(
      { 'state.assignedTo.userRef.user': BaseService.toObjectId(userId) },
      { projection: 'caseDetails' }
    );

    const caseDetails: CaseDetails[] =
      await this.caseDetailsService.findAllAsync(
        { _id: { $in: cases.map((opxCase) => opxCase.caseDetails) } },
        { projection: 'asOf' }
      );

    return cases.map((opxCase) => ({
      caseId: opxCase._id,
      lastReceivedTime: caseDetails.find(
        (caseDetail) =>
          caseDetail._id.toString() === opxCase.caseDetails.toString()
      ).asOf,
    }));

    // //we can use asOf, but let's keep it here.
    // const caseDetails = await this.caseDetailsService.aggregate([
    //   {
    //     $match: {
    //       '_id': { $in: cases.map(case => opxCase.caseDetails) }
    //     }
    //   },
    //   {
    //     $project: {
    //       "_id":1,
    //       'emails': {
    //         $reduce: {
    //           input: {
    //             $concatArrays: ['$emailConversations.emails']
    //           },
    //           initialValue: [],
    //           in: { $setUnion: ['$$this', '$$value'] }
    //         }
    //       },
    //       'chats': {
    //         $reduce: {
    //           input: {
    //             $concatArrays: ['$chatConversations.messages']
    //           },
    //           initialValue: [],
    //           in: { $setUnion: ['$$this', '$$value'] }
    //         }
    //       }
    //     }
    //   },
    //   {
    //     $project:{
    //       '_id': 1,
    //       'emailTimes':'$emails.receivedDateTime',
    //       'chatTimes':'$chats.timestamp'
    //     }
    //   },
    //   {
    //     $project: {
    //       "_id":1,
    //       'receivedTimes':{$concatArrays: ['$emailTimes','$chatTimes' ]}
    //     }
    //   },
    //   {
    //     $project: {
    //       '_id': 1,
    //       'lastReceivedTime': { '$max': '$receivedTimes' },
    //     }
    //   }
    // ]);

    // return cases.map(case=>{
    //   const caseDetail = caseDetails.find(cd=>cd._id.toString() === opxCase.caseDetails.toString());
    //   return {caseId:opxCase._id, lastReceivedTime:caseDetail["lastReceivedTime"]};
    // });
  }

  async getCaseCreateForms() {
    const caseTypes = await this.caseTypeService.findAllAsync();

    return caseTypes
      .filter((c) => c?.uiForms?.caseCreate?.enabled === true)
      .map((c) => {
        return {
          title: c.uiForms.caseCreate.title,
          description: c.uiForms.caseCreate.description,
          id: c.name,
          caseType: c.name,
          formType: 'case-create',
        };
      });
  }

  async getCaseCreateFormById(id: string) {
    const caseTypes = await this.caseTypeService.findAllAsync();
    const caseType = caseTypes.find((c) => c.name === id);
    if (caseType == null) {
      throw new Error(`no case-create form found for id: ${id}`);
    }

    return {
      title: caseType.uiForms.caseCreate.title,
      description: caseType.uiForms.caseCreate.description,
      id: caseType.name,
      caseType: caseType.name,
      formType: 'case-create',
    };
  }
}
