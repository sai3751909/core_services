import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Headers,
  HttpCode,
  HttpStatus,
  Logger,
  Param,
  Post,
  Put,
  Delete,
  Query,
  Req,
  Res,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiExcludeEndpoint,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import {
  CaseDocument,
  InvalidArguments,
  OpsmatixLogger,
  parseBoolean,
  RequiredFeaturePermission,
} from '@opsmatix/core';
import {
  BulkAssignmentRequest,
  BulkIgnoreRequest,
  Case,
  CaseAssignmentRequest,
  UpdateCaseTypeRequest,
  FeaturePermissions,
  NewCaseRequestEvent,
  NewChildCaseRequestEvent,
  UnArchiveCasesDto,
  User,
  TransferCasesRequest,
  transferCasesSchema,
  TransferCasesTeamsRequest,
  transferCasesTeamsRequestSchema,
  TransferCasesListRequest,
  transferCasesListRequestSchema,
  bulkAssignmentSchema,
  bulkIgnoreSchema,
  caseIdRequestSchema,
  CaseIdRequest,
  newCaseRequestEventSchema,
  UpdateCaseTitleRequest,
  updateCaseTitleRequestEventSchema,
} from '@opsmatix/interfaces';
import { AppService } from './app.service';
import { ErrorHandlerInterceptor } from './error-handler.interceptor';
import { ResponseEtagInterceptor } from './interceptor/response-etag-interceptor';
import { JoiValidationPipe } from '@opsmatix/core';
import {
  DashboardSearchDto,
  dashboardSearchSchema,
} from '@opsmatix/interfaces';
import LastSeen = Case.LastSeen;
import SnoozeState = Case.SnoozeState;

@ApiTags('cases')
@Controller('cases')
@UseInterceptors(ErrorHandlerInterceptor)
export class AppController {
  private readonly logger: Logger = new OpsmatixLogger(
    `Object API: ${AppController.name}`
  );

  constructor(private readonly appService: AppService) {}

  //FIXME-ENG-223: this should be post
  @Get('create-process/:processName')
  @RequiredFeaturePermission(FeaturePermissions.SubProcess.Admin)
  @ApiBearerAuth()
  async createProcess(
    @Req() request,
    @Res() response,
    @Param() param
  ): Promise<any> {
    return await response.json(
      await this.appService.createProcess(param.processName)
    );
  }

  //FIXME-ENG-223: this should be post
  @Post('create-process-instance/:caseId/:processName')
  @RequiredFeaturePermission(FeaturePermissions.SubProcess.Update)
  @ApiBearerAuth()
  async triggerCreateProcessInstance(
    @Req() request,
    @Res() response,
    @Param() param
  ): Promise<any> {
    return await response.json(
      await this.appService.triggerCreateProcessInstance(
        param.caseId,
        param.processName
      )
    );
  }

  @Post('cancel-process-instance/:processInstanceId')
  @RequiredFeaturePermission(FeaturePermissions.SubProcess.Update)
  @ApiBearerAuth()
  async cancelProcessInstance(
    @Req() request,
    @Res() response,
    @Param() param
  ): Promise<any> {
    return await response.json(
      await this.appService.cancelProcessInstance(param.processInstanceId)
    );
  }

  @Get('snapshot')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  async getCases(
    @Req() request,
    @Res() response,
    @Query() query
  ): Promise<CaseDocument[]> {
    return response.json(
      await this.appService.getCases(request.params.auth_user, query)
    );
  }

  @Get()
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  async getCasesByParentId(
    @Req() request,
    @Res() response,
    @Query('pid') pid: string
  ): Promise<CaseDocument[]> {
    return response.json(await this.appService.getCasesByParentId(pid));
  }

  @Get('snapshot/:caseId')
  @ApiParam({
    name: 'Id',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  async getUpdatedCase(
    @Param(new JoiValidationPipe(caseIdRequestSchema))
    caseIdRequest: CaseIdRequest
  ): Promise<Record<string, any>> {
    return this.appService.getUpdatedCase(caseIdRequest.caseId);
  }

  @Get('case-counts')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  async getCaseCounts(
    @Req() request,
    @Res() response,
    @Query() query
  ): Promise<any> {
    return response.json(
      await this.appService.getCaseCounts(request.params.auth_user, query)
    );
  }

  //FIXME: this should replaced/merged with case-types (when new case type replaces the old one)
  @Get('triage-case-types/:caseId')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  @ApiParam({
    name: 'Id',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  async getTriageCaseTypes(@Param() param): Promise<any> {
    return this.appService.getTriageCaseTypes(param.caseId);
  }

  @Put(':caseId/update-title')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  @ApiParam({
    name: 'Id',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  async updateCaseTitle(
    @Req() request,
    @Res() response,
    @Param() param,
    @Body(new JoiValidationPipe(updateCaseTitleRequestEventSchema))
    caseTitleRequest: UpdateCaseTitleRequest
  ) {
    return response.json(
      await this.appService.updateCaseTitle(
        param.caseId,
        caseTitleRequest.title,
        request.params.auth_user._id
      )
    );
  }

  @Get('case-types')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  async getCaseTypes(@Res() response): Promise<any> {
    return response.json(await this.appService.getCaseTypes());
  }
  @Put('case-type/:caseId')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  @ApiParam({
    name: 'Id',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  async updateCaseType(
    @Req() request,
    @Res() response,
    @Param() param,
    @Body() caseTypeRequest: UpdateCaseTypeRequest
  ) {
    return response.json(
      await this.appService.updateCaseType(
        param.caseId,
        caseTypeRequest.caseType,
        request.params.auth_user._id
      )
    );
  }

  @Get('dashboard-cases')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  async getDashboardCases(
    @Req() request,
    @Res() response,
    @Query(new JoiValidationPipe<DashboardSearchDto>(dashboardSearchSchema))
    query: DashboardSearchDto
  ): Promise<CaseDocument[]> {
    const additionalCaseFilters = {};
    for (const p of Object.keys(request.query)) {
      if (p.startsWith('filter:')) {
        const par = p.replace('filter:', '');
        additionalCaseFilters['caseAttributes.' + par] = request.query[p];
      }
    }

    return response.json(
      await this.appService.getDashboardCases(
        request.params.auth_user,
        query,
        Object.keys(additionalCaseFilters).length === 0
          ? null
          : additionalCaseFilters
      )
    );
  }

  @Get(':caseId')
  @UseInterceptors(new ResponseEtagInterceptor())
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  @ApiParam({
    name: 'Id',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  async getCase(@Param() param, @Req() request): Promise<CaseDocument> {
    const user: User = request.params['auth_user'];
    return this.appService.getCaseDetail(param.caseId, user);
  }

  @Put('claim/:caseId')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  @ApiParam({
    name: 'Id',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  async claimCase(@Req() request, @Res() response, @Param() param) {
    return response.json(
      await this.appService.claimCase(
        request.params.auth_user._id,
        param.caseId
      )
    );
  }

  @Put('unclaim/:caseId')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  @ApiParam({
    name: 'Id',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  async unclaimCase(@Req() request, @Res() response, @Param() param) {
    return response.json(
      await this.appService.unclaimCase(
        param.caseId,
        request.params.auth_user.id
      )
    );
  }

  @Get('case-transfer-teams/:fromUserId/:toUserId')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Admin)
  @ApiBearerAuth()
  public async getCaseTransferTeams(
    @Req() request,
    @Res() res,
    @Param(
      new JoiValidationPipe<TransferCasesTeamsRequest>(
        transferCasesTeamsRequestSchema
      )
    )
    transferCasesTeamsRequest
  ) {
    const teams = await this.appService.getTransferCasesTeams(
      transferCasesTeamsRequest,
      request.params.auth_user.id
    );
    return res.status(HttpStatus.OK).json(teams);
  }

  @Get('transfer-cases-list/:fromUserId')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Admin)
  @ApiBearerAuth()
  public async getCaseTransferCases(
    @Req() request,
    @Res() res,
    @Param('fromUserId') fromUserId: string,
    @Query('teamIds') teamIds: string
  ): Promise<void> {
    const teamIdsArray = teamIds.split(',');
    const cases = await this.appService.getTransferCasesList(
      fromUserId,
      teamIdsArray
    );
    return res.status(HttpStatus.OK).json(cases);
  }

  @Put('transfer-all-cases')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Admin)
  @ApiBearerAuth()
  async transferAllCases(
    @Req() request,
    @Res() response,
    @Body(new JoiValidationPipe<TransferCasesRequest>(transferCasesSchema))
    transferCaseRequest: TransferCasesRequest
  ) {
    try {
      return response.json(
        await this.appService.transferAllCases(
          transferCaseRequest,
          request.params.auth_user.id
        )
      );
    } catch (ex) {
      this.logger.error('Exception thrown on transfer-all-cases', ex);
      throw ex;
    }
  }

  @Put('assign/:caseId')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  @ApiParam({
    name: 'Id',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  @ApiParam({
    name: 'username',
    description: 'Username of the user tha you want to assign the case to',
    required: true,
    type: 'string',
  })
  async assignUser(
    @Res() response,
    @Req() request,
    @Param() param,
    @Body() caseAssignRequest: CaseAssignmentRequest
  ) {
    return response.json(
      await this.appService.assign(
        param.caseId,
        caseAssignRequest,
        request.params.auth_user
      )
    );
  }

  @Put('bulk-assign')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Admin)
  @ApiBearerAuth()
  @ApiParam({
    name: 'Id',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  @ApiParam({
    name: 'username',
    description: 'Username of the user that you want to assign the case to',
    required: true,
    type: 'string',
  })
  async bulkAssignUser(
    @Req() request,
    @Res() response,
    @Body(new JoiValidationPipe<BulkAssignmentRequest>(bulkAssignmentSchema))
    bulkAssignmentRequest: BulkAssignmentRequest
  ) {
    try {
      await this.appService.bulkAssignUser(
        bulkAssignmentRequest,
        request.params.auth_user
      );
      return response.json({ success: true });
    } catch (err) {
      this.logger.error('Exception thrown on bulk-assign', err);

      throw err;
    }
  }

  @Put('bulk-ignore')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Admin)
  @ApiBearerAuth()
  async bulkIgnore(
    @Req() request,
    @Res() response,
    @Body(new JoiValidationPipe<BulkIgnoreRequest>(bulkIgnoreSchema))
    bulkIgnoreRequest: BulkIgnoreRequest
  ) {
    try {
      await this.appService.bulkIgnore(
        bulkIgnoreRequest,
        request.params.auth_user.id
      );
      return response.json({ success: true });
    } catch (err) {
      this.logger.error('Exception thrown on bulk-ignore', err);
      throw err;
    }
  }

  @Put('accept_classification/:caseId')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  @ApiParam({
    name: 'Id',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  async acceptClassification(
    @Req() request,
    @Res() response,
    @Param() param,
    @Query('sendAck') sendAck = false
  ) {
    return response.json(
      await this.appService.acceptClassification(
        param.caseId,
        parseBoolean(sendAck),
        request.params.auth_user.id
      )
    );
  }

  /**
   * REDUNDANT CODE: duplicated function as @Put('undo-accept/:caseId') written below.
   * Please review and verify before removing.
   */

  // @Put('unaccept_classification/:caseId')
  // @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  // @ApiBearerAuth()
  // @ApiParam({
  //   name: 'Id',
  //   description: 'Case Object _id from Mongo',
  //   required: true,
  //   type: 'string',
  // })
  // async unacceptClassification(
  //   @Req() request,
  //   @Res() response,
  //   @Param() param,
  //   @Query('sendAck') sendAck = false
  // ) {
  //   return response.json(
  //     await this.appService.unacceptClassification(
  //       param.caseId,
  //       parseBoolean(sendAck),
  //       request.params.auth_user.id
  //     )
  //   );
  // }

  @Put('undo-accept/:caseId')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  @ApiParam({
    name: 'Id',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  async undoAcceptCase(@Req() request, @Res() response, @Param() param) {
    return response.json(
      await this.appService.undoAcceptCase(
        param.caseId,
        request.params.auth_user.id
      )
    );
  }

  @Put('close/:caseId')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  @ApiParam({
    name: 'Id',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  async closeCase(
    @Req() request,
    @Res() response,
    @Param() param,
    @Query('sendAck') sendAck = false
  ) {
    return response.json(
      await this.appService.closeCase(param.caseId, request.params.auth_user.id)
    );
  }
  @Put('accept-and-close/:caseId')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  @ApiParam({
    name: 'Id',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  async acceptAndCloseCase(@Req() request, @Res() response, @Param() param) {
    return response.json(
      await this.appService.acceptAndCloseCase(
        param.caseId,
        request.params.auth_user.id
      )
    );
  }

  @Put('reopen/:caseId')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  @ApiParam({
    name: 'Id',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  async reopenCase(@Req() request, @Res() response, @Param() param) {
    return response.json(
      await this.appService.reopenCase(
        param.caseId,
        request.params.auth_user.id
      )
    );
  }

  @Put('merge/:caseId/to/:newCaseId')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  @ApiParam({
    name: 'Id',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  @ApiParam({
    name: 'caseId',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  @ApiParam({
    name: 'newCaseId',
    description: 'Case to merge.',
    required: true,
    type: 'string',
  })
  async mergeToCase(@Req() request, @Res() response, @Param() param) {
    return response.json(
      await this.appService.mergeCase(
        request.params.caseId,
        param.newCaseId,
        request.params.auth_user.id
      )
    );
  }

  @Put('merge-many/:mergeTargetCaseId')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  @ApiParam({
    name: 'mergeTargetCaseId',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  async mergeAll(@Req() request, @Param() param, @Body() caseIds: string[]) {
    if (caseIds && caseIds.length > 0) {
      this.logger.log(`merging following caseIds: ${JSON.stringify(caseIds)}`);
      //validate all the requests first before firing the bulk merge.
      for (const caseId of caseIds) {
        await this.appService.validateMergeCaseRequest(
          caseId,
          param.mergeTargetCaseId
        );
      }

      for (const caseId of caseIds) {
        await this.appService.mergeCase(
          caseId,
          param.mergeTargetCaseId,
          request.params.auth_user.id
        );
      }

      return { success: true };
    } else {
      throw new BadRequestException({
        success: false,
        error: 'no caseIds found in the payload',
      });
    }
  }

  @Put('createChild/:parentCaseId')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  @ApiParam({
    name: 'parentCaseId',
    description: 'CaseId of parent from which child case to be created!',
    required: true,
    type: 'string',
  })
  async createChildCase(
    @Req() request,
    @Param() param,
    @Body() reqPayload: NewChildCaseRequestEvent
  ) {
    if (param.parentCaseId === reqPayload.parentCaseId) {
      this.logger.log(
        `creating child case with the following: ${JSON.stringify(reqPayload)}`
      );
      await this.appService.createChildCase(reqPayload).catch((err) => {
        this.logger.error(
          `Failed to submit request for child case, error message is : ${err.message}`
        );
        throw new BadRequestException({ success: false, error: err.message });
      });
      return { success: true };
    } else {
      throw new BadRequestException({
        success: false,
        error: 'invalid request!',
      });
    }
  }

  @Get('forms/case-create')
  @ApiBearerAuth()
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.NewCase)
  async getCaseCreateForms() {
    return await this.appService.getCaseCreateForms();
  }

  @Get('forms/case-create/:id')
  @ApiBearerAuth()
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.NewCase)
  async getCaseCreateFormById(@Param('id') id: string) {
    return await this.appService.getCaseCreateFormById(id);
  }

  @Post('create')
  @ApiBearerAuth()
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.NewCase)
  async createCase(
    @Req() request,
    @Body(new JoiValidationPipe(newCaseRequestEventSchema))
    newCase: NewCaseRequestEvent
  ) {
    if (newCase) {
      newCase.requester = {
        userId: request.params.auth_user.id,
        email: request.params.auth_user.username,
      };
    }

    const caseTypes = await this.appService.getCaseTypes();
    //find orgunit associated with casetype
    const newCaseCaseType = newCase.caseType;
    const newCaseOrgUnit = caseTypes.find(
      (c) => c.name === newCaseCaseType
    )?.orgUnit;
    newCase.orgUnit = newCaseOrgUnit;
    if (caseTypes?.find((c) => c.name === newCase.caseType) == null) {
      throw new BadRequestException({
        success: false,
        error: `CaseType[${newCase.caseType}] is not valid!`,
      });
    }
    return this.appService
      .createCase(newCase)
      .then(() => {
        return { success: true };
      })
      .catch((err) => {
        this.logger.error(
          `Failed to submit request for new case, error message is : ${err.message}`
        );
        throw new BadRequestException({ success: false, error: err.message });
      });
  }

  @Put('ignore/:caseId')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  @ApiParam({
    name: 'Id',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  async ignoreCase(@Req() request, @Res() response, @Param() param) {
    return response.json(
      await this.appService.ignoreCase(
        param.caseId,
        request.params.auth_user.id
      )
    );
  }

  @Post(':caseId/user-note')
  @RequiredFeaturePermission(FeaturePermissions.ActivityNote.Update)
  @ApiBearerAuth()
  @ApiParam({
    name: 'Id',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  async addNote(
    @Body()
    body: {
      title: string;
      body: {
        contentType: string;
        content: string;
      };
    },
    @Req() request,
    @Res() response,
    @Param() param
  ) {
    return response.json(
      await this.appService.addUserNote(
        param.caseId,
        request.params.auth_user.id,
        body.body
      )
    );
  }
  @Delete(':caseId/user-note/:noteIndex')
  @RequiredFeaturePermission(FeaturePermissions.ActivityNote.Update)
  @ApiBearerAuth()
  @ApiParam({
    name: 'caseId',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  @ApiParam({
    name: 'noteIndex',
    description: 'Index of the note in the userNotes array',
    required: true,
    type: 'number',
  })
  async deleteNote(@Req() request, @Res() response, @Param() param) {
    return response.json(
      await this.appService.deleteUserNote(
        param.caseId,
        request.params.auth_user.id,
        param.noteIndex
      )
    );
  }

  @Post(':caseId/last-seen')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiParam({
    name: 'Id',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  async updateLastSeen(
    @Param('caseId') caseId: string,
    @Body() lastSeen: LastSeen,
    @Req() request
  ) {
    await this.appService.updateLastSeen(
      caseId,
      lastSeen,
      request.params.auth_user.id
    );
  }

  @Get(':caseId/activity_notes')
  @RequiredFeaturePermission(FeaturePermissions.ActivityNote.Read)
  @ApiBearerAuth()
  @ApiParam({
    name: 'Id',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  async getAcvitityNotes(@Param('caseId') caseId: string, @Res() response) {
    return response.json(await this.appService.getActivityNotes(caseId));
  }

  @Get(':caseId/find_similar_cases')
  @RequiredFeaturePermission(FeaturePermissions.SimilarCases.Read)
  @ApiBearerAuth()
  @ApiParam({
    name: 'caseId',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  async findSimilarCases(@Param('caseId') caseId: string, @Res() response) {
    try {
      return response.json(await this.appService.findSimilarCases(caseId));
    } catch (ex) {
      if (ex instanceof InvalidArguments) {
        throw new BadRequestException(ex.message);
      }
      throw ex;
    }
    // return response.json([{
    //   id:"610a6976bef1905610744056",
    //   status:Case.State.Status.CLAIMED,
    //   assignee:'Sateesh',
    //   title:'Message Center Major Change Update Notification'
    // },{
    //   id:"610a6973bef190561074404f",
    //   status:Case.State.Status.CLAIMED,
    //   assignee:'Ilter',
    //   title:'Message Center Microsoft'
    // }]);
  }

  @Get(':caseId/find_related_cases')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  @ApiParam({
    name: 'caseId',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  async findRelatedCases(
    @Param('caseId') caseId: string,
    @Query('limit') limit: number,
    @Res() response
  ) {
    try {
      return response.json(
        await this.appService.findRelatedCases(caseId, limit)
      );
    } catch (ex) {
      if (ex instanceof InvalidArguments) {
        throw new BadRequestException(ex.message);
      }
      throw ex;
    }
  }

  @Get(':caseId/process-instances')
  @RequiredFeaturePermission(FeaturePermissions.SubProcess.Read)
  @ApiBearerAuth()
  @ApiParam({
    name: 'caseId',
    description: 'Case Object _id from Mongo',
    required: true,
    type: 'string',
  })
  async findProcessInstances(@Param('caseId') caseId: string) {
    return this.appService.findProcessInstances(caseId);
  }

  //TODO: Move it to separate resource
  @Get(':caseId/processes')
  @RequiredFeaturePermission(FeaturePermissions.SubProcess.Update)
  @ApiBearerAuth()
  async findAllProcesses() {
    return this.appService.findAllProcesses();
  }

  @Put(':caseId/update-attributes/')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiBearerAuth()
  async updateCalastoneCaseDetails(
    @Headers('if-match') ifMatch: string,
    @Param('caseId') caseId: string,
    @Body() attributes: Record<string, string | number | boolean>,
    @Req() request
  ) {
    if (!ifMatch) {
      this.logger.error(
        `if-match header not found. Throwing BadRequestException for caseId:${caseId}`
      );
      throw new BadRequestException(
        `If-Match doesn't exist in request headers`
      );
    }
    const user: User = request.params['auth_user'];
    return this.appService.updateAttributes(user, caseId, attributes, ifMatch);
  }
  @Put(':caseId/snooze/')
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Update)
  @ApiExcludeEndpoint()
  @ApiBearerAuth()
  async updateSnooze(
    @Headers('if-match') ifMatch: string,
    @Param('caseId') caseId: string,
    @Body() snoozeState: SnoozeState,
    @Req() request
  ) {
    if (!ifMatch) {
      this.logger.error(
        `BadRequestException: if-match header not found for case id: ${caseId}`
      );
      throw new BadRequestException('If-Match not found in request header');
    }
    return this.appService.updateSnooze(
      caseId,
      snoozeState,
      ifMatch,
      request.params.auth_user._id
    );
  }

  @Post('unarchive')
  @ApiBearerAuth()
  @RequiredFeaturePermission(FeaturePermissions.CaseManagement.Admin)
  @HttpCode(HttpStatus.OK)
  async unarchive(@Body() unarchiveCasesDto: UnArchiveCasesDto) {
    return this.appService.unarchive(unarchiveCasesDto);
  }
}
