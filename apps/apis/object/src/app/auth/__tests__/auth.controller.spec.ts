import * as request from 'supertest';
import {
  CanActivate,
  ConsoleLogger,
  ExecutionContext,
  HttpStatus,
  INestApplication,
} from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { APP_GUARD } from '@nestjs/core';
import { Observable } from 'rxjs';
import { ObjectAPIAuthService } from '../auth.service';
import { AuthController } from '../auth.controller';
import { mock } from 'jest-mock-extended';
import { UserService } from '@opsmatix/core';

jest.mock('../../environments/environment', () => ({
  environment: {},
}));
export class MockAuthGuard implements CanActivate {
  canActivate(
    context: ExecutionContext
  ): boolean | Promise<boolean> | Observable<boolean> {
    return true;
  }
}

describe('Auth Controller tests', () => {
  const objectAPIAuthService = mock<ObjectAPIAuthService>();
  const userService = mock<UserService>();
  let app: INestApplication;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      providers: [
        {
          provide: ObjectAPIAuthService,
          useValue: objectAPIAuthService,
        },
        {
          provide: UserService,
          useValue: userService,
        },
        {
          provide: APP_GUARD,
          useClass: MockAuthGuard,
        },
      ],
      controllers: [AuthController],
    })
      .setLogger(new ConsoleLogger())
      .compile();
    app = module.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  beforeEach(async () => {
    jest.resetAllMocks();
  });

  describe('dummy test cases to retain the spec', () => {
    it(`/POST dummy`, async () => {
      console.log('dummy test case');
    });
  });
});
