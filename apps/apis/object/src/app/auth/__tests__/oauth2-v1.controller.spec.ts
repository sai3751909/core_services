import * as request from 'supertest';
import { mock } from 'jest-mock-extended';
import {
  CanActivate,
  ConsoleLogger,
  ExecutionContext,
  HttpStatus,
  INestApplication,
} from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { Observable } from 'rxjs';
import { OAuth2ControllerV1 } from '../oauth2-v1.controller';
import { ObjectAPIAuthService } from '../auth.service';

jest.mock('../../environments/environment', () => ({
  environment: {},
}));

export class MockAuthGuard implements CanActivate {
  canActivate(
    context: ExecutionContext
  ): boolean | Promise<boolean> | Observable<boolean> {
    return true;
  }
}

const objectAPIAuthService = mock<ObjectAPIAuthService>();

describe('OAuth 2.0 Controller tests', () => {
  let app: INestApplication;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      controllers: [OAuth2ControllerV1],
      providers: [
        {
          provide: ObjectAPIAuthService,
          useValue: objectAPIAuthService,
        },
      ],
    })
      .setLogger(new ConsoleLogger())
      .compile();
    app = module.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  beforeEach(async () => {
    jest.resetAllMocks();
  });

  describe('client credential flow', () => {
    it(`/POST get token`, () => {
      objectAPIAuthService.loginClientCredentials.mockResolvedValue({
        accessToken: 'abc',
        expiresIn: 100,
      });
      return request(app.getHttpServer())
        .post('/oauth2/token')
        .send({
          grant_type: 'client_credentials',
          client_id: '3b49d566-df31-4731-8dcc-a3cf08aa2279',
          secret_key: '654321654321deN!',
        })
        .expect(200)
        .expect({
          access_token: 'abc',
          expires_in: 100,
          token_type: 'Bearer',
        });
    });

    it(`/POST get token with expiry string from backend`, () => {
      objectAPIAuthService.loginClientCredentials.mockResolvedValue({
        accessToken: 'abc',
        expiresIn: '100',
      });
      return request(app.getHttpServer())
        .post('/oauth2/token')
        .send({
          grant_type: 'client_credentials',
          client_id: '3b49d566-df31-4731-8dcc-a3cf08aa2279',
          secret_key: '654321654321deN!',
        })
        .expect(200)
        .expect({
          access_token: 'abc',
          expires_in: 100,
          token_type: 'Bearer',
        });
    });

    it(`/POST get token unsupported grant_type`, () => {
      objectAPIAuthService.loginClientCredentials.mockResolvedValue({
        accessToken: 'abc',
        expiresIn: '100',
      });
      return request(app.getHttpServer())
        .post('/oauth2/token')
        .send({
          grant_type: 'unknown',
          client_id: '3b49d566-df31-4731-8dcc-a3cf08aa2279',
          secret_key: '654321654321deN!',
        })
        .expect(HttpStatus.UNAUTHORIZED)
        .expect({
          statusCode: 401,
          message: 'Invalid grant_type',
          error: 'Unauthorized',
        });
    });
  });
});
