import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { APP_GUARD } from '@nestjs/core';
import { TerminusModule } from '@nestjs/terminus';
import {
  ArchivedActivityNoteModule,
  ArchivedCaseDetailsModule,
  ArchivedCaseModule,
  AuditModule,
  AuthModule,
  AUTH_SERVICE_CONFIG,
  CaseDetailsModule,
  CoreModule,
  ProcessInstanceModule,
  ProcessModule,
  CaseModule,
  TeamModule,
  UserModule,
  PreferenceModule,
  UserTaskModule,
  RoleModule,
  UserGuard,
  HealthReporterModule,
  OrgUnitModule,
  ContactListModule,
  CaseTypeModule,
  EntityTypeModule,
  OpsmatixPrometheusModule,
} from '@opsmatix/core';
import { ZeebeModule, ZeebeServer } from 'nestjs-zeebe';
import { environment } from '../environments/environment';
import { AppInitModule } from './app-init/app-init.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ObjectAPIAuthModule } from './auth/auth.module';
import { HealthController } from './health.controller';
import { PortalConfigAPIModule } from './portal-config/portal-config.module';
import { RolesModule } from './roles/roles.module';
import { TeamsModule } from './teams/teams.module';
import { UsersModule } from './users/users.module';
import { PreferencesModule } from './user-preferences/user-preferences.module';
import { MessageChannelsModules } from './message-channel/message-channel.module';
import { SetupModule } from './setup/setup.module';
import { AiModule } from './ai/ai.module';
import { OrganisationUnitModule } from './org-unit/org-unit.module';
import { MailboxesModule } from './mailboxes/mailboxes.module';
import { ContactListsModule } from './contact-lists/contact-lists.module';
import { CaseTypesModule } from './case-types/case-types.module';
import { EntityTypesModule } from './entity-types/entity-types.module';
import { TagsModule } from './tags/tags.module';
import { ReferenceDataModule } from './reference-data/reference-data.module';
import { CasesModule } from './cases/cases.module';
import { BulkUpdateController } from './bulk-update/bulk-update.controller';
import { BulkUpdateService } from './bulk-update/bulk-update.service';
import { SchemaValidationService } from './schema-validation/schema-validation.service';
@Module({
  imports: [
    CoreModule.forRoot(environment.mongodb.url),
    TerminusModule,
    AuthModule.forRoot(AUTH_SERVICE_CONFIG),
    UserModule,
    TeamModule,
    TeamsModule,
    CaseDetailsModule,
    AuditModule,
    ProcessModule,
    ProcessInstanceModule,
    UserTaskModule,
    CaseModule,
    CaseTypeModule,
    ContactListModule,
    UsersModule,
    RoleModule,
    RolesModule,
    PortalConfigAPIModule,
    ArchivedCaseModule,
    ArchivedCaseDetailsModule,
    ArchivedActivityNoteModule,
    ObjectAPIAuthModule,
    ZeebeModule.forRoot({}),
    HttpModule,
    AppInitModule,
    HealthReporterModule,
    PreferenceModule,
    PreferencesModule,
    MessageChannelsModules,
    OrgUnitModule,
    SetupModule,
    AiModule,
    ContactListsModule,
    MailboxesModule,
    OrganisationUnitModule,
    CaseTypesModule,
    EntityTypeModule,
    EntityTypesModule,
    TagsModule,
    OpsmatixPrometheusModule,
    ReferenceDataModule,
    CasesModule,
  ],
  controllers: [AppController, HealthController, BulkUpdateController],
  providers: [
    AppService,
    ZeebeServer,
    {
      provide: APP_GUARD,
      useClass: UserGuard,
    },
    BulkUpdateService,
    SchemaValidationService,
  ],
})
export class AppModule {}
