/* eslint-disable @typescript-eslint/ban-ts-comment */
import { AppService } from './app.service';
import { mock } from 'jest-mock-extended';
import {
  ActivityNoteDocument,
  ActivityNoteService,
  ArchivedActivityNoteService,
  ArchivedCaseDetailsService,
  ArchivedCaseService,
  CaseDetailsService,
  CaseDocument,
  CaseService,
  CaseTypeService,
  ConfigService,
  OpsmatixLogger,
  ProcessInstanceService,
  ProcessService,
  StreamUtil,
  TeamService,
  UserService,
  UserTaskService,
  SimpleCache,
} from '@opsmatix/core';
import { ZBClient } from 'zeebe-node';
import { Case, User } from '@opsmatix/interfaces';
import { OrchestrationHelper } from './cases/orchestration-helper';
import { SchemaValidationService } from './schema-validation/schema-validation.service';

jest.mock('../environments/environment', () => ({
  environment: {
    cases: {
      schemaDrivenDataModel: false,
    },
  },
}));

const mockConfigService = mock<ConfigService>();
const mockProcessService = mock<ProcessService>();
const mockProcessInstanceService = mock<ProcessInstanceService>();
const mockUserCaseService = mock<UserTaskService>();
const mockCaseService = mock<CaseService>();
const mockCaseDetailsService = mock<CaseDetailsService>();
const mockActivityNoteService = mock<ActivityNoteService>();
const mockUserService = mock<UserService>();
const mockUserTaskService = mock<UserTaskService>();
const mockTeamService = mock<TeamService>();
const mockZbClient = mock<ZBClient>();
const mockArchivedCaseService = mock<ArchivedCaseService>();
const mockArchivedCaseDetailsService = mock<ArchivedCaseDetailsService>();
const mockArchivedActivityNoteService = mock<ArchivedActivityNoteService>();
const mockCaseTypeService = mock<CaseTypeService>();
const schemaValidationService = mock<SchemaValidationService>();
const orchestrationHelper = mock<OrchestrationHelper>();
const mockOpsmatixLogger = mock<OpsmatixLogger>();
const mockStreamUtil = mock<StreamUtil>();
const simpleCache = mock<SimpleCache>();

jest.mock('@opsmatix/core', () => ({
  BaseService: {
    toObjectId: jest.fn().mockReturnValue('caseId'),
  },
  ActivityNoteService: jest.fn(() => mockActivityNoteService),
  ArchivedActivityNoteService: jest.fn(() => mockArchivedActivityNoteService),
  ArchivedCaseDetailsService: jest.fn(() => mockArchivedCaseDetailsService),
  ArchivedCaseService: jest.fn(() => mockArchivedCaseService),
  CaseDetailsService: jest.fn(() => mockCaseDetailsService),
  StreamUtil: jest.fn(() => mockStreamUtil),
  SimpleCache: jest.fn(() => simpleCache),
  ConfigService: jest.fn(() => ({
    getInstance: mockConfigService,
  })),
  GlobalConfig: {
    namespace: 'dev-client-impl',
    ports: {
      portal: 4200,
      api: {
        email: 8201,
        chat: 8202,
        object: 8205,
        data: 8209,
        config: 8211,
        sso: 8204,
        stream: 8206,
        report: 8207,
        clientServices: 8208,
      },
      controllers: {
        email: 8501,
        orchestration: 8502,
        archive: 8503,
        chat: 8504,
      },
      services: {
        mongo: 27017,
        rabbitmq: 5672,
        classifier: 8000,
      },
    },
    kafka_topics: {
      event_stream: 'dev-client-impl-event-stream',
      omnichannel_ingestion: 'dev-client-impl-omnichannel-ingestion',
    },
    queues: {
      event_stream: 'dev-client-impl_queue_event_stream',
      omnichannel_ingestion: 'dev-client-impl_queue_omnichannel_ingestion',
    },
    queue_options_noAck: false,
    zeebe: {
      gateway: 'ZEEBE_ADDRESS',
      health_check_url: 'ZEEBE_HEALTH_CHECK_URL',
      directoryToLoadProcessDefinitions: './assets/zeebe',
      defaultWorkerOptions: {
        maxJobsToActivate: 5,
        longPoll: 30000,
        timeout: 300000,
      },
    },
    kafka: {
      brokers: [],
    },
    zookeeper: {
      connectionString: 'ZOOKEEPER_CONNECTION_STRING',
      defaultConfig: { spinDelay: 1000, sessionTimeout: 10000 },
    },
  },
  OpsmatixLogger: jest.fn(() => mockOpsmatixLogger),
  ProcessInstanceService: jest.fn(() => mockProcessInstanceService),
  ProcessService: jest.fn(() => mockProcessService),
  CaseService: jest.fn(() => mockCaseService),
  TeamService: jest.fn(() => mockTeamService),
  UserService: jest.fn(() => mockUserService),
  UserTaskService: jest.fn(() => mockUserTaskService),
  CaseTypeService: jest.fn(() => mockCaseTypeService),
}));

const appService = new AppService(
  mockProcessService,
  mockProcessInstanceService,
  mockUserCaseService,
  mockCaseService,
  mockCaseDetailsService,
  mockActivityNoteService,
  mockUserService,
  mockTeamService,
  mockZbClient,
  mockArchivedCaseService,
  mockArchivedCaseDetailsService,
  mockArchivedActivityNoteService,
  mockCaseTypeService,
  orchestrationHelper,
  schemaValidationService
);

describe('AppService Test Suite', () => {
  it('should have initialized AppService', () => {
    expect(appService).not.toBeNull();
  });
  describe('Audit Attributes update', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });
    const getCaseDocument = () => {
      return mock<CaseDocument>({
        attributes: {
          autoAccept: false,
          //@ts-ignore
          fraudreferral: {
            data: {
              classification: 'InsufficientData',
              requesterType: 'Individual',
              requesterName: 'Operations',
              requesterEmail: 'test.operations@opsmatix.onmicrosoft.com',
              beneficiarySortCode: '',
              beneficiaryAccount: '',
              partnerType: null,
              partnerName: null,
              partnerId: null,
              partnerEmail: null,
              partnerPublicEmail: null,
              amount: null,
              currency: null,
              dateOfPayment: null,
              rootCause: null,
              thirdpartyName: null,
              remitterSortCode: null,
              remitterAccount: null,
            },
            internal: { aiResponse: [] },
          },
          zeebeProcessInstanceKey: '4503599654950406',
        },
      });
    };

    it('should log multiple attributes update detail', async () => {
      //@ts-ignore
      mockCaseService.findByIdAsync.mockResolvedValueOnce(getCaseDocument());

      const UpdatedAttributes = {
        autoAccept: false,
        fraudreferral: {
          data: {
            classification: 'InsufficientData',
            requesterType: 'Individual',
            requesterName: 'Operations',
            requesterEmail: 'test.operations@opsmatix.onmicrosoft.com',
            beneficiarySortCode: '012345',
            beneficiaryAccount: '88888888',
            partnerType: null,
            partnerName: null,
            partnerId: null,
            partnerEmail: null,
            partnerPublicEmail: null,
            amount: null,
            currency: null,
            dateOfPayment: null,
            rootCause: null,
            thirdpartyName: null,
            remitterSortCode: null,
            remitterAccount: null,
          },
          internal: { aiResponse: [] },
        },
        zeebeProcessInstanceKey: '4503599654950406',
      };

      mockCaseService.updateAsync.mockResolvedValueOnce(mock<CaseDocument>());
      mockActivityNoteService.updateAsync.mockResolvedValueOnce(
        mock<ActivityNoteDocument>()
      );

      const loggedInUser = mock<User>({ username: 'user@example.com' });

      const auditMessage =
        'The following values are updated by user@example.com \nBeneficiary Sort Code is updated to new value 012345 \nBeneficiary Account is updated to new value 88888888 \n';

      await appService.updateAttributes(
        loggedInUser,
        'caseId',
        UpdatedAttributes,
        'ifMatch'
      );

      const note = {
        entity: 'caseId',
        user: 'system',
        title: 'Attributes Update',
        details: auditMessage,
        component: 'apis-object',
      };

      expect(mockActivityNoteService.updateAsync).toHaveBeenCalledTimes(1);
      expect(mockActivityNoteService.updateAsync).toHaveBeenCalledWith(note);
      expect(mockOpsmatixLogger.debug).toHaveBeenCalledTimes(1);
      expect(mockOpsmatixLogger.debug).toHaveBeenCalledWith(auditMessage);
    });

    it('should log single attributes update detail', async () => {
      //@ts-ignore
      mockCaseService.findByIdAsync.mockResolvedValueOnce(getCaseDocument());

      const UpdatedAttributes = {
        autoAccept: false,
        fraudreferral: {
          data: {
            classification: 'InsufficientData',
            requesterType: 'Individual',
            requesterName: 'Operations',
            requesterEmail: 'test.operations@opsmatix.onmicrosoft.com',
            beneficiarySortCode: '012345',
            beneficiaryAccount: '',
            partnerType: null,
            partnerName: null,
            partnerId: null,
            partnerEmail: null,
            partnerPublicEmail: null,
            amount: null,
            currency: null,
            dateOfPayment: null,
            rootCause: null,
            thirdpartyName: null,
            remitterSortCode: null,
            remitterAccount: null,
          },
          internal: { aiResponse: [] },
        },
        zeebeProcessInstanceKey: '4503599654950406',
      };

      mockCaseService.updateAsync.mockResolvedValueOnce(mock<CaseDocument>());
      mockActivityNoteService.updateAsync.mockResolvedValueOnce(
        mock<ActivityNoteDocument>()
      );

      const loggedInUser = mock<User>({ username: 'user@example.com' });

      const auditMessage =
        'The following value is updated by user@example.com \nBeneficiary Sort Code is updated to new value 012345 \n';

      await appService.updateAttributes(
        loggedInUser,
        'caseId',
        UpdatedAttributes,
        'ifMatch'
      );

      const note = {
        entity: 'caseId',
        user: 'system',
        title: 'Attributes Update',
        details: auditMessage,
        component: 'apis-object',
      };

      expect(mockActivityNoteService.updateAsync).toHaveBeenCalledTimes(1);
      expect(mockActivityNoteService.updateAsync).toHaveBeenCalledWith(note);
      expect(mockOpsmatixLogger.debug).toHaveBeenCalledTimes(1);
      expect(mockOpsmatixLogger.debug).toHaveBeenCalledWith(auditMessage);
    });

    it('should log when there is no attributes update', async () => {
      //@ts-ignore
      mockCaseService.findByIdAsync.mockResolvedValueOnce(getCaseDocument());

      const UpdatedAttributes = {
        autoAccept: false,
        fraudreferral: {
          data: {
            classification: 'InsufficientData',
            requesterType: 'Individual',
            requesterName: 'Operations',
            requesterEmail: 'test.operations@opsmatix.onmicrosoft.com',
            beneficiarySortCode: '',
            beneficiaryAccount: '',
            partnerType: null,
            partnerName: null,
            partnerId: null,
            partnerEmail: null,
            partnerPublicEmail: null,
            amount: null,
            currency: null,
            dateOfPayment: null,
            rootCause: null,
            thirdpartyName: null,
            remitterSortCode: null,
            remitterAccount: null,
          },
          internal: { aiResponse: [] },
        },
        zeebeProcessInstanceKey: '4503599654950406',
      };

      const loggedInUser = mock<User>({ username: 'user@example.com' });

      const response =
        'No changes detected in this attributes update by user@example.com.';

      await appService.updateAttributes(
        loggedInUser,
        'caseId',
        UpdatedAttributes,
        'ifMatch'
      );

      expect(mockActivityNoteService.updateAsync).toHaveBeenCalledTimes(0);
      expect(mockOpsmatixLogger.debug).toHaveBeenCalledTimes(1);
      expect(mockOpsmatixLogger.debug).toHaveBeenCalledWith(response);
    });
    it('should not throw exception when activity note service throws exception', async () => {
      //@ts-ignore
      mockCaseService.findByIdAsync.mockResolvedValueOnce(getCaseDocument());

      const UpdatedAttributes = {
        autoAccept: false,
        fraudreferral: {
          data: {
            classification: 'InsufficientData',
            requesterType: 'Individual',
            requesterName: 'Operations',
            requesterEmail: 'test.operations@opsmatix.onmicrosoft.com',
            beneficiarySortCode: '012345',
            beneficiaryAccount: '',
            partnerType: null,
            partnerName: null,
            partnerId: null,
            partnerEmail: null,
            partnerPublicEmail: null,
            amount: null,
            currency: null,
            dateOfPayment: null,
            rootCause: null,
            thirdpartyName: null,
            remitterSortCode: null,
            remitterAccount: null,
          },
          internal: { aiResponse: [] },
        },
        zeebeProcessInstanceKey: '4503599654950406',
      };

      const loggedInUser = mock<User>({ username: 'user@example.com' });

      mockCaseService.updateAsync.mockResolvedValueOnce(mock<CaseDocument>());
      mockActivityNoteService.updateAsync.mockRejectedValueOnce(
        new Error('Boom!')
      );

      const auditMessage =
        'The following value is updated by user@example.com \nBeneficiary Sort Code is updated to new value 012345 \n';

      await appService.updateAttributes(
        loggedInUser,
        'caseId',
        UpdatedAttributes,
        'ifMatch'
      );

      const note = {
        entity: 'caseId',
        user: 'system',
        title: 'Attributes Update',
        details: auditMessage,
        component: 'apis-object',
      };

      expect(mockActivityNoteService.updateAsync).toHaveBeenCalledTimes(1);
      expect(mockActivityNoteService.updateAsync).toHaveBeenCalledWith(note);
      expect(mockOpsmatixLogger.debug).toHaveBeenCalledTimes(1);
      expect(mockOpsmatixLogger.debug).toHaveBeenCalledWith(auditMessage);
      expect(mockOpsmatixLogger.error).toHaveBeenCalledTimes(1);
      expect(mockOpsmatixLogger.error).toBeCalledWith(
        'Failed to audit updates to attributes. error is: Boom!'
      );
    });
  });
  describe('Stream case update', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });

    it('should return updated case in the same format as `snapshot` call', async () => {
      const mockResponse = {
        _id: 'caseId',
        snoozeState: {
          status: Case.SnoozeState.Status.ACTIVE,
        },
        caseDetailsAsOf: new Date('1/12/2023'),
        attributes: {
          title: 'testing',
          origin: 'Email',
          receivedDateTime: '2022-08-26T10:46:06Z',
          autoAccept: false,
        },
        caseType: 'default',
        state: {
          status: Case.State.Status.INITIATED,
          assignedTo: {
            teamRef: {
              team: '633e8da9e45ba6d2eccdc2de',
              name: 'TeamName',
            },
          },
        },
        createdAt: new Date('1/1/2023'),
        updatedAt: new Date('1/1/2023'),
      };

      const response = {
        _id: 'caseId',
        attr: {
          autoAccept: false,
          origin: 'Email',
          receivedDateTime: '2022-08-26T10:46:06Z',
          title: 'testing',
        },
        cdao: new Date('1/12/2023'),
        ct: 'default',
        requester: {},
        s: {
          assignedTo: {
            teamRef: {
              name: 'TeamName',
              team: '633e8da9e45ba6d2eccdc2de',
            },
          },
          status: 'Initiated',
        },
        sn: true,
        uat: new Date('1/1/2023'),
      };

      //@ts-ignore
      mockCaseService.findById.mockResolvedValueOnce(mockResponse);

      const result = await appService.getUpdatedCase('caseId');

      expect(mockCaseService.findById).toHaveBeenCalledTimes(1);
      expect(mockCaseService.findById).toHaveBeenCalledWith('caseId');
      expect(result).toEqual([response]);
    });
  });
  describe('tryClaimCase', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });

    it('should throw error when case is not claimable state', async () => {
      const caseId = '6573532a9a772cda25da9745';
      const userId = '6573532a9a772cda25da9746';

      const mockedCase = mock<CaseDocument>({
        state: {
          status: Case.State.Status.CLOSED,
        },
      });

      //@ts-ignore
      mockCaseService.findByIdAsync.mockResolvedValueOnce(mockedCase);

      await expect(async () => {
        await appService.tryClaimCase(caseId, userId);
      }).rejects.toThrow(
        new Error(
          `Case[${caseId}] is not in claimable state[${Case.State.Status.CLOSED}]!`
        )
      );
    });

    it('should return without any error when claiming is successful', async () => {
      const caseId = '6573532a9a772cda25da9745';
      const userId = '6573532a9a772cda25da9746';

      const mockedCase = mock<CaseDocument>({
        state: {
          status: Case.State.Status.INITIATED,
        },
      });

      //@ts-ignore
      mockCaseService.findByIdAsync.mockResolvedValueOnce(mockedCase);
      mockCaseService.claimCaseWithCaseObj.mockResolvedValueOnce(mockedCase);

      await appService.tryClaimCase(caseId, userId);

      expect(mockCaseService.findByIdAsync).toHaveBeenCalledTimes(1);
      expect(mockCaseService.claimCaseWithCaseObj).toHaveBeenCalledTimes(1);
    });

    it('should throw error when claimCase throws error', async () => {
      const caseId = '6573532a9a772cda25da9745';
      const userId = '6573532a9a772cda25da9746';

      const mockedCase = mock<CaseDocument>({
        state: {
          status: Case.State.Status.INITIATED,
        },
      });

      //@ts-ignore
      mockCaseService.findByIdAsync.mockResolvedValueOnce(mockedCase);
      mockCaseService.claimCaseWithCaseObj.mockRejectedValueOnce(
        new Error('Claim Case Failed!')
      );
      await expect(async () => {
        await appService.tryClaimCase(caseId, userId);
      }).rejects.toThrow(new Error('Claim Case Failed!'));
    });
  });
});
