import { Injectable, Logger } from '@nestjs/common';
import { OpsmatixLogger } from '@opsmatix/core';
import { environment } from '../../environments/environment';
import { atlasAxiosClient } from './axios-client';

const buildCreateIndexPayload = (
  collectionName: string,
  indexName: string
): any => ({
  collectionName: collectionName,
  database: environment.atlasSearch.databaseName,
  mappings: {
    dynamic: true,
  },
  name: indexName,
});

const logResponseDetailsIfEnabled =
  (logger: Logger) =>
  (collectionName: string, operation: string, response: any) => {
    if (environment.atlasSearch.logRequestEnabled) {
      logger.log(`${collectionName} collection ${operation} response details:`);
      if (response) {
        logger.log(`atlas search indexes:${JSON.stringify(response.data)}`);
        logger.log(`httpStatus:${response.status}`);
        logger.log(`httpStatusAsText:${response.statusText}`);
        logger.log(`response headers:${JSON.stringify(response.headers)}`);
        logger.log(`response config:${JSON.stringify(response.config)}`);
      } else {
        logger.error(
          `response object is undefined for collectionName:${collectionName}, operation:${operation}`
        );
      }
    }
  };

const getAtlasSearchIndexForCollection =
  (logger: Logger) => async (collectionName: string) => {
    try {
      const atlasSearchIndexResponse = await atlasAxiosClient.get(
        `/${environment.atlasSearch.databaseName}/${collectionName}`
      );
      logResponseDetailsIfEnabled(logger)(
        collectionName,
        'getAtlasSearchIndexForCollection',
        atlasSearchIndexResponse
      );
      return atlasSearchIndexResponse?.data;
    } catch (error) {
      if (error.response) {
        logger.error(
          `error.response.data:${JSON.stringify(error.response.data)}`
        );
        logger.error(
          `error.response.status:${JSON.stringify(error.response.status)}`
        );
        logger.error(
          `error.response.headers:${JSON.stringify(error.response.headers)}`
        );
      } else if (error.request) {
        logger.error(error.request);
      } else {
        logger.error('Error', error.message);
      }
      logger.error(`${JSON.stringify(error.config)}`);
    }
  };

@Injectable()
export class AtlasClientService {
  private readonly logger: Logger = new OpsmatixLogger(`AtlasClientService`);

  async hasSearchIndex(collectionName: string): Promise<boolean> {
    const searchIndexes = await getAtlasSearchIndexForCollection(this.logger)(
      collectionName
    );
    return searchIndexes && searchIndexes.length > 0;
  }

  async createIndex(
    collectionName: string,
    indexName = 'default'
  ): Promise<any> {
    try {
      const createIndexResponse = await atlasAxiosClient.post(
        '',
        buildCreateIndexPayload(collectionName, indexName)
      );
      logResponseDetailsIfEnabled(this.logger)(
        collectionName,
        'createIndex',
        createIndexResponse
      );

      return createIndexResponse?.data;
    } catch (error) {
      if (error.response) {
        this.logger.error(
          `error.response.data:${JSON.stringify(error.response.data)}`
        );
        this.logger.error(
          `error.response.status:${JSON.stringify(error.response.status)}`
        );
        this.logger.error(
          `error.response.headers:${JSON.stringify(error.response.headers)}`
        );
      } else if (error.request) {
        this.logger.error(error.request);
      } else {
        this.logger.error('Error', error.message);
      }
      this.logger.error(`${JSON.stringify(error.config)}`);
    }
  }
}
