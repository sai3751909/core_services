/**
 * Temperoray work around for axios.
 * Fix - https://opsmatix.atlassian.net/browse/ENG-677
 */
const axios = require('axios');
import { AxiosInstance } from 'axios';
import { environment } from '../../environments/environment';
import * as crypto from 'crypto';

const buildHeaders = (): any => ({ 'Content-Type': 'application/json' });
const builBaseURL = (): string =>
  environment.atlasSearch.apiUrl
    .replace('${GROUP_ID}', environment.atlasSearch.groupId)
    .replace('${CLUSTER_NAME}', environment.atlasSearch.clusterName);

export const atlasAxiosClient: AxiosInstance = axios.create({
  baseURL: builBaseURL(),
  headers: buildHeaders(),
});

if (environment.atlasSearch.logRequestEnabled === true) {
  atlasAxiosClient.interceptors.request.use((request) => {
    console.log('Starting Request', JSON.stringify(request, null, 2));
    return request;
  });
}

atlasAxiosClient.interceptors.response.use(undefined, (err) => {
  const error = err.response;
  const originalRequest = error.config;

  if (error.status === 401 && error.config && !error.config.__isRetryRequest) {
    const authDetails = err.response.headers['www-authenticate']
      .split(', ')
      .map((v) => v.split('='));
    const nonceCount = ('00000000' + 1).slice(-8);
    const cnonce = crypto.randomBytes(24).toString('hex');

    const realm = authDetails[0][1].replace(/"/g, '');
    const nonce = authDetails[2][1].replace(/"/g, '');

    const md5 = (str) => crypto.createHash('md5').update(str).digest('hex');

    const HA1 = md5(
      `${environment.atlasSearch.publicKey}:${realm}:${environment.atlasSearch.privateKey}`
    );
    const HA2 = md5(`${originalRequest.method.toUpperCase()}:${builBaseURL()}`);
    const response = md5(`${HA1}:${nonce}:${nonceCount}:${cnonce}:auth:${HA2}`);

    const authorization =
      `Digest username="${environment.atlasSearch.publicKey}",realm="${realm}",` +
      `nonce="${nonce}",uri="${builBaseURL()}",qop="auth",algorithm="MD5",` +
      `response="${response}",nc="${nonceCount}",cnonce="${cnonce}"`;

    originalRequest.__isRetryRequest = true;
    originalRequest.headers.authorization = authorization;

    return atlasAxiosClient(originalRequest);
  } else {
    console.error(err.response);
  }
});
