import { Module } from '@nestjs/common';
import { AtlasClientService } from './atlas-client-service';
import { CreateAtlasSearchIndexService } from './create-atlas-search-index';

@Module({
  providers: [CreateAtlasSearchIndexService, AtlasClientService],
})
export class AppInitModule {}
