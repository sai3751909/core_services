import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { OpsmatixLogger } from '@opsmatix/core';
import { environment } from '../../environments/environment';
import { AtlasClientService } from './atlas-client-service';

@Injectable()
export class CreateAtlasSearchIndexService implements OnModuleInit {
  private readonly logger: Logger = new OpsmatixLogger(
    `CreateAtlasSearchIndexService`
  );

  constructor(private readonly atlasClientService: AtlasClientService) {}

  async onModuleInit(): Promise<void> {
    this.logger.log(
      `The module has been initialized. createAtlasSearchIndexOnInitEnabled:${environment.atlasSearch.createIndexOnInitEnabled}`
    );
    if (environment.atlasSearch.createIndexOnInitEnabled === true) {
      this.logger.log(
        `Required atlas search index for collections:${environment.atlasSearch.collectionNames}`
      );

      const collectionNames =
        environment.atlasSearch.collectionNames.split(',');
      for await (const collectionName of collectionNames) {
        this.logger.log(
          `Checking the search Index for collection:${collectionName}`
        );
        const has = await this.atlasClientService.hasSearchIndex(
          collectionName
        );
        if (!has) {
          this.logger.log(
            `${collectionName} collection doesn't have search index. Creating it...`
          );
          const createIndexResponse = await this.atlasClientService.createIndex(
            collectionName
          );
          this.logger.log(
            `createIndexResponse: ${
              createIndexResponse
                ? JSON.stringify(createIndexResponse)
                : 'Couldnt create the index for collection'
            }`
          );
        } else {
          this.logger.log(
            `${collectionName} collection has search index. So doing nothing.`
          );
        }
      }
    } else {
      this.logger.log(
        `Creating Atlas Search Index is not enabled. So doing nothing...`
      );
    }
  }
}
