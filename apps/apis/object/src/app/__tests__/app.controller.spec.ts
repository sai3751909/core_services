import * as request from 'supertest';
import {
  CanActivate,
  ConsoleLogger,
  ExecutionContext,
  INestApplication,
} from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { APP_GUARD } from '@nestjs/core';
import { Observable } from 'rxjs';
import { CacheModule } from '@nestjs/cache-manager';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { MongooseModule } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { AppService } from '../app.service';
import { any, mock } from 'jest-mock-extended';
import { AppController } from '../app.controller';
import exp = require('constants');

jest.mock('../../environments/environment', () => ({
  environment: {},
}));
export class MockAuthGuard implements CanActivate {
  canActivate(
    context: ExecutionContext
  ): boolean | Promise<boolean> | Observable<boolean> {
    return true;
  }
}

describe('Object API App Controller tests', () => {
  const appService = mock<AppService>();
  let app: INestApplication;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      providers: [
        {
          provide: APP_GUARD,
          useClass: MockAuthGuard,
        },
        {
          provide: AppService,
          useValue: appService,
        },
      ],
      controllers: [AppController],
    })
      .setLogger(new ConsoleLogger())
      .compile();
    app = module.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  beforeEach(async () => {
    jest.resetAllMocks();
  });

  describe('table view additional case filters test cases', () => {
    it(`/GET table view cases with additional case filters`, async () => {
      appService.getDashboardCases
        .calledWith(any(), any(), any())
        .mockResolvedValueOnce([]);

      await request(app.getHttpServer())
        .get(
          '/cases/dashboard-cases?filter:QueryType=NAV&filter:Priority=High&NotAFilter=NewValue'
        )
        .expect(200);

      expect(appService.getDashboardCases).toHaveBeenCalledTimes(1);
      expect(appService.getDashboardCases).toHaveBeenCalledWith(any(), any(), {
        'caseAttributes.QueryType': 'NAV',
        'caseAttributes.Priority': 'High',
      });
    });

    it(`/GET table view cases with NO additional case filters`, async () => {
      appService.getDashboardCases
        .calledWith(any(), any(), any())
        .mockResolvedValueOnce([]);

      await request(app.getHttpServer())
        .get('/cases/dashboard-cases?NotAFilter=NewValue')
        .expect(200);

      expect(appService.getDashboardCases).toHaveBeenCalledTimes(1);
      expect(appService.getDashboardCases).toHaveBeenCalledWith(
        any(),
        any(),
        null
      );
    });
  });
});
